//
//  AddFilialVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class AddFilialVC: UIViewController {
    
    let UI: AddFilialView = {
        let UI = AddFilialView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.msiBlue
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var previousIndexPath: IndexPath? = nil
    var currentIndexPath: IndexPath? = nil
    var cellItem: FilialSelectedCell? = nil
    var selectedFilial: FilialModel? = nil
    weak var delegate: GetFilialProtocol?
    let cadenas = ["1", "2", "3", "4", "5", "6"]
    var filialsArray = [FilialModel]()
    var favouriteFilial: FilialModel? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        setTargets()
        setDelegates()
        validateConnection()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = "Agregar favorita"
        
        let closeItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop, target: self, action: #selector(closePressed))
        
        navigationItem.rightBarButtonItem = closeItem
    }
    
    @objc private func closePressed() {
        
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
        UI.flag = false
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asignar los delegados del receptor
    private func setDelegates() {
        
        UI.tableView.delegate = self
        UI.tableView.dataSource = self
    }
    
    // MARK: - Método para asociar una acción a los controles correspondientes
    private func setTargets() {
        
        UI.addBtn.addTarget(self, action: #selector(btnPressed), for: UIControl.Event.touchUpInside)
    }
    
    // MARK: - Métodos para declarar las acciones asociadas a los botones
    @objc private func btnPressed() {
        let alert = UIAlertController(title: "Agregar a favoritos", message: "¿Deseas agregar esta filial a favoritos?", preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.destructive, handler: nil)
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) { (_) in
            if let filial = self.selectedFilial {
                self.delegate?.selectedFilial(filial: filial)
                self.dismiss(animated: true, completion: nil)
        
            }

        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {
        
        previousIndexPath = nil
        currentIndexPath = nil
    }
    
    // MARK: - Método para hacer el request al servicio de getFiliales
    private func request() {
        let token = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) ?? ""
        let parameters = FilialRequestModel(token: token)
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getFilialesRequest()
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getFilials()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
    
}
