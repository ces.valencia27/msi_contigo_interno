//
//  HomeVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    let UI: HomeView = {
        let UI = HomeView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var cellArray1 = [HomeCellViewModel]()
    var cellArray2 = [HomeCellViewModel]()
    var areas = [AreasModel]()
    var networkManager1: URL_Session?
    var networkManager2: URL_Session?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setupArrays()
        setSubviews()
        setAutolayout()
        setTargets()
        setDelegates()
        validateConnection()
        
        if let _ = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) {
            UserDefaults.standard.set(true, forKey: "firstEntry")
        } else {
            UserDefaults.standard.set(false, forKey: "firstEntry")
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size

        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }

        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        if let newImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()

            return newImage
        } else {
            UIGraphicsEndImageContext()

            return UIImage(named: "profilePlaceholder")!
        }

    }
    
    private func setupArrays() {
        
        let cellModel1 = HomeCellModel(icon: UIImage(named: "filialIcon") ?? UIImage(), name: "Filial")
        let cellModel2 = HomeCellModel(icon: UIImage(named: "avisosIcon") ?? UIImage(), name: "Avisos")
        let cellModel3 = HomeCellModel(icon: UIImage(named: "newsIcon") ?? UIImage(), name: "Noticias")
        let cellModel4 = HomeCellModel(icon: UIImage(named: "contactIcon") ?? UIImage(), name: "Contacto")
        let cellModel5 = HomeCellModel(icon: UIImage(named: "faqIcon") ?? UIImage(), name: "FAQ")
        let cellModel6 = HomeCellModel(icon: UIImage(named: "webSiteIcon") ?? UIImage(), name: "Sitio Web")
        
        cellArray1 = [HomeCellViewModel(cellModel: cellModel1), HomeCellViewModel(cellModel: cellModel2), HomeCellViewModel(cellModel: cellModel3)]
        cellArray2 = [HomeCellViewModel(cellModel: cellModel4), HomeCellViewModel(cellModel: cellModel5), HomeCellViewModel(cellModel: cellModel6)]
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
//        navigationItem.title = "Inicio"
        
        let logoImage = UIImage(named: "logo")
        let imageView = UIImageView(image: logoImage)
        let bannerWidth = navigationController?.navigationBar.frame.size.width
        let bannerHeight = navigationController?.navigationBar.frame.size.height
        let bannerX = bannerWidth!/2 - (logoImage?.size.width)!/2
        let bannerY = bannerHeight!/2 - (logoImage?.size.height)!/2
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth!, height: bannerHeight!)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
        navigationItem.title = "Inicio"
        
        if let imageData = getUserImage() {
            let avatarImage = resizeImage(image: UIImage(data: imageData)!, targetSize: CGSize(width: 30.0, height: 30.0))
            let imageView = UIImageView(image: avatarImage)
            imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            imageView.contentMode = .scaleToFill
            imageView.layer.cornerRadius = 15
            imageView.layer.masksToBounds = true
            
            let profileIcon = UIBarButtonItem(customView: imageView)
//            let profileIcon = UIBarButtonItem(image: UIImage(data: imageData)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuPressed))
            let menuItem = UIBarButtonItem(image: UIImage(named: "dotsIcon")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuPressed))
            
            navigationItem.leftBarButtonItem = profileIcon
            navigationItem.rightBarButtonItem = menuItem
        } else {
            let profileIcon = UIBarButtonItem(image: UIImage(named: "profilePlaceholder")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: nil, action: nil)
            let menuItem = UIBarButtonItem(image: UIImage(named: "dotsIcon")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuPressed))
            
            navigationItem.leftBarButtonItem = profileIcon
            navigationItem.rightBarButtonItem = menuItem
        }

    }
    
    @objc public func menuPressed() {
        
        debugPrint("Open Menu")
        menuNavigation()
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asociar una acción a los controles(botones)
    private func setTargets() {
        
        UI.addFilialBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
        UI.changeFilialBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    
    // MARK: - Método para declarar las acciones asociadas a los botones
    @objc private func btnAction(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            addFilialNavigation()
        case 1:
            if UI.addFilialBtn.tag == 0 {
                
            } else {
                StaticMethods.shared().clearGradient(UI.addFilialBtn, title: "Agregar filial favorita", tag: 0)
                UI.changeFilialBtn.isEnabled = false
                addFilialNavigation()
            }
        default:
            getSavedFilial()
        }

    }
    
    // MARK: - Método para asignar los delegados del receptor
    private func setDelegates() {
        
        UI.generalCollection.delegate = self
        UI.generalCollection.dataSource = self
        UI.msiCollection.delegate = self
        UI.msiCollection.dataSource = self
        
    }
    
    // MARK: - Métodos que se ejecutan en los eventos de los botones
    private func addFilialNavigation() {
        
        let nextVC = AddFilialVC()
        nextVC.delegate = self
        let navigation = UINavigationController(rootViewController: nextVC)
        navigation.navigationBar.tintColor = UIColor.white
        navigation.navigationBar.barTintColor = UIColor.clear
        navigation.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.barStyle = .black
        
        present(navigation, animated: true, completion: nil)
    }
    
    // MARK: - Método para navegar a la pantalla de menú
    private func menuNavigation() {
        
        let nextVC = MenuVC()
        nextVC.delegate = self
        let navigation = UINavigationController(rootViewController: nextVC)
        navigation.navigationBar.tintColor = UIColor.black
        navigation.navigationBar.barTintColor = UIColor.clear
        navigation.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.barStyle = .default
        
        present(navigation, animated: true, completion: nil)
    }
    
    // MARK: - Método para extraer información de CoreData y navegar a la pantalla de Módulos
    public func getSavedFilial() {
        
        do {
            if let filial = try readData() {
                let nextVC = FilialModulesVC()
                nextVC.isFavourite = true
                nextVC.currentFilial = filial
                navigationController?.pushViewController(nextVC, animated: true)
            } else {
                debugPrint("No se pudo obtener el valor del opcional")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: - Método para extraer información de CoreData y establecer el valor inicial del botón
    public func setFavouriteBtn() {
        do {
            let obj = try readData()
            
            if let filial = obj {
                debugPrint("Si hay registro =)")
                StaticMethods.shared().getGradiendBG(UI.addFilialBtn, title: filial.nombre, tag: 2)
                UI.changeFilialBtn.isEnabled = true
            } else {
                StaticMethods.shared().clearGradient(UI.addFilialBtn, title: "Agregar filial favorita", tag: 0)
                UI.changeFilialBtn.isEnabled = false
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: - Método para hacer el request al servicio de updateOneSignal
    private func updateOneSignal() {
        
        if let playerID = UserDefaults.standard.string(forKey: "playerID") {
            let parameters = UpdatePushRequestModel(onesignal: playerID)
            networkManager2 = URL_Session()
            networkManager2?.delegate = self
            networkManager2?.updateOneSignal(parameters)
        } else {
            
        }
    }
    
    // MARK: - Método para hacer la petición al servicio de getAreas
    private func request() {
        
        let parameters = AreasRequestModel()
        networkManager1 = URL_Session()
        networkManager1?.delegate = self
        networkManager1?.getAreas(parameters)
        
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
            let operation = OperationQueue()
            let block1 = BlockOperation {
                self.request()
            }
            
            let block2 = BlockOperation {
                self.updateOneSignal()
            }
            
            operation.addOperation(block1)
            operation.addOperation(block2)
        } else {
            getAreas()
        }
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {
        setFavouriteBtn()
    }
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {
        setFavouriteBtn()
    }
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
}

