//
//  NewsDetailVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController {

    let UI: NewsDetailView = {
        let UI = NewsDetailView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var new: NewsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        setDelegates()
        validateConnection()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = ""
        
        let shareItem = UIBarButtonItem(image: UIImage(named: "shareIcon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(shareItemPressed))
        
        navigationItem.rightBarButtonItem = shareItem
    }
    
    @objc private func shareItemPressed() {
        
        let activityController = UIActivityViewController(activityItems: ["\(new?.titulo ?? "") para leer más información descarga la app MSI Contigo en: https://apps.apple.com/mx/app/msi-contigo/id1482202179"], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asignar los delegados del receptor
    private func setDelegates() {
        
//        UI.newsContent.delegate = self
    }
    
    // MARK: - Método para hacer el request al servicio de getNoticiaDetail
    private func request() {
        
        let token = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) ?? ""
        let newID = new?.id ?? 0
        let parameters = NewsDetailRequestModel(token: token, noticia_id: "\(newID)")
        
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getNoticiaDetailRequest(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getNewsDetail()
        }
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
