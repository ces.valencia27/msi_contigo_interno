//
//  LoginVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    let interface: LoginView = {
        let interface = LoginView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.msiBlue
        interface.translatesAutoresizingMaskIntoConstraints = false
        return interface
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del cotrolador
        setupUI()
    }
    
    // Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setSubviews()
        setAutolayout()
        setTargets()
        setDelegates()
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        navigationItem.title = ""
        view.addSubview(interface)
    }
    
    // Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            interface.topAnchor.constraint(equalTo: view.topAnchor),
            interface.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            interface.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            interface.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asociar una acción a los controles(botones)
    private func setTargets() {
        
        interface.enterBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
        interface.passRecoverBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
        interface.registerBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
        interface.termsBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    // MARK: - Método para declarar las acciones asociadas a los botones
    @objc private func btnAction(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            debugPrint("Entrar")
            if NetworkConnection.isConnectedToNetwork() {
                accessEvent()
            } else {
                let alert = UIAlertController(title: "Aviso", message: "No se pudo iniciar sesión, verifique su conexión a internet y vuelva a intentarlo", preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                navigationController?.present(alert, animated: true, completion: nil)
            }
        case 1:
            passRecoverNavigation()
        case 2:
            debugPrint("Regitro")
            enrollNavigation()
        default:
            debugPrint("Términos y condiciones")
            termsNavigation()
        }
    }
    
    // MARK: - Método para asignar los delegados del receptor
    private func setDelegates() {
        
        interface.employeeNumberField.delegate = self
        interface.mailField.delegate = self
        interface.passField.delegate = self
    }
    
    // MARK: - Métodos que ejecutan los eventos de los botones del controlador
    private func accessEvent() {
        
        if let number = interface.employeeNumberField.text, let mail = interface.mailField.text, let pass = interface.passField.text {
            if number.isEmpty {
                interface.employeeNumberField.becomeFirstResponder()
                interface.employeeNumberField.layer.borderColor = UIColor.red.cgColor
                interface.employeeNumberField.attributedPlaceholder = NSAttributedString(string: "Campo obligatorio", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else if number.count < 13 {
                interface.employeeNumberField.becomeFirstResponder()
                interface.employeeNumberField.layer.borderColor = UIColor.red.cgColor
                interface.employeeNumberField.attributedPlaceholder = NSAttributedString(string: "El RFC debe contener 13 caracteres", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
                interface.employeeNumberField.text = ""
            } else if mail.isEmpty {
                interface.mailField.becomeFirstResponder()
                interface.mailField.layer.borderColor = UIColor.red.cgColor
                interface.mailField.attributedPlaceholder = NSAttributedString(string: "Campo obligatorio", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else if pass.isEmpty {
                interface.passField.becomeFirstResponder()
                interface.passField.layer.borderColor = UIColor.red.cgColor
                interface.passField.attributedPlaceholder = NSAttributedString(string: "Campo obligatorio", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else if Utils.shared.isValidEmail(string: mail) != true {
                interface.mailField.text = ""
                interface.mailField.becomeFirstResponder()
                interface.mailField.layer.borderColor = UIColor.red.cgColor
                interface.mailField.attributedPlaceholder = NSAttributedString(string: "El formato de correo es incorrecto", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else {
                let params = LoginRequestModel(employee_number: number, employee_mail: mail, employee_password: pass)
                let networkManager = URL_Session()
                networkManager.delegate = self
                networkManager.loginRequest(params)
                LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
            }
        } else {
            debugPrint("No fue posible hacer la petición")
        }

    }
    
    private func passRecoverNavigation() {
        
        let nextVC = PassRecoverVC()
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    private func enrollNavigation() {
        
        let enrollVC = EnrollVC()
        self.navigationController?.pushViewController(enrollVC, animated: true)
    }
    
    private func termsNavigation() {
        
        let liga: String? = "https://www.multisistemas.com.mx/privacidad.php"
        openURL(link: liga)
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
    
}
