//
//  NewsVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class NewsVC: UIViewController {

    let UI: NewsView = {
        let UI = NewsView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var filialID: Int = 0
    var isFilial: Bool = false
    var fakeBolean: Bool = false
    var newsArray = [NewsModel]()
    var pagingArray: ArraySlice<NewsModel> = []
    var filterArray = [NewsModel]()
    var filterIsActive: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setSubviews()
        setAutolayout()
        setDelegates()
        validateConnection()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = "Todas"
        navigationItem.prompt = "Noticias"
//        navigationController?.navigationBar.prefersLargeTitles = true
//        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.bold)]
        
        let filterItem = UIBarButtonItem(image: UIImage(named: "filterIcon")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(filterPressed))
        
        navigationItem.rightBarButtonItem = filterItem
    }
    
    @objc private func filterPressed() {
        
        debugPrint("Filtrar")
        showAlertFilter()
        
    }
    
    func showAlertFilter() {
        
        let categories = ["Todos", "Seguridad", "Tecnologia", "Consejos", "Sociales"]
        
        let actionSheet = UIAlertController(title: "Categorías", message: "Selecciona una categoría", preferredStyle: UIAlertController.Style.actionSheet)
        
        categories.forEach { (category) in
            
            let action = UIAlertAction(title: category, style: UIAlertAction.Style.default) { (_) in
                self.navigationItem.title = category
                if category == "Todos" {
                    self.filterIsActive = false
                    self.UI.newsCollection.reloadData()
                } else {
                    self.filterIsActive = true
                    self.filterArray = self.newsArray.filter( {$0.categoria == category })
                    self.UI.newsCollection.reloadData()
                }

            }
            actionSheet.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil)
        
        actionSheet.addAction(cancelAction)

        navigationController?.present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asignar los delegados del receptor
    private func setDelegates() {
        
        UI.newsCollection.delegate = self
        UI.newsCollection.dataSource = self
    }
    
    // MARK: - Método para hacer la petición al servicio de getNoticias
    private func request() {
        
        let parameters = NewsRequestModel()
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getNoticiasRequest(parameters)
//        if isFilial {
//            networkManager.getNoticiasByFilialRequest(parameters)
//        } else {
//            networkManager.getNoticiasRequest(parameters)
//        }
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
        
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getNews()
        }
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
//        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
