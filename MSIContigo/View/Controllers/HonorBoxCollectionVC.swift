//
//  HonorBoxCollectionVC.swift
//  MSIContigo
//
//  Created by David Valencia on 9/4/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

private let reuseIdentifier = "honorBoxCell"

class HonorBoxCollectionVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var filialID: Int = 0
    var peopleArray = [PersonModel]()
    var pagingArray: ArraySlice<PersonModel> = []
    var fakeBolean: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.view.backgroundColor = UIColor.white
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.register(HonorBoxCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView.register(HonorBoxFooterSection.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "footer")

        // Do any additional setup after loading the view.
        setupNavBar()
        validateConnection()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.prompt = "Cuadro de Honor"
        if let filialName = UserDefaults.standard.string(forKey: "filial_name") {
            navigationItem.title = filialName
        }
//        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barTintColor = UIColor.white
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return pagingArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HonorBoxCell
        cell.backgroundColor = UIColor.clear
        cell.modelView = HonorBoxViewModel(person: pagingArray[indexPath.item])
        
        // Configure the cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wCell = self.collectionView.bounds.width/3
        let hCell = wCell

        return CGSize(width: wCell, height: hCell)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let nextVC = HonorBoxDetailVC()
        nextVC.currentPerson = pagingArray[indexPath.item]
        navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if fakeBolean != true {
            return CGSize(width: self.view.frame.width, height: self.view.frame.height * 0.2)
        } else {
            return CGSize.zero
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionFooter:
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footer", for: indexPath) as! HonorBoxFooterSection
            footerView.showMoreBtn.addTarget(self, action: #selector(showMorePressed), for: UIControl.Event.touchUpInside)
            
            return footerView
            
        default:
            debugPrint("No se agrega reusableView")
            return UICollectionReusableView()
        }
    }
    
    @objc func showMorePressed() {
        
        if peopleArray.count > 0 {
            
            let newItems = self.peopleArray.prefix(5)
            self.pagingArray.append(contentsOf: newItems)
            self.peopleArray = Array(self.peopleArray.dropFirst(5))
            self.collectionView.reloadData()
        } else {
            fakeBolean = true
            self.collectionView.reloadData()
        }
    }

    // MARK: - Método para hacer la petición al servicio de getC_Honor
    private func request() {
        
        let parameters = HonorBoxRequestModel()
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getC_Honor(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getPeople()
        }
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
}
