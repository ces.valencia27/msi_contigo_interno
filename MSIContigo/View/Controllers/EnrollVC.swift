//
//  EnrollVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class EnrollVC: UIViewController {
    
    let UI: EnrollView = {
        let UI = EnrollView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.msiBlue
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var getFilialsManager: URL_Session?
    var newRequestManager: URL_Session?
    var filialsArray: [FilialModel] = [FilialModel]()
    var filialID: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setSubviews()
        setAutolayout()
        setTargets()
        setDelegates()
        addDoneButtonOnKeyboard()
        
        if NetworkConnection.isConnectedToNetwork() {
            getFilialsRequest()
        } else {
            let alert = UIAlertController(title: "Aviso", message: "No se pudo iniciar sesión, verifique su conexión a internet y vuelva a intentarlo", preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(action)
            navigationController?.present(alert, animated: true, completion: nil)
        }

    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asociar una acción a los controles(botones)
    private func setTargets() {
        
        UI.requestBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
        UI.termsBtn.addTarget(self, action: #selector(btnAction(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    // MARK: - Método para declarar las acciones asociadas a los botones
    @objc private func btnAction(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            debugPrint("Solicitud")
            requestEvent()
        default:
            debugPrint("Términos y condiciones")
            termsNavigation()
        }
    }
    
    // MARK: - Método para asignar los delegados del receptor
    private func setDelegates() {
        
        UI.employeeNumberField.delegate = self
        UI.mailField.delegate = self
        UI.filialField.delegate = self
        UI.filialPicker.delegate = self
        UI.filialPicker.dataSource = self
    }
    
    // MARK: - Métodos que ejecutan los eventos de los botones del controlador
    private func requestEvent() {
        
        if let number = UI.employeeNumberField.text, let mail = UI.mailField.text, let filial = UI.filialField.text {
            if number.isEmpty {
                UI.employeeNumberField.becomeFirstResponder()
                UI.employeeNumberField.layer.borderColor = UIColor.red.cgColor
                UI.employeeNumberField.attributedPlaceholder = NSAttributedString(string: "Campo obligatorio", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else if number.count < 13 {
                UI.employeeNumberField.becomeFirstResponder()
                UI.employeeNumberField.layer.borderColor = UIColor.red.cgColor
                UI.employeeNumberField.attributedPlaceholder = NSAttributedString(string: "El RFC debe contener 13 caracteres", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
                UI.employeeNumberField.text = ""
            } else if mail.isEmpty {
                UI.mailField.becomeFirstResponder()
                UI.mailField.layer.borderColor = UIColor.red.cgColor
                UI.mailField.attributedPlaceholder = NSAttributedString(string: "Campo obligatorio", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else if filial.isEmpty {
                UI.mailField.becomeFirstResponder()
                UI.mailField.layer.borderColor = UIColor.red.cgColor
                UI.mailField.attributedPlaceholder = NSAttributedString(string: "Campo obligatorio", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else if Utils.shared.isValidEmail(string: mail) != true {
                UI.mailField.text = ""
                UI.mailField.becomeFirstResponder()
                UI.mailField.layer.borderColor = UIColor.red.cgColor
                UI.mailField.attributedPlaceholder = NSAttributedString(string: "El formato de correo es incorrecto", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.red])
            } else {
                if NetworkConnection.isConnectedToNetwork() {
                    let params = NewSolicitudRequestModel(employee_number: number, employee_mail: mail, filial_id: String(filialID))
                    newRequestManager = URL_Session()
                    newRequestManager?.delegate = self
                    newRequestManager?.newSolicitudRequest(params)
                    LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
                } else {
                    let alert = UIAlertController(title: "Aviso", message: "No se pudo iniciar sesión, verifique su conexión a internet y vuelva a intentarlo", preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(action)
                    navigationController?.present(alert, animated: true, completion: nil)
                }

            }
        } else {
            debugPrint("No fue posible hacer la petición")
        }
    }
    
    // MARK: - Método para hacer el request al servicio de getFiliales
    private func getFilialsRequest() {
        
        getFilialsManager = URL_Session()
        getFilialsManager?.delegate = self
        getFilialsManager?.getFilialesRequest()
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    
    private func termsNavigation() {
        
        let liga: String? = "https://www.multisistemas.com.mx/privacidad.php"
        openURL(link: liga)
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
}
