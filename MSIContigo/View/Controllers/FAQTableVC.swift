//
//  FAQTableVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/31/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class FAQTableVC: UITableViewController {
    
    let cellId = "faqCell"
    
    
    var twoDimensionalArray = [QuestionModel]()
    
    var showIndexPaths = false
    var headerButton: HeaderButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        setupNavBar()
        self.tableView.register(FaqCell.self, forCellReuseIdentifier: cellId)
        validateConnection()
    }
    
    @objc func handleShowIndexPath() {
        
        print("Attemping reload animation of indexPaths...")
        
        // build all the indexPaths we want to reload
        var indexPathsToReload = [IndexPath]()
        
        for section in twoDimensionalArray.indices {
            for row in twoDimensionalArray[section].anwers.indices {
                print(section, row)
                let indexPath = IndexPath(row: row, section: section)
                indexPathsToReload.append(indexPath)
            }
        }
        
        //        for index in twoDimensionalArray[0].indices {
        //            let indexPath = IndexPath(row: index, section: 0)
        //            indexPathsToReload.append(indexPath)
        //        }
        
        showIndexPaths = !showIndexPaths
        
        let animationStyle = showIndexPaths ? UITableView.RowAnimation.right : .left
        
        tableView.reloadRows(at: indexPathsToReload, with: animationStyle)
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = "Preguntas frecuentes"
        
        let closeItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop, target: self, action: #selector(closePressed))
        
        navigationItem.rightBarButtonItem = closeItem
    }
    
    @objc private func closePressed() {
        
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return twoDimensionalArray.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !twoDimensionalArray[section].isExpanded {
            return 0
        }
        
        return twoDimensionalArray[section].anwers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! FaqCell
        let answer = twoDimensionalArray[indexPath.section].anwers[indexPath.row]
        
        cell.questionLabel.text = "\n" + answer + "\n\n"
        
        
        
        //        cell.textLabel?.text = name
        //        cell.textLabel?.layer.borderColor = UIColor.gray.cgColor
        //        cell.textLabel?.layer.borderWidth = 1
        
        //        cell.textLabel?.backgroundColor = UIColor.customWhite1
        //        cell.textLabel?.numberOfLines = 0
        
        //        if showIndexPaths {
        //            cell.textLabel?.text = "\(name)   Section:\(indexPath.section) Row:\(indexPath.row)"
        //        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        headerButton = HeaderButton(type: .system)
        headerButton?.setTitle(twoDimensionalArray[section].question, for: .normal)
        if #available(iOS 13.0, *) {
            headerButton?.setTitleColor(.label, for: .normal)
        } else {
            headerButton?.setTitleColor(.black, for: .normal)
        }
        if #available(iOS 13.0, *) {
            headerButton?.backgroundColor = UIColor.systemBackground
        } else {
            headerButton?.backgroundColor = UIColor.white
        }
        headerButton?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        headerButton?.contentHorizontalAlignment = .left
        headerButton?.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        headerButton?.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        headerButton?.tag = section
        
        let containerView = UIView()
        
        containerView.addSubview(headerButton!)
        
        NSLayoutConstraint.activate([
            headerButton!.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            headerButton!.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        //        button.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        //        button.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        
        return containerView
        
    }
    
    @objc func handleExpandClose(button: UIButton) {
        print("Trying to expand and close section...")
        
        let section = button.tag
        
        // we'll try to close the section first by deleting the rows
        var indexPaths = [IndexPath]()
        for row in twoDimensionalArray[section].anwers.indices {
            print(0, row)
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = twoDimensionalArray[section].isExpanded
        twoDimensionalArray[section].isExpanded = !isExpanded
        
        button.setTitle(isExpanded ? twoDimensionalArray[section].question : twoDimensionalArray[section].question, for: .normal)
        
        if isExpanded {
            tableView.deleteRows(at: indexPaths, with: .fade)
            
            UIView.animate(withDuration: 0.5) {
                
            }
            
        } else {
            UIView.animate(withDuration: 0.5) {
                self.headerButton?.layer.cornerRadius = 10.0
            }
            
            tableView.insertRows(at: indexPaths, with: .fade)
            
//            for item in indexPaths {
//                let headers = tableView.headerView(forSection: item.item)?.subviews
//            }
            
            for index in indexPaths {
                let cell = tableView.cellForRow(at: index)
                cell?.textLabel?.backgroundColor = UIColor.customWhite1
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - Método para hacer la petición al servicio de getFAQ
    private func request() {
        
        let parameters = FAQRequestModel()
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getFAQ(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getFAQ()
        }
    }
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

class HeaderButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false // enables auto layout
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
//        let originalContentSize = super.intrinsicContentSize
//        let height = (self.superview?.frame.height)!
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 6.0
        layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMaxXMinYCorner]
        layer.masksToBounds = true
        return CGSize(width: UIScreen.main.bounds.width * 0.93, height: 50)
    }
    
}
