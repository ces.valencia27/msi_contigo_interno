//
//  HonorBoxDetailVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class HonorBoxDetailVC: UIViewController {

    let UI: HonorBoxDetailView = {
        let UI = HonorBoxDetailView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var currentPerson: PersonModel? = nil
    var unixID: Int = 0
    var networkManager1: URL_Session?
    var networkManager2: URL_Session?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        setTargets()
        validateConnection()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = ""
        
        let shareItem = UIBarButtonItem(image: UIImage(named: "shareIcon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(shareItemPressed))
        
        navigationItem.rightBarButtonItem = shareItem
    }
    
    @objc private func shareItemPressed() {
        
        let name = currentPerson?.nombre ?? ""
        let fecha = currentPerson?.fecha ?? ""
        let mensaje = name + " " + fecha
        let activityController = UIActivityViewController(activityItems: [mensaje], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asociar un evento con los botones de la vista
    private func setTargets() {
        
        UI.lastMonthBtn.addTarget(self, action: #selector(selectMonthPressed(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func selectMonthPressed(sender: UIButton) {
        
        if sender.tag == 0 {
            UI.lastMonthBtn.tag = 1
            UI.lastMonthBtn.setAttributedTitle(NSAttributedString(string: "Mes actual", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
            requestLastMonth()
        } else {
            UI.lastMonthBtn.tag = 0
            UI.lastMonthBtn.setAttributedTitle(NSAttributedString(string: "Mes anterior", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
            request()
        }
    }
    
    // MARK: - Método para hacer la petición al servicio de getC_HonorDetail
    private func request() {
        
        let parameters = HonorDetailRequestModel(c_honor_id: currentPerson?.id)
        networkManager1 = URL_Session()
        networkManager1?.delegate = self
        networkManager1?.getC_HonorDetail(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para hacer la petición al servicio de getC_HonorDetailPreviusMonth
    private func requestLastMonth() {
        
        let parameters = LastHonorDetailRequestModel(fecha_unix: "\(unixID)")
        networkManager2 = URL_Session()
        networkManager2?.delegate = self
        networkManager2?.getC_HonorDetailPreviusMonth(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getHonorDetail()
        }
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
