//
//  MenuVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

protocol sendAvatar: class {
    func getNewImage(data: Data)
}

class MenuVC: UIViewController {

    let UI: MenuView = {
        let UI = MenuView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.msiBlue
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var areas = [AreasModel]()
    var tapGesture: UITapGestureRecognizer?
    var pickerController: UIImagePickerController?
    weak var delegate: sendAvatar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        setAvatar()
        setTargets()
        getAreas()
        validateConnection()
    }
    
    // MARK: - Método para definir la imagen de perfil del usuario
    private func setAvatar() {
        
        if let data = getUserImage() {
            UI.avarImage.image = UIImage(data: data)
        } else {
            UI.avarImage.image = UIImage(named: "avatarPlaceholder")
        }
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = ""
        
        let closeItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop, target: self, action: #selector(closePressed))
        navigationItem.rightBarButtonItem = closeItem
    }
    
    @objc private func closePressed() {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }

    // MARK: - Action buttons
    private func setTargets() {
        
        UI.contactBtn.addTarget(self, action: #selector(btnPressed(sender:)), for: UIControl.Event.touchUpInside)
        UI.faqBtn.addTarget(self, action: #selector(btnPressed(sender:)), for: UIControl.Event.touchUpInside)
        UI.closeSessionBtn.addTarget(self, action: #selector(btnPressed(sender:)), for: UIControl.Event.touchUpInside)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapDetected(sender:)))
        tapGesture?.numberOfTapsRequired = 1
        UI.avarImage.addGestureRecognizer(tapGesture!)
    }
    
    @objc private func tapDetected(sender: UITapGestureRecognizer) {
        
        let actionSheet = UIAlertController(title: "¿Qué deseas hacer?", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        
        let takePictureAction = UIAlertAction(title: "Tomar foto", style: UIAlertAction.Style.default) { (_) in

            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.pickerController = UIImagePickerController()
                self.pickerController?.delegate = self
                self.pickerController?.sourceType = .camera
                self.pickerController?.cameraDevice = .rear
                self.pickerController?.cameraFlashMode = .auto
                self.pickerController?.cameraCaptureMode = .photo
                self.pickerController?.allowsEditing = false
                self.pickerController?.showsCameraControls = true
                
                self.present(self.pickerController!, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Aviso", message: "Este dispositivo no tiene una cámara disponible", preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        let choosePictureAction = UIAlertAction(title: "Seleccionar una foto", style: UIAlertAction.Style.default) { (_) in

            self.pickerController = UIImagePickerController()
            self.pickerController?.delegate = self
            self.pickerController?.sourceType = .photoLibrary
            
            self.present(self.pickerController!, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.destructive, handler: nil)
        
        actionSheet.addAction(takePictureAction)
        actionSheet.addAction(choosePictureAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    @objc private func btnPressed(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            showAlertContact()
        case 1:
            faqNavigation()
        default:
            if let _ = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) {
//                UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {
//    //             UIApplication.shared.keyWindow?.rootViewController?.present(LoginVC(), animated: true, completion: nil)
//                    KeychainService.removePassword(service: KeychainValues.service, account: KeychainValues.account)
//                    UserDefaults.standard.set(false, forKey: "firstEntry")
//                })
                
                KeychainService.removePassword(service: KeychainValues.service, account: KeychainValues.account)
                UserDefaults.standard.set(false, forKey: "firstEntry")
                
                let startVC = LoginVC()
                let navigationVC = UINavigationController(rootViewController: startVC)
                navigationVC.navigationBar.barTintColor = UIColor.msiBlue
                navigationVC.navigationBar.barStyle = .black
                navigationVC.navigationBar.tintColor = UIColor.msiYellow
                navigationVC.navigationBar.isTranslucent = false
                navigationVC.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                navigationVC.navigationBar.shadowImage = UIImage()

                UIApplication.shared.windows.first?.rootViewController = navigationVC
            } else {
                debugPrint("No se puede cerrar la sesión")
            }
        }
    }
    
    // MARK: - Método para abrir el controlador de preguntas frecuentes
    private func faqNavigation() {
        
        let nextVC = FAQTableVC()
        let navigation = UINavigationController(rootViewController: nextVC)
        navigation.navigationBar.tintColor = UIColor.black
        navigation.navigationBar.barTintColor = UIColor.clear
        navigation.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.barStyle = .default
        navigation.navigationBar.prefersLargeTitles = true
        navigation.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.bold)]
        
        present(navigation, animated: true, completion: nil)
    }
    
        func showAlertContact() {
            
            let actionSheet = UIAlertController(title: "Contacto", message: "Contáctanos hoy!", preferredStyle: UIAlertController.Style.actionSheet)
            
            areas.forEach { (area) in
                
                let action = UIAlertAction(title: area.nombre, style: UIAlertAction.Style.default) { (_) in
                    let nextVC = ContactVC()
                    nextVC.areaModel = area
                    nextVC.title = area.nombre
                    self.navigationController?.pushViewController(nextVC, animated: true)
                }
                actionSheet.addAction(action)
            }

            let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil)
            
            actionSheet.addAction(cancelAction)

            navigationController?.present(actionSheet, animated: true, completion: nil)
        }
    
    // MARK: - Método para hacer el request al servicio de getEmpleadoInfo
    private func request() {
        
        let token = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) ?? ""
        let parameters = EmployeeRequestModel(token: token)
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getEmpleadoInfoRequest(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getEmployeeInfo()
        }
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        navigationController?.navigationBar.tintColor = UIColor.black

    }
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
