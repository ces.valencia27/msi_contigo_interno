//
//  BannerVC.swift
//  MSIContigo
//
//  Created by David Valencia on 29/09/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class BannerVC: UIViewController {
    
    let image: UIImageView = {
        let image = UIImageView(frame: CGRect.zero)
        image.image = UIImage(named: "underTest")
        image.contentMode = UIView.ContentMode.scaleToFill
        image.isUserInteractionEnabled = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    var pinchGesture: UIPinchGestureRecognizer?
    var currentImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        setAutolayout()
        setupView()
    }
    
    private func setAutolayout() {
        
//        image.image = currentImage!
        view.addSubview(image)
        
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            image.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30)
        ])
    }
    
    private func setupView() {
        
        view.backgroundColor = UIColor.white
        
        let closeBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop, target: self, action: #selector(closePressed))
        navigationItem.rightBarButtonItem = closeBtn
        
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchDetected(sender:)))
        image.addGestureRecognizer(pinchGesture!)
    }
    
    @objc private func closePressed() {
        
        dismiss(animated: true, completion:  nil)
    }
    
    @objc private func pinchDetected(sender: UIPinchGestureRecognizer) {
        
        sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
        sender.scale = 1.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
