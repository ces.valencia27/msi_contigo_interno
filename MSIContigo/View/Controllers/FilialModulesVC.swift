//
//  FilialModulesVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class FilialModulesVC: UIViewController {
    
    let UI: FilialModulesView = {
        let UI = FilialModulesView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var currentFilial: FilialModel?
    let modulesArray = [
        ModulesModel(iconName: "Noticias", moduleName: "Noticias"),
        ModulesModel(iconName: "Mural", moduleName: "Mural"),
        ModulesModel(iconName: "Cuadro", moduleName: "C. de Honor"),
        ModulesModel(iconName: "Contacto", moduleName: "Contacto")
    ]
    var isFavourite: Bool = false
    var areas = [AreasModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        setDelegates()
        setTargets()
        getAreas()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        if let filialName = UserDefaults.standard.string(forKey: "filial_name") {
            navigationItem.title = filialName
        }
        
    
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
        
//        if isFavourite {
//            UI.stackView.isHidden = false
//        } else {
//            UI.stackView.isHidden = true
//        }
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asignar los delegados del receptor
    private func setDelegates() {
        
        UI.modulesCollection.delegate = self
        UI.modulesCollection.dataSource = self
    }
    
    // MARK: - Método para asociar los botones con su acción
    private func setTargets() {
        
        UI.changeFilialBtn.addTarget(self, action: #selector(changePressed), for: UIControl.Event.touchUpInside)
        UI.changeFilialBtn.isEnabled = true
    }
    
    // MARK: - Métodos asociados a los botones
    @objc private func changePressed() {
        
        let nextVC = AddFilialVC()
        nextVC.delegate = self
        let navigation = UINavigationController(rootViewController: nextVC)
        navigation.navigationBar.tintColor = UIColor.white
        navigation.navigationBar.barTintColor = UIColor.clear
        navigation.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.barStyle = .black
        
        present(navigation, animated: true, completion: nil)
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {
        if isFavourite {
            setFavouriteBtn()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
