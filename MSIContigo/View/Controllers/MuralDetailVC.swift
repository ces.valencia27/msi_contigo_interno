//
//  MuralDetailVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class MuralDetailVC: UIViewController {

    let UI: WallDetailView = {
        let UI = WallDetailView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var muralID: Int = 0
    var currentMural: WallDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        validateConnection()
        setTargets()
        registerForPreviewing(with: self, sourceView: UI.zoomBtn)

    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = ""
        
        let shareItem = UIBarButtonItem(image: UIImage(named: "shareIcon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(shareItemPressed))
        
        navigationItem.rightBarButtonItem = shareItem
    }
    
    @objc private func shareItemPressed() {
        
        let activityController = UIActivityViewController(activityItems: [currentMural?.titulo ?? ""], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asociar un evento con lso botones del controlador
    private func setTargets() {
        
        UI.zoomBtn.addTarget(self, action: #selector(zoomPressed), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func zoomPressed() {
        
        let nextVC = BannerVC()
        nextVC.currentImage = UI.bannerImage.image
        let nav = UINavigationController(rootViewController: nextVC)
        nav.navigationBar.barTintColor = UIColor.white
        nav.navigationBar.tintColor = UIColor.msiBlue
        
        self.present(nav, animated: true, completion: nil)
    }
    
    // MARK: - Método para hacer el request al servicio de muralDetail
    private func request() {
        
        let muralID = self.muralID
        let parameters = WallDetailRequestModel(mural_id: muralID)
        
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getMuralDetail(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getWallDetail()
        }
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
