//
//  ContactVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class ContactVC: UIViewController {

    let UI: ContactView = {
        let UI = ContactView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var areaModel: AreasModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        setDelegates()
        setTargets()
    }
    
    private func setDelegates() {
        
        UI.textField1.delegate = self
        UI.textField2.delegate = self
        UI.messageField.delegate = self
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = areaModel?.nombre
        navigationItem.prompt = "Contacto"
//        navigationController?.navigationBar.prefersLargeTitles = true
//        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    // MARK: - Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.backgroundColor = UIColor.white
        view.addSubview(UI)

    }
    
    // MARK: - Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asociar los eventos a los botones
    private func setTargets() {
        
        UI.sendBtn.addTarget(self, action: #selector(btnPressed(sender:)), for: UIControl.Event.touchUpInside)
        UI.faqBtn.addTarget(self, action: #selector(btnPressed(sender:)), for: UIControl.Event.touchUpInside)
        UI.callBtn.addTarget(self, action: #selector(btnPressed(sender:)), for: UIControl.Event.touchUpInside)
        UI.msjBtn.addTarget(self, action: #selector(btnPressed(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func btnPressed(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            request()
        case 1:
            faqNavigation()
        case 2:
            let url = URL(string: "tel://911")
            UIApplication.shared.open(url!)
        default:
            let activityController = UIActivityViewController(activityItems: ["Contacto"], applicationActivities: nil)
            present(activityController, animated: true, completion: nil)

        }
    }
    
    private func faqNavigation() {
        
        let nextVC = FAQTableVC()
        let navigation = UINavigationController(rootViewController: nextVC)
        navigation.navigationBar.tintColor = UIColor.black
        navigation.navigationBar.barTintColor = UIColor.clear
        navigation.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.barStyle = .default
        navigation.navigationBar.prefersLargeTitles = true
        navigation.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.bold)]
        
        present(navigation, animated: true, completion: nil)
    }
    
    // MARK: - Método para hacer la petición al servicio de newMensaje
    private func request() {
        
        if UI.messageField.text.isEmpty {
            let alert = UIAlertController(title: "Aviso", message: "El campo de mensaje está vacío", preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(action)
            navigationController?.present(alert, animated: true, completion: nil)
        } else {
            guard let id_area = areaModel?.id else {
                return
            }
            
            let token = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) ?? ""
            
            let parameters = NewMessageRequestModel(token: token, area_id: "\(id_area)", mensaje: UI.messageField.text)
            let networkManager = URL_Session()
            networkManager.delegate = self
            networkManager.newMensaje(parameters)
            LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
        }
        
    }
    
    // MARK: - Métodos del ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UI.msjBtn.titleEdgeInsets = UIEdgeInsets(top: 0,left: -60,bottom: 0,right: 0)
        UI.msjBtn.imageEdgeInsets = UIEdgeInsets(top: 6,left: (UI.msjBtn.frame.width) - 35,bottom: 6,right: 0)
        UI.callBtn.titleEdgeInsets = UIEdgeInsets(top: 0,left: -60,bottom: 0,right: 0)
        UI.callBtn.imageEdgeInsets = UIEdgeInsets(top: 6,left: (UI.callBtn.frame.width) - 35,bottom: 6,right: 0)
    }
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
