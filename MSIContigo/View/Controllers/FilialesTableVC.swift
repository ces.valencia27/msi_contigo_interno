//
//  FilialesTableVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class FilialesTableVC: UITableViewController {
    
    var filialsArray = [FilialModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz de la tabla
    private func setupUI() {
        
        setupNavBar()
        validateConnection()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = "Filiales"
        
        let closeItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop, target: self, action: #selector(closePressed))
        
        navigationItem.rightBarButtonItem = closeItem
    }
    
    @objc private func closePressed() {
        
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filialsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: nil)
        cell.textLabel?.text = filialsArray[indexPath.item].nombre
        cell.textLabel?.textColor = UIColor.customGreen1
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        cell.accessoryType = .disclosureIndicator

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerTitle = CustomLabel()
        headerTitle.text = "ELIGE TU FILIAL"
        headerTitle.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        headerTitle.textColor = UIColor.customGray3
        headerTitle.backgroundColor = UIColor.white
        headerTitle.numberOfLines = 0
        
        return headerTitle
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nextVC = FilialModulesVC()
        nextVC.currentFilial = filialsArray[indexPath.item]
        navigationController?.pushViewController(nextVC, animated: true)
    }

    // MARK: - Método para hacer el request al servicio de getFiliales
    private func request() {
        let token = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) ?? ""
        let parameters = FilialRequestModel(token: token)
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getFilialesRequest()
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getFilials()
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
