//
//  NotificationsVC.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class NotificationsVC: UITableViewController {
    
    let fakeArray = ["Here's my very first message", "I'm going to message another long message that will word wrap", "I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap", "Yo, dawg, Whaddup!", "This message should appear on the left with a white background bubble", "Third Section message"]
    var avisosArray = [AddModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        navigationItem.title = "Avisos"
        self.tableView.separatorStyle = .none
        self.tableView.register(AvisosCell.self, forCellReuseIdentifier: "avisosCell")
        validateConnection()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return avisosArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "avisosCell", for: indexPath) as! AvisosCell
        cell.modelView = AvisosViewModel(aviso: avisosArray[indexPath.item])
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: - Método para hacer la petición al servicio de getAvisos
    private func request() {
        
        let token = KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) ?? ""
        let parameters = AddRequestModel(token: token)
        
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getAvisosRequest(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getAds()
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
