//
//  FAQExtension.swift
//  MSIContigo
//
//  Created by EON on 9/25/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension FAQTableVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        print(response)
        FAQModel.shared.getData(dictionary: response) { (status, array, msg) in
            
            if status {
                if let jsonArray = array {
                    self.twoDimensionalArray.removeAll()
                    for item in jsonArray {
                        let faq = QuestionModel(isExpanded: false, question: item.pregunta, anwers: [item.respuesta])
                        self.twoDimensionalArray.append(faq)
                    }
                    
                    self.tableView.reloadData()
                    self.saveFAQ(jsonArray: jsonArray)
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Extensión que contiene los métodos para trabajar con CoreData en este controlador
extension FAQTableVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveFAQ(jsonArray: [FAQModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: FAQEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getFAQ() {
        
        do {
            let data = try getData()
            
            if let faq = data {
                self.twoDimensionalArray.removeAll()
                for item in faq {
                    let faq = QuestionModel(isExpanded: false, question: item.pregunta, anwers: [item.respuesta])
                    self.twoDimensionalArray.append(faq)
                }
                
                self.tableView.reloadData()
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: FAQModel) throws {

        let entity = FAQEntity(context: getContext)
        entity.pregunta = data.pregunta
        entity.respuesta = data.respuesta
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [FAQModel]? {
        
        var jsonArray = [FAQModel]()
        let data = try getContext.fetch(FAQEntity.fetchRequest() as NSFetchRequest<FAQEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = FAQModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
    
}
