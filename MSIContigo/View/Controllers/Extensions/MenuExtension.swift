//
//  MenuExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 9/15/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension MenuVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let picture = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if let imagData = picture.jpegData(compressionQuality: 0.1) {
                
                saveUserImage(imageData: imagData)

                if let data = getUserImage() {
                    UI.avarImage.image = UIImage(data: data)
                    delegate?.getNewImage(data: data)
                } else {
                    UI.avarImage.image = UIImage(named: "avatarPlaceholder")
                }
            }
        } else {
            debugPrint("No se pudo almacenar la foto")
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension MenuVC: URL_SessionDelegate {
    
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        EmployeeModel.shared.getData(dictionary: response) { (status, data, msg) in
            
            if status {
                guard let model = data else {
                    fatalError("Error en el desempaquetado")
                }
                self.UI.modelView = EmployeeViewModel(employee: model)
                let employeeNumber = UIBarButtonItem(title: "NO: \(model.numero)", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
                self.navigationItem.leftBarButtonItem = employeeNumber
                self.saveEmployeeInfo(dataModel: model)
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Extensión que contiene las funciones para trabajar con CoreData
extension MenuVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para almacenar la imagen de usuario en CoreData
    func saveUserImage(imageData: Data) {
        
        DBManager.shared.cleanEntity(currentEntity: ProfileImageEntity.self)
        
        do {
            let entity = ProfileImageEntity(context: getContext)
            entity.profile_image = imageData
            
            getContext.insert(entity)
            
            try getContext.save()
            
            print("Se almacenó el registro correctamente")
        } catch let error {
            debugPrint(error.localizedDescription)
        }

    }
    
    // MARK: - Método para obtener la imagen de usuario de CoreData
    func getUserImage() -> Data? {
        
        do {
            let data = try getImageDataFromCoreData()
            
            if let info = data {
                if let imageData = info.profile_image {
                    return imageData
                } else {
                    return nil
                }
//                if let imageData = info.profile_image {
//                    UI.avarImage.image = UIImage(data: imageData)
//                } else {
//                    UI.avarImage.image = UIImage(named: "avatarPlaceholder")
//                }
            } else {
                debugPrint("No hay registro en CoreData")
                return nil
            }
        } catch let error {
            debugPrint(error.localizedDescription)
            return nil
        }
    }
    
    func getImageDataFromCoreData() throws -> ProfileImageEntity? {
        
        let data = try getContext.fetch(ProfileImageEntity.fetchRequest() as NSFetchRequest<ProfileImageEntity>)
        
        guard let register = data.first else {
            debugPrint("No se pudo extraer el registro")
            
            return nil
        }
        
        
        return register
    }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveEmployeeInfo(dataModel: EmployeeModel) {
        
        DBManager.shared.cleanEntity(currentEntity: EmployeeEntity.self)
        do {
            try insertData(data: dataModel)
            debugPrint("Registro insertado")
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getEmployeeInfo() {
        
        do {
            let data = try getData()
            
            if let info = data {
                self.UI.modelView = EmployeeViewModel(employee: info)
                let employeeNumber = UIBarButtonItem(title: "NO: \(info.numero)", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
                self.navigationItem.leftBarButtonItem = employeeNumber
            } else {
                debugPrint("No hay registro en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: EmployeeModel) throws {

        let entity = EmployeeEntity(context: getContext)
        entity.nombre = data.nombre
        entity.email = data.email
        entity.numero = data.numero
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> EmployeeModel? {
        
        let data = try getContext.fetch(EmployeeEntity.fetchRequest() as NSFetchRequest<EmployeeEntity>)
        
        guard let register = data.first else {
            debugPrint("No se pudo extraer el registro")
            
            return nil
        }
        
        let model = EmployeeModel(entity: register)
        
        return model
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getAreas() {
        
        do {
            let data = try getAreaData()
            
            if let areasArray = data {
                self.areas.removeAll()
                self.areas = areasArray
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func getAreaData() throws -> [AreasModel]? {
        
        var jsonArray = [AreasModel]()
        let data = try getContext.fetch(AreasEntity.fetchRequest() as NSFetchRequest<AreasEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = AreasModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
}
