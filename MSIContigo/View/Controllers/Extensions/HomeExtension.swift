//
//  HomeExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import CoreData

extension HomeVC: sendAvatar {
    
    func getNewImage(data: Data) {
                if let currentImage = UIImage(data: data) {
                    let avatarImage = resizeImage(image: currentImage, targetSize: CGSize(width: 30.0, height: 30.0))
                    let imageView = UIImageView(image: avatarImage)
                    imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                    imageView.contentMode = .scaleToFill
                    imageView.layer.cornerRadius = 15
                    imageView.layer.masksToBounds = true
                    
                    let profileIcon = UIBarButtonItem(customView: imageView)
                    let menuItem = UIBarButtonItem(image: UIImage(named: "dotsIcon")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuPressed))
                    
                    navigationItem.leftBarButtonItem = profileIcon
                    navigationItem.rightBarButtonItem = menuItem
                } else {
                    let profileIcon = UIBarButtonItem(image: UIImage(named: "profilePlaceholder")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuPressed))
                    let menuItem = UIBarButtonItem(image: UIImage(named: "dotsIcon")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuPressed))
                    
                    navigationItem.leftBarButtonItem = profileIcon
                    navigationItem.rightBarButtonItem = menuItem
                }
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SFSafariViewControllerDelegate {
    
    // MARK: - Métodos de protoclo UICollectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == UI.generalCollection {
            return cellArray1.count
        } else {
            return cellArray2.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == UI.generalCollection {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "general", for: indexPath) as? GeneralCell {
                
                cell.backgroundColor = UIColor.white
                cell.layer.shadowOffset = CGSize(width: -4.0, height: 4.0)
                cell.layer.shadowRadius = 10.0
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOpacity = 0.2
                cell.layer.masksToBounds = false
                cell.layer.cornerRadius = 8.0
                
                cell.viewModel = cellArray1[indexPath.item]
                
                return cell
            } else {
                return UICollectionViewCell()
            }
        } else {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "msi", for: indexPath) as? MsiCell {
                
                cell.backgroundColor = UIColor.white
                cell.layer.shadowOffset = CGSize(width: -4.0, height: 4.0)
                cell.layer.shadowRadius = 10.0
                cell.layer.shadowColor = UIColor.black.cgColor
                cell.layer.shadowOpacity = 0.2
                cell.layer.masksToBounds = false
                cell.layer.cornerRadius = 8.0
                
                cell.viewModel = cellArray2[indexPath.item]
                
                return cell
            } else {
                return UICollectionViewCell()
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wCell = UI.generalCollection.frame.width/3.1
        let hCell = UI.generalCollection.frame.width/3.1
        
        return CGSize(width: wCell, height: hCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == UI.generalCollection {
            switch indexPath.item {
            case 0:
//                filialesNavigation()
                let nextVC = FilialModulesVC()
                nextVC.isFavourite = true
                navigationController?.pushViewController(nextVC, animated: true)
            case 1:
                let nextVC = NotificationsVC()
                navigationController?.pushViewController(nextVC, animated: true)
            default:
                let nextVC = NewsVC()
                navigationController?.pushViewController(nextVC, animated: true)
            }
        } else {
            switch indexPath.item {
            case 0:
                showAlertContact()
            case 1:
                faqNavigation()
            default:
                let url =  URL(string: "https://www.multisistemas.com.mx")
                
                let safariVC = SFSafariViewController(url: url!)
                present(safariVC, animated: true, completion: nil)
                safariVC.delegate = self
            }
        }
    }
    
    // MARK: - Métodos que se ejecutan en los eventos del didSelectItemAt
    func filialesNavigation() {
        
        let nextVC = FilialesTableVC()
        let navigation = UINavigationController(rootViewController: nextVC)
        navigation.navigationBar.tintColor = UIColor.black
        navigation.navigationBar.barTintColor = UIColor.clear
        navigation.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.barStyle = .default
        
        present(navigation, animated: true, completion: nil)
    }
    
    private func faqNavigation() {
        
        let nextVC = FAQTableVC()
        let navigation = UINavigationController(rootViewController: nextVC)
        navigation.navigationBar.tintColor = UIColor.black
        navigation.navigationBar.barTintColor = UIColor.clear
        navigation.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.barStyle = .default
        navigation.navigationBar.prefersLargeTitles = true
        navigation.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.bold)]
        
        present(navigation, animated: true, completion: nil)
    }
    
    func showAlertContact() {
        
        let actionSheet = UIAlertController(title: "Contacto", message: "Contáctanos hoy!", preferredStyle: UIAlertController.Style.actionSheet)
        
        areas.forEach { (area) in
            
            let action = UIAlertAction(title: area.nombre, style: UIAlertAction.Style.default) { (_) in
                let nextVC = ContactVC()
                nextVC.areaModel = area
                nextVC.title = area.nombre
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
            actionSheet.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil)
        
        actionSheet.addAction(cancelAction)

        navigationController?.present(actionSheet, animated: true, completion: nil)
        
    }
    
}

extension HomeVC: GetFilialProtocol {
    
    
    func selectedFilial(filial: FilialModel) {
        
        DBManager.shared.cleanEntity(currentEntity: FavouriteFilialEntity.self)
        
        do {
            try self.insertData(data: filial)
        } catch let error {
            fatalError(error.localizedDescription)
        }
    
        setFavouriteBtn()

    }
    
}

// MARK: - Extensión que contiene las funciones para trabajar con CoreData
extension HomeVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    func insertData(data: FilialModel) throws {
        
        let entity = FavouriteFilialEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.nombre = data.nombre
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func readData() throws -> FilialModel? {
        
        let data = try getContext.fetch(FavouriteFilialEntity.fetchRequest() as NSFetchRequest<FavouriteFilialEntity>)
        
        guard let register = data.first else {
            debugPrint("No se pudo extraer el registro")
            
            return nil
        }
        
        let model = FilialModel(entity: register)
        
        return model
    }
}

// MARK: - Extesión que contiene los métodos del protocolo URL_Session
extension HomeVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_Session
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        if session == networkManager1 {
            print(response)
            AreasModel.shared.getData(dictionary: response) { (status, array, msg) in
                
                if status {
                    if let jsonArray = array {
                        self.areas.removeAll()
                        self.areas = jsonArray
                        self.saveAreas(jsonArray: jsonArray)
                    }
                } else {
                    let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) { (_) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
            }
        } else {
            UpdatePushModel.shared.getData(dict: response) { (status, msg) in
                if status {
                    debugPrint(msg)
                } else {
                    debugPrint(msg)
                }
                LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
            }
        }

    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
        
        // MARK: - Método para obtener la imagen de usuario de CoreData
        func getUserImage() -> Data? {
            
            do {
                let data = try getImageDataFromCoreData()
                
                if let info = data {
                    if let imageData = info.profile_image {
                        return imageData
                    } else {
                        return nil
                    }
    //                if let imageData = info.profile_image {
    //                    UI.avarImage.image = UIImage(data: imageData)
    //                } else {
    //                    UI.avarImage.image = UIImage(named: "avatarPlaceholder")
    //                }
                } else {
                    debugPrint("No hay registro en CoreData")
                    return nil
                }
            } catch let error {
                debugPrint(error.localizedDescription)
                return nil
            }
        }
        
        func getImageDataFromCoreData() throws -> ProfileImageEntity? {
            
            let data = try getContext.fetch(ProfileImageEntity.fetchRequest() as NSFetchRequest<ProfileImageEntity>)
            
            guard let register = data.first else {
                debugPrint("No se pudo extraer el registro")
                
                return nil
            }
            
            
            return register
        }
    
    func saveAreas(jsonArray: [AreasModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: AreasEntity.self)
            for item in jsonArray {
                do {
                    try insertArea(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getAreas() {
        
        do {
            let data = try getAreaData()
            
            if let areasArray = data {
                self.areas.removeAll()
                self.areas = areasArray
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertArea(data: AreasModel) throws {
        
        let entity = AreasEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.nombre = data.nombre
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getAreaData() throws -> [AreasModel]? {
        
        var jsonArray = [AreasModel]()
        let data = try getContext.fetch(AreasEntity.fetchRequest() as NSFetchRequest<AreasEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = AreasModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
}
