//
//  LoginExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 8/26/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

extension LoginVC: UITextFieldDelegate {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Métodos de protocolo UITetxfieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField.tag {
        case 1:
            interface.passField.becomeFirstResponder()
        default:
            interface.passField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 0 {
            addDoneButtonOnKeyboard()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch textField {
        case interface.employeeNumberField:
            textField.layer.borderColor = UIColor.clear.cgColor
            textField.attributedPlaceholder = NSAttributedString(string: "RFC", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 13
        case interface.mailField:
            textField.layer.borderColor = UIColor.clear.cgColor
            textField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        case interface.passField:
            textField.layer.borderColor = UIColor.clear.cgColor
            textField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        default:
            debugPrint("No existe el textfield")
        }
        
        return true
    }
    
    // MARK: - Método para agregar el bottón de intro al teclado numérico
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItem.Style.done, target: self, action: #selector(cancelAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Aceptar", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneAction))
        
        var items = [UIBarButtonItem]()
        items.append(cancel)
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        interface.employeeNumberField.inputAccessoryView = doneToolbar
        
    }
    
    @objc func cancelAction() {
        
        interface.employeeNumberField.resignFirstResponder()
    }
    
    @objc func doneAction() {
        
        interface.mailField.becomeFirstResponder()
    }
}

extension LoginVC: SFSafariViewControllerDelegate {
    
    // MARK: - Método para abrir una URL utilizando SafariViewcontroller
    func openURL(link: String?) {
        
        if let url: URL = URL(string: link ?? "https://www.apple.com/mx/") {
            let safariVC = SFSafariViewController(url: url)
            present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        } else {
            debugPrint("Ocurrió un error al obtener el valor de la liga")
        }
    }
}

extension LoginVC: URL_SessionDelegate {
    
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        LoginModel.shared.getData(dict: response) { (status, value) in
            
            if status {
                self.interface.employeeNumberField.text = ""
                self.interface.mailField.text = ""
                self.interface.passField.text = ""
                KeychainService.savePassword(service: KeychainValues.service, account: KeychainValues.account, data: value)
                
                let homeVC = HomeVC()
                let navigationVC = UINavigationController(rootViewController: homeVC)
                navigationVC.modalPresentationStyle = .currentContext
                navigationVC.view.backgroundColor = UIColor.white
                navigationVC.navigationBar.barTintColor = UIColor.white
                navigationVC.navigationBar.tintColor = UIColor.black
                navigationVC.navigationBar.isTranslucent = false
                //        navigationVC.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                navigationVC.navigationBar.shadowImage = UIImage()
                navigationVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)]
                self.present(navigationVC, animated: true) {
                    self.interface.mailField.resignFirstResponder()
                    self.interface.passField.resignFirstResponder()
                    self.interface.employeeNumberField.resignFirstResponder()
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: value, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
        
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}
