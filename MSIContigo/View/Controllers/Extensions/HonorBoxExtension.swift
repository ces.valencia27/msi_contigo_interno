//
//  HonorBoxExtension.swift
//  MSIContigo
//
//  Created by EON on 9/25/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension HonorBoxCollectionVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        HonorBoxModel.shared.getData(dictionary: response) { (status, model, msg) in
            
            if status {
                if let modelo = model {
                    
                    self.peopleArray.removeAll()
                    self.peopleArray = modelo.personas
                    self.pagingArray = self.peopleArray.prefix(5)
                    self.peopleArray = Array(self.peopleArray.dropFirst(5))
                    self.collectionView.reloadData()
                    self.savePeople(jsonArray: model?.personas ?? [])
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) { (_) in
                    self.navigationController?.popViewController(animated: true)
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Extensión que contiene los métodos para trabajar con CoreData en este controlador
extension HonorBoxCollectionVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func savePeople(jsonArray: [PersonModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: PersonEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getPeople() {
        
        do {
            let data = try getData()
            
            if let persons = data {
                self.peopleArray.removeAll()
                self.peopleArray = persons
                self.pagingArray = self.peopleArray.prefix(5)
                self.peopleArray = Array(self.peopleArray.dropFirst(5))
                self.collectionView.reloadData()
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: PersonModel) throws {

        let entity = PersonEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.nombre = data.nombre
        entity.fecha = data.fecha
        entity.imagen = data.imagen
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [PersonModel]? {
        
        var jsonArray = [PersonModel]()
        let data = try getContext.fetch(PersonEntity.fetchRequest() as NSFetchRequest<PersonEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = PersonModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
    
}
