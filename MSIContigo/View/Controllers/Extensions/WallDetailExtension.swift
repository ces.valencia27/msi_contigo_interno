//
//  WallDetailExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 9/16/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension MuralDetailVC: UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        previewingContext.sourceRect = UI.zoomBtn.frame
        
        let nextVC = BannerVC()
        nextVC.currentImage = UI.bannerImage.image
        
        return nextVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        let nav = UINavigationController(rootViewController: viewControllerToCommit)
        nav.navigationBar.tintColor = UIColor.msiBlue
        
        self.present(nav, animated: true, completion: nil)
    }
    
}

extension MuralDetailVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        WallDetailModel.shared.getData(dictionary: response) { (status, model, msg) in
            
            if status {
                guard let modelo = model else {
                    fatalError("Error")
                }
                self.currentMural = modelo
                let viewModel = WallDetailViewModel(detail: modelo)
                self.UI.modelView = viewModel
                self.saveWallDetail(dataModel: modelo)
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        print(error.localizedDescription)
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Extensión que contiene las funciones para trabajar con CoreData
extension MuralDetailVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveWallDetail(dataModel: WallDetailModel) {
        
        DBManager.shared.cleanEntity(currentEntity: WallDetailEntity.self)
        do {
            try insertData(data: dataModel)
            debugPrint("Registro insertado")
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getWallDetail() {
        
        do {
            let data = try getData()
            
            if let modelo = data {
                let viewModel = WallDetailViewModel(detail: modelo)
                self.UI.modelView = viewModel
            } else {
                debugPrint("No hay registro en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: WallDetailModel) throws {

        let entity = WallDetailEntity(context: getContext)
        entity.titulo = data.titulo
        entity.subtitulo = data.subtitulo
        entity.contenido = data.contenido
        entity.fecha = data.fecha
        entity.filial = data.filial
        entity.imagen = data.imagen
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> WallDetailModel? {
        
        let data = try getContext.fetch(WallDetailEntity.fetchRequest() as NSFetchRequest<WallDetailEntity>)
        
        guard let register = data.first else {
            debugPrint("No se pudo extraer el registro")
            
            return nil
        }
        
        let model = WallDetailModel(entity: register)
        
        return model
    }
}

