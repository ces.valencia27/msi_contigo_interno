//
//  NewsExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 8/31/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension NewsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: - Métodos de protocolo UICollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if filterIsActive {
            return filterArray.count
        } else {
            return newsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if filterIsActive {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! NewsCell
            cell.modelView = NewsViewModel(noticia: filterArray[indexPath.item])
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! NewsCell
            cell.modelView = NewsViewModel(noticia: newsArray[indexPath.item])
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wCell = UI.newsCollection.frame.width
        let hCell = UI.newsCollection.frame.height/6
        
        return CGSize(width: wCell, height: hCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if filterIsActive {
            let nextVC = NewsDetailVC()
            nextVC.new = filterArray[indexPath.item]
            navigationController?.pushViewController(nextVC, animated: true)
        } else {
            let nextVC = NewsDetailVC()
            nextVC.new = newsArray[indexPath.item]
            navigationController?.pushViewController(nextVC, animated: true)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize.zero
//        if fakeBolean != true {
//            return CGSize(width: self.view.frame.width, height: self.view.frame.height * 0.2)
//        } else {
//            return CGSize.zero
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionFooter:
            
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footer", for: indexPath) as! NewsFooterSection
            footerView.showMoreBtn.addTarget(self, action: #selector(showMorePressed), for: UIControl.Event.touchUpInside)

            return footerView
            
        default:
            debugPrint("No se agrega reusableView")
            return UICollectionReusableView()
        }
    }
    
    @objc func showMorePressed() {
        
        if newsArray.count > 0 {
            
            if filterIsActive {
                let newItems = self.pagingArray.prefix(5)
                self.filterArray.append(contentsOf: newItems)
                self.pagingArray = self.pagingArray.dropFirst(5)
                self.UI.newsCollection.reloadData()
            } else {
                let newItems = self.newsArray.prefix(5)
                self.pagingArray.append(contentsOf: newItems)
                self.newsArray = Array(self.newsArray.dropFirst(5))
                self.UI.newsCollection.reloadData()
            }

        } else {
            fakeBolean = true
            self.UI.newsCollection.reloadData()
        }

    }
}

extension NewsVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_Session
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        print(response)
        NewsModel.shared.getData(dictionary: response) { (status, array, msg) in
            
            if status {
                if let jsonArray = array {
                    self.newsArray.removeAll()
                    self.newsArray = jsonArray
//                    self.pagingArray = self.newsArray.prefix(5)
//                    self.newsArray = Array(self.newsArray.dropFirst(5))
                    self.UI.newsCollection.reloadData()
                    self.saveNews(jsonArray: jsonArray)
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) { (_) in
                    self.navigationController?.popViewController(animated: true)
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension NewsVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveNews(jsonArray: [NewsModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: NewsEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getNews() {
        
        do {
            let data = try getData()
            
            if let news = data {
                if filterIsActive {
//                    self.pagingArray.removeAll()
//                    self.pagingArray = news[0..<news.count]
//                    self.filterArray = self.pagingArray.prefix(5)
//                    self.pagingArray = self.pagingArray.dropFirst(5)
                    self.UI.newsCollection.reloadData()
                } else {
                    self.newsArray.removeAll()
                    self.newsArray = news
//                    self.pagingArray = self.newsArray.prefix(5)
//                    self.newsArray = Array(self.newsArray.dropFirst(5))
                    self.UI.newsCollection.reloadData()
                }

            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: NewsModel) throws {
        
        let entity = NewsEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.titulo = data.titulo
        entity.fecha = data.fecha
        entity.imagen = data.imagen
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [NewsModel]? {
        
        var jsonArray = [NewsModel]()
        let data = try getContext.fetch(NewsEntity.fetchRequest() as NSFetchRequest<NewsEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = NewsModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
}
