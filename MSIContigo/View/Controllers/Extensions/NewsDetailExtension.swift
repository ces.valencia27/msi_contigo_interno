//
//  NewsDetailExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 8/31/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension NewsDetailVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        print(response)
        NewsDetailModel.shared.getData(dictionary: response) { (status, model, msg) in
            
            if status {
                guard let modelo = model else {
                    fatalError("Error")
                }
                let viewModel = NewsDetailViewModel(detail: modelo)
                self.UI.modelView = viewModel
                self.saveNewsDetail(dataModel: modelo)
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        print(error.localizedDescription)
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Extensión que contiene las funciones para trabajar con CoreData
extension NewsDetailVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveNewsDetail(dataModel: NewsDetailModel) {
        
        DBManager.shared.cleanEntity(currentEntity: NewsDetailEntity.self)
        do {
            try insertData(data: dataModel)
            debugPrint("Registro insertado")
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getNewsDetail() {
        
        do {
            let data = try getData()
            
            if let modelo = data {
                let viewModel = NewsDetailViewModel(detail: modelo)
                self.UI.modelView = viewModel
            } else {
                debugPrint("No hay registro en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: NewsDetailModel) throws {

        let entity = NewsDetailEntity(context: getContext)
        entity.titulo = data.titulo
        entity.contenido = data.contenido
        entity.autor = data.autor
        entity.categoria = data.categoria
        entity.fecha = data.fecha
        entity.filial = data.filial
        entity.imagen = data.imagen
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> NewsDetailModel? {
        
        let data = try getContext.fetch(NewsDetailEntity.fetchRequest() as NSFetchRequest<NewsDetailEntity>)
        
        guard let register = data.first else {
            debugPrint("No se pudo extraer el registro")
            
            return nil
        }
        
        let model = NewsDetailModel(entity: register)
        
        return model
    }
}
