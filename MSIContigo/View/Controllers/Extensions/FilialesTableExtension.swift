//
//  FilialesTableExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 9/16/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension FilialesTableVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_Session
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        print(response)
        FilialModel.shared.getData(dictionary: response) { (status, jsonArray, msg) in
            if status {
                if let data = jsonArray {
                    self.filialsArray.removeAll()
                    self.filialsArray = data
                    self.tableView.reloadData()
                    self.saveFilials(jsonArray: data)
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Extensión que contiene los métodos para trabajar con CoreData en este controlador
extension FilialesTableVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveFilials(jsonArray: [FilialModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: FilialEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getFilials() {
        
        do {
            let data = try getData()
            
            if let filiales = data {
                self.filialsArray.removeAll()
                self.filialsArray = filiales
                self.tableView.reloadData()
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: FilialModel) throws {
        
        let entity = FilialEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.nombre = data.nombre
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [FilialModel]? {
        
        var jsonArray = [FilialModel]()
        let data = try getContext.fetch(FilialEntity.fetchRequest() as NSFetchRequest<FilialEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = FilialModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
    
}
