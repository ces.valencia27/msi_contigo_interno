//
//  ContactExtension.swift
//  MSIContigo
//
//  Created by EON on 9/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

extension ContactVC: URL_SessionDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Placeholder"
            textView.textColor = UIColor.lightGray
        }
    }

    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        NewMessageModel.shared.getData(dict: response) { (status, value) in
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
            if status {
                let alert = UIAlertController(title: "Aviso", message: value, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) { (_) in
                    self.navigationController?.popViewController(animated: true)
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Aviso", message: value, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }

        }
        
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
