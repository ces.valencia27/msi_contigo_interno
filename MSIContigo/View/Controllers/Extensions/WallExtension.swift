//
//  WallExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 9/14/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension WallVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Métodos de protovcolo UICollectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return wallArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wallCell", for: indexPath) as! WallCell
        
        cell.modelView = WallViewModel(wall: wallArray[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wCell = UI.wallCollection.frame.width
        let hCell = UI.wallCollection.frame.height
        
        return CGSize(width: wCell, height: hCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
//        let nextVC = MuralDetailVC()
//        nextVC.muralID = wallArray[indexPath.item].id
//        navigationController?.pushViewController(nextVC, animated: true)
        
        let urlString = Environment.baseURL.absoluteString + wallArray[indexPath.item].imagen
        if let url = URL(string: urlString) {
//            bannerImage.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
            let nextVC = BannerVC()
            nextVC.image.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
            let nav = UINavigationController(rootViewController: nextVC)
            nav.navigationBar.barTintColor = UIColor.white
            nav.navigationBar.tintColor = UIColor.msiBlue
            
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    // Método para ejecutar el cambio de página en el pageControl
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { (_) in
            self.UI.wallCollection.collectionViewLayout.invalidateLayout()
            
            if self.UI.pageControl.currentPage == 0 {
                self.UI.wallCollection.contentOffset = CGPoint.zero
            } else {
                let indexPath = IndexPath(item: self.UI.pageControl.currentPage, section: 0)
                self.UI.wallCollection.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
            }
        }, completion: nil)
    }
}

extension WallVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_Session
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
    
        WallModel.shared.getData(dictionary: response) { (status, array, msg) in
            
            if status {
                if let jsonArray = array {
                    self.wallArray.removeAll()
                    self.wallArray = jsonArray
                    self.UI.wallCollection.reloadData()
                    self.UI.pageControl.numberOfPages = jsonArray.count
                    self.saveWalls(jsonArray: jsonArray)
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default) { (_) in
                    self.navigationController?.popViewController(animated: true)
                }
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Extensión que contiene los métodos para trabajar con CoreData en este controlador
extension WallVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveWalls(jsonArray: [WallModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: WallEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getWalls() {
        
        do {
            let data = try getData()
            
            if let walls = data {
                self.wallArray.removeAll()
                self.wallArray = walls
                self.UI.wallCollection.reloadData()
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: WallModel) throws {

        let entity = WallEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.titulo = data.titulo
        entity.subtitulo = data.subtitulo
        entity.filial = data.filial
        entity.imagen = data.imagen
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [WallModel]? {
        
        var jsonArray = [WallModel]()
        let data = try getContext.fetch(WallEntity.fetchRequest() as NSFetchRequest<WallEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = WallModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
    
}
