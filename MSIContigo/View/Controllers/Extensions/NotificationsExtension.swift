//
//  NotificationsExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 9/16/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension NotificationsVC: URL_SessionDelegate {
    
    //MARK: - Métodos de protocolo URL_Session
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        AddModel.shared.getData(dictionary: response) { (status, array, msg) in
            
            if status {
                if let jsonArray = array {
                    self.avisosArray.removeAll()
                    self.avisosArray = jsonArray
                    self.tableView.reloadData()
                    self.saveAds(jsonArray: jsonArray)
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Extensión que contiene los métodos para trabajar con CoreData en este controlador
extension NotificationsVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveAds(jsonArray: [AddModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: AddEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getAds() {
        
        do {
            let data = try getData()
            
            if let adds = data {
                self.avisosArray.removeAll()
                self.avisosArray = adds
                self.tableView.reloadData()
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: AddModel) throws {

        let entity = AddEntity(context: getContext)
        entity.emisor = data.emisor
        entity.titulo = data.titulo
        entity.mensaje = data.mensaje
        entity.fecha = data.fecha
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [AddModel]? {
        
        var jsonArray = [AddModel]()
        let data = try getContext.fetch(AddEntity.fetchRequest() as NSFetchRequest<AddEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = AddModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
    
}
