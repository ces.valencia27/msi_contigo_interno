//
//  HonorBoxDetailExtension.swift
//  MSIContigo
//
//  Created by EON on 9/25/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension HonorBoxDetailVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        if session == networkManager1 {
            HonorDetailModel.shared.getData(dictionary: response) { (status, model, msg) in
                
                if status {
                    guard let modelo = model else {
                        fatalError("Error")
                    }
                    let viewModel = HonorBoxDetailViewModel(detail: modelo)
                    self.UI.modelView = viewModel
                    self.saveHonorDetail(dataModel: modelo)
                    self.unixID = Int(modelo.fecha_unix)
                } else {
                    let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        } else {
            LastHonorDetailModel.shared.getData(dictionary: response) { (status, model, msg) in
                
                if status {
                    guard let modelo = model else {
                        fatalError("Error")
                    }
                    let viewModel = HonorBoxDetailViewModel(detail: modelo)
                    self.UI.modelView = viewModel
                    self.saveHonorDetail(dataModel: modelo)
                    self.unixID = Int(modelo.fecha_unix)
                } else {
                    let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        print(error.localizedDescription)
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Extensión que contiene las funciones para trabajar con CoreData
extension HonorBoxDetailVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveHonorDetail(dataModel: HonorDetailModel) {
        
        DBManager.shared.cleanEntity(currentEntity: HonorDetailEntity.self)
        do {
            try insertData(data: dataModel)
            debugPrint("Registro insertado")
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getHonorDetail() {
        
        DBManager.shared.cleanEntity(currentEntity: WallDetailEntity.self)
        do {
            let data = try getData()
            
            if let modelo = data {
                let viewModel = HonorBoxDetailViewModel(detail: modelo)
                self.UI.modelView = viewModel
            } else {
                debugPrint("No hay registro en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: HonorDetailModel) throws {

        let entity = HonorDetailEntity(context: getContext)
        entity.nombre = data.nombre
        entity.descripcion = data.descripcion
        entity.anio = data.anio
        entity.mes = data.mes
        entity.imagen = data.imagen
        entity.fecha_unix = Int64(data.fecha_unix)
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> HonorDetailModel? {
        
        let data = try getContext.fetch(HonorDetailEntity.fetchRequest() as NSFetchRequest<HonorDetailEntity>)
        
        guard let register = data.first else {
            debugPrint("No se pudo extraer el registro")
            
            return nil
        }
        
        let model = HonorDetailModel(entity: register)
        
        return model
    }
}

