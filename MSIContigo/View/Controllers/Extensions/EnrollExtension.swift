//
//  EnrollExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import CoreData

// MARK: Extensión que contiene los métodos del protocolo UIPickerViewDelegate y DataSource
extension EnrollVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return filialsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return filialsArray[row].nombre
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        filialID = filialsArray[row].id
        UI.filialField.text = filialsArray[row].nombre
    }
    
}

extension EnrollVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == UI.employeeNumberField {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 13
        } else {
            return true
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Métodos de protocolo UITetxfieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField.tag {
        case 0:
            UI.mailField.becomeFirstResponder()
        default:
            UI.mailField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == UI.filialField {
            UI.filialPicker.selectRow(0, inComponent: 0, animated: true)
        }
    }
    
    // MARK: - Método para agregar el bottón de intro al teclado numérico
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItem.Style.done, target: self, action: #selector(cancelAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Aceptar", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneAction))
        
        var items = [UIBarButtonItem]()
        items.append(cancel)
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        UI.employeeNumberField.inputAccessoryView = doneToolbar
        UI.filialField.inputView = UI.filialPicker
        UI.filialField.inputAccessoryView = doneToolbar
    }
    
    @objc func cancelAction() {
        
        UI.employeeNumberField.resignFirstResponder()
    }
    
    @objc func doneAction() {
        
        if UI.employeeNumberField.isFirstResponder {
            UI.mailField.becomeFirstResponder()
        } else if UI.mailField.isFirstResponder {
            UI.filialField.becomeFirstResponder()
        } else {
            UI.filialField.resignFirstResponder()
        }
    }
    
}


extension EnrollVC: SFSafariViewControllerDelegate {
    
    // MARK: - Método para abrir una URL utilizando SafariViewcontroller
    func openURL(link: String?) {
        
        if let url: URL = URL(string: link ?? "https://www.apple.com/mx/") {
            let safariVC = SFSafariViewController(url: url)
            present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        } else {
            debugPrint("Ocurrió un error al obtener el valor de la liga")
        }
    }
}

extension EnrollVC: URL_SessionDelegate {
    
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        if session == getFilialsManager {
            FilialModel.shared.getData(dictionary: response) { (status, jsonArray, msg) in
                if status {
                    if let data = jsonArray {
                        self.filialsArray.removeAll()
                        self.filialsArray = data
                        self.saveFilials(jsonArray: data)
                    }
                } else {
                    let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
            }
        } else {
            NewSolicitudModel.shared.getData(dict: response) { (status, value) in
                
                if status {
                    LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
                    let alert = UIAlertController(title: "Aviso", message: value, preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Aviso", message: value, preferredStyle: UIAlertController.Style.alert)
                    let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
                LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
            }
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: Extensión que contiene los métodos para trabajar con coredata
extension EnrollVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveFilials(jsonArray: [FilialModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: FilialEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getFilials() {
        
        do {
            let data = try getData()
            
            if let filiales = data {
                self.filialsArray.removeAll()
                self.filialsArray = filiales
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: FilialModel) throws {
        
        let entity = FilialEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.nombre = data.nombre
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [FilialModel]? {
        
        var jsonArray = [FilialModel]()
        let data = try getContext.fetch(FilialEntity.fetchRequest() as NSFetchRequest<FilialEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = FilialModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
}
