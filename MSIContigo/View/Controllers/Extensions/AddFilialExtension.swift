//
//  AddFilialExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension AddFilialVC: UITableViewDelegate, UITableViewDataSource {

    // MARK: - Métodos de protocolo UITabelView
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filialsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "selectFilialCell", for: indexPath) as! FilialSelectedCell
        
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.modelView = FilialViewModel(filial: filialsArray[indexPath.item])

        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.02)
        cell.selectedBackgroundView = backgroundView
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerTitle = CustomLabel()
        headerTitle.text = "ELIGE TU FILIAL"
        headerTitle.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        headerTitle.textColor = UIColor.customGray3
        headerTitle.backgroundColor = UIColor.clear
        headerTitle.numberOfLines = 0
        
        return headerTitle
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        selectedFilial = filialsArray[indexPath.item]
        
        UI.flag = true
//        if let cell = tableView.cellForRow(at: indexPath) as? FilialSelectedCell {
//            selectedFilial = filialsArray[indexPath.item].nombre
//        } else {
//            selectedFilial = ""
//        }
        
    }
    
}

extension AddFilialVC: URL_SessionDelegate {
    
    // MARK: - Métodos de protocolo URL_Session
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        FilialModel.shared.getData(dictionary: response) { (status, jsonArray, msg) in
            if status {
                if let data = jsonArray {
                    self.filialsArray.removeAll()
                    self.filialsArray = data
                    self.UI.tableView.reloadData()
                    self.saveFilials(jsonArray: data)
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: msg, preferredStyle: UIAlertController.Style.alert)
                let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
            LoaderView.shared().hideActivityIndicator(uiView: self.navigationController!.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: navigationController!.view)
        let alert = UIAlertController(title: "Aviso", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Extensión que contiene los métodos para trabajar con CoreData en este controlador
extension AddFilialVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para recorrer el arreglo e insertar los registros en CoreData
    func saveFilials(jsonArray: [FilialModel]) {
        
        if jsonArray.count > 0 {
            DBManager.shared.cleanEntity(currentEntity: FilialEntity.self)
            for item in jsonArray {
                do {
                    try insertData(data: item)
                    debugPrint("Registro insertado")
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            }
        } else {
            debugPrint("El arreglo no tiene elementos")
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getFilials() {
        
        do {
            let data = try getData()
            
            if let filiales = data {
                self.filialsArray.removeAll()
                self.filialsArray = filiales
                self.UI.tableView.reloadData()
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func insertData(data: FilialModel) throws {
        
        let entity = FilialEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.nombre = data.nombre
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func getData() throws -> [FilialModel]? {
        
        var jsonArray = [FilialModel]()
        let data = try getContext.fetch(FilialEntity.fetchRequest() as NSFetchRequest<FilialEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = FilialModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
    
}
