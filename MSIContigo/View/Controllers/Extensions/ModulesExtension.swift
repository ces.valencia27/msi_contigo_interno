//
//  ModulesExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 9/3/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension FilialModulesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return modulesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "modulesCell", for: indexPath) as! ModulesCell
        cell.layer.cornerRadius = 6.0
        cell.backgroundColor = UIColor.customWhite1
        cell.model = modulesArray[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top ?? CGFloat(0.0)
            
            if topPadding > 20.0 {
                let wCell = UI.modulesCollection.frame.width/2 - 10
                let hCell = UI.modulesCollection.frame.height * 0.36
                
                return CGSize(width: wCell, height: hCell)
            } else {
                let wCell = UI.modulesCollection.frame.width/2 - 10
                let hCell = UI.modulesCollection.frame.height * 0.45
                
                return CGSize(width: wCell, height: hCell)
            }
        } else {
            return CGSize(width: 0.0, height: 0.0)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.item {
        case 0:
            let nextVC = NewsVC()
            nextVC.isFilial = true
            nextVC.filialID = currentFilial?.id ?? 0
            navigationController?.pushViewController(nextVC, animated: true)
        case 1:
            let nextVC = WallVC()
            nextVC.isFilial = true
            nextVC.filialID = currentFilial?.id ?? 0
            navigationController?.pushViewController(nextVC, animated: true)
        case 2:
            let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
            layout.scrollDirection = UICollectionView.ScrollDirection.vertical
            
            let nextVC = HonorBoxCollectionVC(collectionViewLayout: layout)
            nextVC.filialID = currentFilial?.id ?? 0
            navigationController?.pushViewController(nextVC, animated: true)
        default:
            showAlertContact()
        }
    }
    
    func showAlertContact() {
        
        let actionSheet = UIAlertController(title: "Contacto", message: "Contáctanos hoy!", preferredStyle: UIAlertController.Style.actionSheet)
        
        areas.forEach { (area) in
            
            let action = UIAlertAction(title: area.nombre, style: UIAlertAction.Style.default) { (_) in
                let nextVC = ContactVC()
                nextVC.areaModel = area
                nextVC.title = area.nombre
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
            actionSheet.addAction(action)
        }

        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil)
        
        actionSheet.addAction(cancelAction)

        navigationController?.present(actionSheet, animated: true, completion: nil)
    }
    
}

extension FilialModulesVC: GetFilialProtocol {
    
    func selectedFilial(filial: FilialModel) {
        
        DBManager.shared.cleanEntity(currentEntity: FavouriteFilialEntity.self)
        
        do {
            try self.insertData(data: filial)
        } catch let error {
            fatalError(error.localizedDescription)
        }
        
        setFavouriteBtn()
        
    }
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    func insertData(data: FilialModel) throws {
        
        let entity = FavouriteFilialEntity(context: getContext)
        entity.id = Int64(data.id)
        entity.nombre = data.nombre
        
        getContext.insert(entity)
        
        try getContext.save()
        
        print("Se almacenó el registro correctamente")
    }
    
    func readData() throws -> FilialModel? {
        
        let data = try getContext.fetch(FavouriteFilialEntity.fetchRequest() as NSFetchRequest<FavouriteFilialEntity>)
        
        guard let register = data.first else {
            debugPrint("No se pudo extraer el registro")
            
            return nil
        }
        
        let model = FilialModel(entity: register)
        
        return model
    }
    
    // MARK: - Método para extraer información de CoreData y establecer el valor inicial del botón
    public func setFavouriteBtn() {
        do {
            let obj = try readData()
            
            if let filial = obj {
                debugPrint("Si hay registro =)")
                UI.addFilialBtn.setAttributedTitle(NSAttributedString(string: filial.nombre, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
                
                currentFilial = filial
                if let filialName = UserDefaults.standard.string(forKey: "filial_name") {
                    navigationItem.title = filialName
                }
            } else {
                debugPrint("No existe registro")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: Método para extraer las filiales de CoreData y recargar la tabla con los valores locales
    func getAreas() {
        
        do {
            let data = try getAreaData()
            
            if let areasArray = data {
                self.areas.removeAll()
                self.areas = areasArray
            } else {
                debugPrint("No hay filiales en CoreData")
            }
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    func getAreaData() throws -> [AreasModel]? {
        
        var jsonArray = [AreasModel]()
        let data = try getContext.fetch(AreasEntity.fetchRequest() as NSFetchRequest<AreasEntity>)
        
        if data.count > 0 {
            for item in data {
                let model = AreasModel(entity: item)
                jsonArray.append(model)
            }
            return jsonArray
        } else{
            debugPrint("No hay registros")
            return nil
        }
    }
}
