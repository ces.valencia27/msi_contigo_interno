//
//  WallVC.swift
//  MSIContigo
//
//  Created by David Valencia on 9/14/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

private let reuseIdentifier = "wallCell"

class WallVC: UIViewController {
    
    let UI: WallView = {
        let UI = WallView(frame: CGRect.zero)
        UI.backgroundColor = UIColor.white
        UI.translatesAutoresizingMaskIntoConstraints = false
        return UI
    }()
    
    var filialID: Int = 0
    var isFilial: Bool = false
    var wallArray = [WallModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        setupUI()
    }
    
    // MARK: - Método para cargar la interfaz gráfica del controlador
    private func setupUI() {
        
        setupNavBar()
        setSubviews()
        setAutolayout()
        setDelegates()
        validateConnection()
    }
    
    // MARK: - Método para configurar los componentes de la navigationBar
    private func setupNavBar() {
        
        navigationItem.title = "Mural"
        
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        view.addSubview(UI)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            UI.topAnchor.constraint(equalTo: view.topAnchor),
            UI.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            UI.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            UI.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Método para asignar los delegados
    private func setDelegates() {
        
        UI.wallCollection.delegate = self
        UI.wallCollection.dataSource = self
    }
    
    // Método para detectar cuando el usuario termina de hacer scroll
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let x = targetContentOffset.pointee.x
        UI.pageControl.currentPage = Int(x/view.frame.width)
    }
    
    // MARK: - Método para hacer la ptición al servicio de getMurales
    private func request() {
        
        let parameters = WallRequestModel()
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getMuralesRequest(parameters)
        LoaderView.shared().showActivityIndicatory(uiView: navigationController!.view)
    }
    
    // MARK: - Método para validar si existe conexión a internet y ejecutar el flujo correspondiente
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            getWalls()
        }
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema con el manejo de memoria")
    }

}

