//
//  ContactView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class ContactView: UIView {
    
    let textField1: CustomTextField = {
        let textField1 = CustomTextField(frame: CGRect.zero)
        textField1.layer.masksToBounds = true
        textField1.layer.cornerRadius = 5.0
        textField1.attributedPlaceholder = NSAttributedString(string: "Asunto", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        textField1.textColor = UIColor.customBlack1
        textField1.backgroundColor = UIColor.customWhite1
        textField1.keyboardType = UIKeyboardType.default
        textField1.keyboardAppearance = UIKeyboardAppearance.dark
        textField1.tag = 0
        textField1.translatesAutoresizingMaskIntoConstraints = false
        return textField1
    }()
    
    let textField2: CustomTextField = {
        let textField2 = CustomTextField(frame: CGRect.zero)
        textField2.layer.masksToBounds = true
        textField2.layer.cornerRadius = 5.0
        textField2.attributedPlaceholder = NSAttributedString(string: "Filial", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        textField2.textColor = UIColor.customBlack1
        textField2.backgroundColor = UIColor.customWhite1
        textField2.keyboardType = UIKeyboardType.default
        textField2.keyboardAppearance = UIKeyboardAppearance.dark
        textField2.tag = 1
        textField2.translatesAutoresizingMaskIntoConstraints = false
        return textField2
    }()
    
    let messageField: UITextView = {
        let messageField = UITextView(frame: CGRect.zero)
        messageField.layer.masksToBounds = true
        messageField.layer.cornerRadius = 5.0
        messageField.text = "Comentario"
        messageField.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
        messageField.textColor = UIColor.lightGray
        messageField.backgroundColor = UIColor.customWhite1
        messageField.keyboardType = UIKeyboardType.default
        messageField.keyboardAppearance = UIKeyboardAppearance.dark
        messageField.tag = 2
        messageField.translatesAutoresizingMaskIntoConstraints = false
        return messageField
    }()
    
    let sendBtn: CustomButton3 = {
        let sendBtn = CustomButton3(type: UIButton.ButtonType.custom)
        sendBtn.setAttributedTitle(NSAttributedString(string: "Enviar", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
        sendBtn.tag = 0
        sendBtn.translatesAutoresizingMaskIntoConstraints = false
        return sendBtn
    }()
    
    let faqBtn: UIButton = {
        let faqBtn = UIButton(type: UIButton.ButtonType.custom)
        faqBtn.setAttributedTitle(NSAttributedString(string: "Preguntas frecuentes", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium), NSAttributedString.Key.foregroundColor : UIColor.customGray2]), for: UIControl.State.normal)
        faqBtn.tag = 1
        faqBtn.translatesAutoresizingMaskIntoConstraints = false
        return faqBtn
    }()
    
    let stack: UIStackView = {
        let stack = UIStackView(frame: CGRect.zero)
        stack.axis = NSLayoutConstraint.Axis.vertical
        stack.distribution = UIStackView.Distribution.fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    let callBtn: UIButton = {
        let callBtn = UIButton(type: UIButton.ButtonType.custom)
        callBtn.frame = CGRect.zero
        callBtn.setAttributedTitle(NSAttributedString(string: "Llamar", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.customGreen1]), for: UIControl.State.normal)
        callBtn.layer.cornerRadius = 5.0
        callBtn.layer.masksToBounds = true
        callBtn.setImage(UIImage(named:"phoneIcon"), for: .normal)
        callBtn.titleLabel?.adjustsFontSizeToFitWidth = true
//        callBtn.imageEdgeInsets = UIEdgeInsets(top: 6,left: 100,bottom: 6,right: 0)
//        callBtn.titleEdgeInsets = UIEdgeInsets(top: 0,left: -30,bottom: 0,right: 20)
        callBtn.backgroundColor = UIColor.customWhite1
        callBtn.tag = 2
        callBtn.translatesAutoresizingMaskIntoConstraints = false
        return callBtn
    }()
    
    let msjBtn: UIButton = {
        let msjBtn = UIButton(type: UIButton.ButtonType.custom)
        msjBtn.frame = CGRect.zero
        msjBtn.setAttributedTitle(NSAttributedString(string: "Escribir", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.customGreen1]), for: UIControl.State.normal)
        msjBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        msjBtn.layer.cornerRadius = 5.0
        msjBtn.layer.masksToBounds = true
        msjBtn.setImage(UIImage(named:"messageIcon"), for: .normal)
        msjBtn.backgroundColor = UIColor.customWhite1
        msjBtn.tag = 3
        msjBtn.translatesAutoresizingMaskIntoConstraints = false
        return msjBtn
    }()
    
    let footerStack: UIStackView = {
        let footerStack = UIStackView(frame: CGRect.zero)
        footerStack.axis = NSLayoutConstraint.Axis.horizontal
        footerStack.spacing = 10
        footerStack.distribution = UIStackView.Distribution.fillEqually
        footerStack.translatesAutoresizingMaskIntoConstraints = false
        return footerStack
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
//        self.addSubview(textField1)
//        self.addSubview(textField2)
        self.addSubview(messageField)
        self.addSubview(stack)
        stack.addArrangedSubview(sendBtn)
        stack.addArrangedSubview(faqBtn)
        self.addSubview(callBtn)
        footerStack.addArrangedSubview(callBtn)
        footerStack.addArrangedSubview(msjBtn)
        self.addSubview(footerStack)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
//        NSLayoutConstraint.activate([
//            textField1.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
//            textField1.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
//            textField1.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
//            textField1.heightAnchor.constraint(equalToConstant: 50)
//            ])
//
//        NSLayoutConstraint.activate([
//            textField2.topAnchor.constraint(equalTo: textField1.bottomAnchor, constant: 10),
//            textField2.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
//            textField2.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
//            textField2.heightAnchor.constraint(equalToConstant: 50)
//            ])
        
        NSLayoutConstraint.activate([
            messageField.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
            messageField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            messageField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            messageField.heightAnchor.constraint(equalToConstant: 150)
            ])
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: messageField.bottomAnchor, constant: 30),
            stack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            stack.heightAnchor.constraint(equalToConstant: 110)
            ])
        
        NSLayoutConstraint.activate([
            footerStack.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            footerStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            footerStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            footerStack.heightAnchor.constraint(equalToConstant: 40)
            ])
    }

}
