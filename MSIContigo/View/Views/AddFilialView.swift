//
//  AddFilialView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class AddFilialView: UIView {
    
    var flag: Bool? {
        didSet {
            if flag == true {
//                StaticMethods.shared().getGradiendBG(addBtn, title: "Agregar", tag: 0)
//                addBtn.isEnabled = true
                //                StaticMethods.shared().clearGradient(addBtn, title: "Agregar", tag: 0)
                addBtn.setAttributedTitle(NSAttributedString(string: "Agregar", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.msiBlue]), for: UIControl.State.normal)
                addBtn.backgroundColor = UIColor.msiYellow
                addBtn.layer.masksToBounds = true
                addBtn.layer.cornerRadius = 6.0
                addBtn.tag = 0
                //                addBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
                //                addBtn.layer.shadowRadius = 7.0
                //                addBtn.layer.shadowColor = UIColor.customBlue2.cgColor
                //                addBtn.layer.shadowOpacity = 0.5
                addBtn.isEnabled = true
            } else {
//                StaticMethods.shared().clearGradient(addBtn, title: "Agregar", tag: 0)
                addBtn.setAttributedTitle(NSAttributedString(string: "Agregar", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.msiBlue.withAlphaComponent(0.5)]), for: UIControl.State.normal)
                addBtn.backgroundColor = UIColor.msiYellow.withAlphaComponent(0.5)
                addBtn.layer.masksToBounds = true
                addBtn.layer.cornerRadius = 6.0
//                addBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
//                addBtn.layer.shadowRadius = 7.0
//                addBtn.layer.shadowColor = UIColor.customBlue2.cgColor
//                addBtn.layer.shadowOpacity = 0.5
                addBtn.layer.borderColor = UIColor.customBlue2.withAlphaComponent(0.5).cgColor
                addBtn.layer.borderWidth = 2
                addBtn.isEnabled = false
            }
        }
    }
    
    let tableView: UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
        tableView.register(FilialSelectedCell.self, forCellReuseIdentifier: "selectFilialCell")
        tableView.backgroundColor = UIColor.clear
        tableView.allowsMultipleSelection = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    let addBtn: UIButton = {
        let addBtn = UIButton(type: UIButton.ButtonType.custom)
        addBtn.setAttributedTitle(NSAttributedString(string: "Agregar", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
        addBtn.layer.masksToBounds = true
        addBtn.clipsToBounds = true
        addBtn.tag = 0
        addBtn.isEnabled = false
        addBtn.translatesAutoresizingMaskIntoConstraints = false
        return addBtn
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(tableView)
        self.addSubview(addBtn)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tableView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.6)
            ])
        
        NSLayoutConstraint.activate([
            addBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            addBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            addBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            addBtn.heightAnchor.constraint(equalToConstant: 50)
            ])
    }
    
}
