//
//  MenuView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class MenuView: UIView {
    
    var modelView: EmployeeViewModel? {
        didSet {
            if let viewModel = modelView {
                number = viewModel.getNumber
                userName.text = viewModel.getName
                userMail.text = viewModel.getMail
            }
        }
    }
    
    var number: String = {
        let number = ""
        return number
    }()
    
    let avarImage: UIImageView = {
        let avatarImage = UIImageView(frame: CGRect.zero)
        avatarImage.image = UIImage(named: "avatarPlaceholder")
        avatarImage.layer.cornerRadius = 40
        avatarImage.layer.masksToBounds = true
        avatarImage.contentMode = UIView.ContentMode.scaleToFill
        avatarImage.isUserInteractionEnabled = true
        avatarImage.translatesAutoresizingMaskIntoConstraints = false
        return avatarImage
    }()
    
    let userName: UILabel = {
        let userName = UILabel(frame: CGRect.zero)
        userName.text = "Miguel G. Prado"
        userName.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.heavy)
        userName.textColor = UIColor.white
        userName.textAlignment = NSTextAlignment.center
        userName.numberOfLines = 0
        userName.translatesAutoresizingMaskIntoConstraints = false
        return userName
    }()
    
    let userMail: UILabel = {
        let userMail = UILabel(frame: CGRect.zero)
        userMail.text = "miguel@msidigital.com.mx"
        userMail.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        userMail.textColor = UIColor.white.withAlphaComponent(0.7)
        userMail.textAlignment = NSTextAlignment.center
        userMail.numberOfLines = 0
        userMail.translatesAutoresizingMaskIntoConstraints = false
        return userMail
    }()
    
    let appVersion: UILabel = {
        let versionNumber: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let appVersion = UILabel(frame: CGRect.zero)
        appVersion.text = "Versión " + versionNumber
        appVersion.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        appVersion.textColor = UIColor.customBlue8
        appVersion.textAlignment = NSTextAlignment.center
        appVersion.numberOfLines = 0
        appVersion.translatesAutoresizingMaskIntoConstraints = false
        return appVersion
    }()
    
    let contactBtn: UIButton = {
        let contactBtn = UIButton(type: UIButton.ButtonType.custom)
        contactBtn.setAttributedTitle(NSAttributedString(string: "Contacto", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.7)]), for: UIControl.State.normal)
        contactBtn.backgroundColor = UIColor.clear
        contactBtn.tag = 0
        contactBtn.translatesAutoresizingMaskIntoConstraints = false
        return contactBtn
    }()
    
    let faqBtn: UIButton = {
        let faqBtn = UIButton(type: UIButton.ButtonType.custom)
        faqBtn.setAttributedTitle(NSAttributedString(string: "Preguntas frecuentes", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.7)]), for: UIControl.State.normal)
        faqBtn.backgroundColor = UIColor.clear
        faqBtn.tag = 1
        faqBtn.translatesAutoresizingMaskIntoConstraints = false
        return faqBtn
    }()
    
    let closeSessionBtn: UIButton = {
        let closeSessionBtn = UIButton(type: UIButton.ButtonType.custom)
        closeSessionBtn.setAttributedTitle(NSAttributedString(string: "Cerrar sesión", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.white.withAlphaComponent(0.7)]), for: UIControl.State.normal)
        closeSessionBtn.backgroundColor = UIColor.clear
        closeSessionBtn.tag = 2
        closeSessionBtn.translatesAutoresizingMaskIntoConstraints = false
        return closeSessionBtn
    }()
    
    let btnStack: UIStackView = {
        let btnStackView = UIStackView(frame: CGRect.zero)
        btnStackView.axis = NSLayoutConstraint.Axis.vertical
        btnStackView.distribution = UIStackView.Distribution.fillEqually
        btnStackView.translatesAutoresizingMaskIntoConstraints = false
        return btnStackView
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(avarImage)
        self.addSubview(userName)
        self.addSubview(userMail)
        self.addSubview(appVersion)
        btnStack.addArrangedSubview(contactBtn)
        btnStack.addArrangedSubview(faqBtn)
        btnStack.addArrangedSubview(closeSessionBtn)
        self.addSubview(btnStack)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            avarImage.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20),
            avarImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            avarImage.widthAnchor.constraint(equalToConstant: 80),
            avarImage.heightAnchor.constraint(equalToConstant: 80)
            ])
        
        NSLayoutConstraint.activate([
            userName.topAnchor.constraint(equalTo: avarImage.bottomAnchor, constant: 10),
            userName.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            userName.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            userMail.topAnchor.constraint(equalTo: userName.bottomAnchor, constant: 10),
            userMail.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            userMail.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            appVersion.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            appVersion.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6),
            appVersion.centerXAnchor.constraint(equalTo: self.centerXAnchor)
            ])
        
        NSLayoutConstraint.activate([
            btnStack.bottomAnchor.constraint(equalTo: appVersion.topAnchor, constant: -50),
            btnStack.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            btnStack.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            btnStack.heightAnchor.constraint(equalToConstant: 150)
            ])
    }
}
