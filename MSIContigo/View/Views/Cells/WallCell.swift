//
//  WallCell.swift
//  MSIContigo
//
//  Created by David Valencia on 9/3/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class WallCell: UICollectionViewCell {
    
    var modelView: WallViewModel? {
        didSet {
            if let viewModel = modelView {
                let urlString = Environment.baseURL.absoluteString + viewModel.getImage
                if let url = URL(string: urlString) {
                    bannerImage.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
                }
                titleLabel.text = viewModel.getTitle
                previewLabel.text = viewModel.getSubtitle
                
                let mutable: NSMutableAttributedString = NSMutableAttributedString()
                let text1 = NSAttributedString(string: viewModel.getFilial, attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)])
                let text2 = NSAttributedString(string: "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)])
                
                mutable.append(text1)
                mutable.append(text2)
            }
        }
    }
    
    let bannerImage: UIImageView = {
        let bannerImage = UIImageView(frame: CGRect.zero)
        bannerImage.contentMode = UIView.ContentMode.scaleToFill
        bannerImage.backgroundColor = UIColor.customWhite2
        bannerImage.image = UIImage(named: "underTest")
        bannerImage.translatesAutoresizingMaskIntoConstraints = false
        return bannerImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.15)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let filialAreaLabel: UILabel = {
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        let text1 = NSAttributedString(string: "MSI CENTRO", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)])
        let text2 = NSAttributedString(string: " R. Humanos", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)])
        
        mutable.append(text1)
        mutable.append(text2)
        
        let filialAreaLabel = UILabel(frame: CGRect.zero)
        filialAreaLabel.attributedText = mutable
        filialAreaLabel.numberOfLines = 0
        filialAreaLabel.textAlignment = NSTextAlignment.center
        filialAreaLabel.backgroundColor = UIColor.clear
        filialAreaLabel.adjustsFontSizeToFitWidth = true
        filialAreaLabel.isHidden = true
        filialAreaLabel.translatesAutoresizingMaskIntoConstraints = false
        return filialAreaLabel
    }()
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel(frame: CGRect.zero)
        titleLabel.text = "MSI garantiza que puedes incrementar tus ingresos"
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.bold)
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.textColor = UIColor.white
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.isHidden = true
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    let previewLabel: UILabel = {
        let previewLabel = UILabel(frame: CGRect.zero)
        previewLabel.text = "Contamos con 36 prestaciones adiconales a las de la ley, descubre más…"
        previewLabel.numberOfLines = 2
        previewLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
        previewLabel.textAlignment = NSTextAlignment.center
        previewLabel.textColor = UIColor.white
        previewLabel.backgroundColor = UIColor.clear
        previewLabel.adjustsFontSizeToFitWidth = true
        previewLabel.isHidden = true
        previewLabel.translatesAutoresizingMaskIntoConstraints = false
        return previewLabel
    }()
    
    let showMoreBtn: CustomButton3 = {
        let showMoreBtn = CustomButton3(type: UIButton.ButtonType.custom)
        showMoreBtn.setAttributedTitle(NSAttributedString(string: "Ver más...", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
        //        enterBtn.backgroundColor = UIColor.customBlue2
        //        enterBtn.layer.masksToBounds = false
        //        enterBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        //        enterBtn.layer.shadowRadius = 7.0
        //        enterBtn.layer.shadowColor = UIColor.customBlue2.cgColor
        //        enterBtn.layer.shadowOpacity = 0.5
        //        enterBtn.layer.cornerRadius = 6.0
        showMoreBtn.tag = 0
        showMoreBtn.isEnabled = false
        showMoreBtn.isHidden = true
        showMoreBtn.translatesAutoresizingMaskIntoConstraints = false
        return showMoreBtn
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(bannerImage)
        bannerImage.addSubview(overlay)
        self.addSubview(showMoreBtn)
        self.addSubview(previewLabel)
        self.addSubview(titleLabel)
        self.addSubview(filialAreaLabel)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bannerImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            bannerImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            bannerImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            bannerImage.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: bannerImage.topAnchor, constant: 0),
            overlay.leadingAnchor.constraint(equalTo: bannerImage.leadingAnchor, constant: 0),
            overlay.trailingAnchor.constraint(equalTo: bannerImage.trailingAnchor, constant: 0),
            overlay.bottomAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            showMoreBtn.bottomAnchor.constraint(equalTo: overlay.bottomAnchor, constant: -50),
            showMoreBtn.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -23),
            showMoreBtn.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 23),
            showMoreBtn.heightAnchor.constraint(equalToConstant: 50)
            ])
        
        NSLayoutConstraint.activate([
            previewLabel.bottomAnchor.constraint(equalTo: showMoreBtn.topAnchor, constant: -30),
            previewLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 23),
            previewLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: previewLabel.topAnchor, constant: -20),
            titleLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -40),
            titleLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 40)
            ])
        
        NSLayoutConstraint.activate([
            filialAreaLabel.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -20),
            filialAreaLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -23),
            filialAreaLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 23)
            ])
    }
}
