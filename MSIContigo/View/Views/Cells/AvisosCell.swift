//
//  AvisosCell.swift
//  MSIContigo
//
//  Created by David Valencia on 8/31/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class AvisosCell: UITableViewCell {
    
    var modelView: AvisosViewModel? {
        didSet {
            if let viewModel = modelView {
                let mutable: NSMutableAttributedString = NSMutableAttributedString()
                let text1 = NSAttributedString(string: viewModel.getTitutlo + "\n", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customGreen1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)])
                let text2 = NSAttributedString(string: viewModel.getMensaje, attributes: [NSAttributedString.Key.foregroundColor : UIColor.customGray3, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)])
                
                mutable.append(text1)
                mutable.append(text2)
                messageLabel.attributedText = mutable
                dateLabel.text = viewModel.getFecha
            }
        }
    }
    
    let profileImage: UIImageView = {
        let profileImage = UIImageView(frame: CGRect.zero)
        profileImage.contentMode = UIView.ContentMode.scaleToFill
        profileImage.backgroundColor = UIColor.clear
        profileImage.layer.cornerRadius = 15
        profileImage.image = UIImage(named: "profilePlaceholder")
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        return profileImage
    }()
    
    let bubleContainer: UIView = {
        let bubleContainer = UIView(frame: CGRect.zero)
        bubleContainer.backgroundColor = UIColor.customWhite1
        bubleContainer.layer.cornerRadius = 8.0
        bubleContainer.translatesAutoresizingMaskIntoConstraints = false
        return bubleContainer
    }()
    
    let messageLabel: UILabel = {
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        let text1 = NSAttributedString(string: "MSI Mesa Directiva", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customGreen1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)])
        let text2 = NSAttributedString(string: "Gorgeous!", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customGray3, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)])
        
        mutable.append(text1)
        mutable.append(text2)
        
        let messageLabel = UILabel(frame: CGRect.zero)
        messageLabel.attributedText = mutable
        messageLabel.numberOfLines = 0
        messageLabel.backgroundColor = UIColor.clear
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        return messageLabel
    }()
    
    let dateLabel: UILabel = {
        let dateLabel = UILabel(frame: CGRect.zero)
        dateLabel.text = "Lunes 2 de febrero at 09:31"
        dateLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        dateLabel.textColor = UIColor.customGray3
        dateLabel.numberOfLines = 0
        dateLabel.backgroundColor = UIColor.clear
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        return dateLabel
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "avisosCell")
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(profileImage)
        self.addSubview(bubleContainer)
        self.addSubview(messageLabel)
        self.addSubview(dateLabel)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            profileImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            profileImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            profileImage.widthAnchor.constraint(equalToConstant: 30),
            profileImage.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            messageLabel.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 20),
            messageLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -43),
            ])
        
        NSLayoutConstraint.activate([
            bubleContainer.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -10),
            bubleContainer.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -10),
            bubleContainer.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10),
            bubleContainer.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 10)
            ])
        
        NSLayoutConstraint.activate([
            dateLabel.topAnchor.constraint(equalTo: bubleContainer.bottomAnchor, constant: 5),
            dateLabel.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 10),
            dateLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            dateLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5)
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
