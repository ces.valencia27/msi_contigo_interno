//
//  HonorBoxFooterSection.swift
//  MSIContigo
//
//  Created by David Valencia on 9/4/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class HonorBoxFooterSection: UICollectionReusableView {
    
    let showMoreBtn: CustomButton3 = {
        let showMoreBtn = CustomButton3(type: UIButton.ButtonType.custom)
        showMoreBtn.setAttributedTitle(NSAttributedString(string: "Ver mas...", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
        showMoreBtn.tag = 0
        showMoreBtn.translatesAutoresizingMaskIntoConstraints = false
        return showMoreBtn
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(showMoreBtn)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            showMoreBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            showMoreBtn.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            showMoreBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            showMoreBtn.heightAnchor.constraint(equalToConstant: 50)
            ])
    }
}
