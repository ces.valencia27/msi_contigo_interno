//
//  MsiCell.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class MsiCell: UICollectionViewCell {
    
    // Observadores para las propiedades
    var viewModel: HomeCellViewModel? {
        didSet {
            icon.image = viewModel?.getIcon
            optionName.text = viewModel?.getName
        }
    }
    
    let icon: UIImageView = {
        let icon = UIImageView(frame: CGRect.zero)
        icon.image = UIImage(named: "avisosIcon")
        icon.contentMode = UIView.ContentMode.scaleAspectFit
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    let optionName: UILabel = {
        let optionName = UILabel(frame: CGRect.zero)
        optionName.text = "Avisos"
        optionName.font = UIFont.systemFont(ofSize: 13.5, weight: UIFont.Weight.bold)
        optionName.textAlignment = NSTextAlignment.center
        optionName.textColor = UIColor.customGreen1
        optionName.numberOfLines = 0
        optionName.translatesAutoresizingMaskIntoConstraints = false
        return optionName
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(icon)
        self.addSubview(optionName)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            icon.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            icon.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -10),
            icon.widthAnchor.constraint(equalToConstant: 25),
            icon.heightAnchor.constraint(equalToConstant: 25)
            ])
        
        NSLayoutConstraint.activate([
            optionName.topAnchor.constraint(equalTo: icon.bottomAnchor, constant: 10),
            optionName.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
            optionName.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5)
            ])
    }
}
