//
//  NewsCell.swift
//  MSIContigo
//
//  Created by David Valencia on 8/31/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class NewsCell: UICollectionViewCell {
    
    var modelView: NewsViewModel? {
        didSet {
            if let viewModel = modelView {
                let urlString = Environment.baseURL.absoluteString + viewModel.getImg
                if let url = URL(string: urlString) {
                    newsImge.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
                }
                postDate.text = viewModel.getDate
                previewDescription.text = viewModel.getTitle
            }
        }
    }
    
    let newsImge: UIImageView = {
        let newsImage = UIImageView(frame: CGRect.zero)
        newsImage.image = UIImage(named: "imgPlaceholder")
        newsImage.contentMode = UIView.ContentMode.scaleToFill
        newsImage.layer.cornerRadius = 5.0
        newsImage.translatesAutoresizingMaskIntoConstraints = false
        return newsImage
    }()
    
    let postDate: UILabel = {
        let postDate = UILabel(frame: CGRect.zero)
        postDate.text = "14 May 2018"
        postDate.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        postDate.textColor = UIColor.customGray3
        postDate.backgroundColor = UIColor.clear
        postDate.adjustsFontSizeToFitWidth = true
        postDate.translatesAutoresizingMaskIntoConstraints = false
        return postDate
    }()
    
    let previewDescription: UILabel = {
        let previewDescription = UILabel(frame: CGRect.zero)
        previewDescription.text = "Alexandria Ocasio-Cortez Earns Fox News Own Goal"
        previewDescription.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        previewDescription.numberOfLines = 2
        previewDescription.textColor = UIColor.customGreen1
        previewDescription.backgroundColor = UIColor.clear
        previewDescription.adjustsFontSizeToFitWidth = true
        previewDescription.translatesAutoresizingMaskIntoConstraints = false
        return previewDescription
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(newsImge)
        self.addSubview(postDate)
        self.addSubview(previewDescription)
    }
    
    // MARK: - Método para definir al autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            newsImge.topAnchor.constraint(equalTo: self.topAnchor, constant: 15),
            newsImge.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            newsImge.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -15),
            newsImge.widthAnchor.constraint(equalToConstant: 100)
            ])
        
        NSLayoutConstraint.activate([
            postDate.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            postDate.leadingAnchor.constraint(equalTo: newsImge.trailingAnchor, constant: 10),
            postDate.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            postDate.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.2)
            ])
        
        NSLayoutConstraint.activate([
            previewDescription.topAnchor.constraint(equalTo: postDate.bottomAnchor, constant: 5),
            previewDescription.leadingAnchor.constraint(equalTo: newsImge.trailingAnchor, constant: 10),
            previewDescription.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            previewDescription.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20)
            ])
    }
}
