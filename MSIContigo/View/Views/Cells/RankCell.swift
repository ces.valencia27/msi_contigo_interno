//
//  RankCell.swift
//  MSIContigo
//
//  Created by David Valencia on 8/30/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class RankCell: UITableViewCell {
    
    let filialNumber: UILabel = {
        let filialNumber = UILabel(frame: CGRect.zero)
        filialNumber.text = ""
        filialNumber.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        filialNumber.textColor = UIColor.customGray4
        filialNumber.backgroundColor = UIColor.clear
        filialNumber.translatesAutoresizingMaskIntoConstraints = false
        return filialNumber
    }()
    
    let filialName: UILabel = {
        let filialName = UILabel(frame: CGRect.zero)
        filialName.text = ""
        filialName.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        filialName.textColor = UIColor.customBlack3
        filialName.backgroundColor = UIColor.clear
        filialName.translatesAutoresizingMaskIntoConstraints = false
        return filialName
    }()
    
    let qualification: UILabel = {
        let qualification = UILabel(frame: CGRect.zero)
        qualification.text = ""
        qualification.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold)
        qualification.textColor = UIColor.customBlack3
        qualification.backgroundColor = UIColor.clear
        qualification.translatesAutoresizingMaskIntoConstraints = false
        return qualification
    }()
    
    let indicatorIcon: UIImageView = {
        let indicatorIcon = UIImageView(frame: CGRect.zero)
        indicatorIcon.image = UIImage(named: "greenArrow")
        indicatorIcon.contentMode = UIView.ContentMode.scaleAspectFit
        indicatorIcon.translatesAutoresizingMaskIntoConstraints = false
        return indicatorIcon
    }()
    
    let leftStack: UIStackView = {
        let leftStack = UIStackView(frame: CGRect.zero)
        leftStack.axis = NSLayoutConstraint.Axis.horizontal
        leftStack.distribution = UIStackView.Distribution.fillProportionally
        leftStack.spacing = 0
        leftStack.translatesAutoresizingMaskIntoConstraints = false
        return leftStack
    }()
    
    let rightStack: UIStackView = {
        let rightStack = UIStackView(frame: CGRect.zero)
        rightStack.axis = NSLayoutConstraint.Axis.horizontal
        rightStack.distribution = UIStackView.Distribution.fillProportionally
        rightStack.spacing = 0
        rightStack.translatesAutoresizingMaskIntoConstraints = false
        return rightStack
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "rankCell")
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la celda
    private func setSubviews() {
        
        leftStack.addArrangedSubview(filialNumber)
        leftStack.addArrangedSubview(filialName)
        rightStack.addArrangedSubview(qualification)
        rightStack.addArrangedSubview(indicatorIcon)
        self.addSubview(leftStack)
        self.addSubview(rightStack)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            leftStack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            leftStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            leftStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            leftStack.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.65)
            ])
        
        NSLayoutConstraint.activate([
            rightStack.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            rightStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            rightStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            rightStack.leadingAnchor.constraint(equalTo: leftStack.trailingAnchor, constant: 0)
            ])
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
