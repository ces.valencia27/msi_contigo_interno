//
//  ModulesCell.swift
//  MSIContigo
//
//  Created by David Valencia on 9/2/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class ModulesCell: UICollectionViewCell {
    
    var model: ModulesModel? {
        didSet {
            if let unwrappedModel = model {
                bgImage.image = UIImage(named: unwrappedModel.iconName)
                optionName.text = unwrappedModel.moduleName
            }
        }
    }
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.layer.masksToBounds = true
        bgImage.layer.cornerRadius = 6.0
        bgImage.image = UIImage(named: "underTest")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.backgroundColor = UIColor.clear
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.layer.cornerRadius = 15
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let dotsContainer: UIView = {
        let dotsContainer = UIView(frame: CGRect.zero)
        dotsContainer.layer.cornerRadius = 15
        dotsContainer.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        dotsContainer.layer.shadowRadius = 3.0
        dotsContainer.layer.shadowColor = UIColor.black.cgColor
        dotsContainer.layer.shadowOpacity = 0.5
        dotsContainer.contentMode = UIView.ContentMode.scaleAspectFit
        dotsContainer.backgroundColor = UIColor.white
        dotsContainer.translatesAutoresizingMaskIntoConstraints = false
        return dotsContainer
    }()
    
    let dotsIcon: UIImageView = {
        let dotsIcon = UIImageView(frame: CGRect.zero)
        dotsIcon.image = UIImage(named: "dotsIcon")
        dotsIcon.contentMode = UIView.ContentMode.scaleAspectFit
        dotsIcon.backgroundColor = UIColor.clear
        dotsIcon.translatesAutoresizingMaskIntoConstraints = false
        return dotsIcon
    }()
    
    let optionName: UILabel = {
        let optionName = UILabel(frame: CGRect.zero)
        optionName.text = "Noticias"
        optionName.font = UIFont.systemFont(ofSize: 21, weight: UIFont.Weight.bold)
        optionName.textColor = UIColor.white
        optionName.numberOfLines = 0
        optionName.backgroundColor = UIColor.clear
        optionName.translatesAutoresizingMaskIntoConstraints = false
        return optionName
    }()
    
    let articlesNumber: UILabel = {
        let articlesNumber = UILabel(frame: CGRect.zero)
        articlesNumber.text = "Más información"
        articlesNumber.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        articlesNumber.textColor = UIColor.white.withAlphaComponent(0.8)
        articlesNumber.numberOfLines = 0
        articlesNumber.backgroundColor = UIColor.clear
        articlesNumber.translatesAutoresizingMaskIntoConstraints = false
        return articlesNumber
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(bgImage)
        self.addSubview(overlay)
        self.addSubview(dotsContainer)
        dotsContainer.addSubview(dotsIcon)
        stackView.addArrangedSubview(optionName)
        stackView.addArrangedSubview(articlesNumber)
        self.addSubview(stackView)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: self.topAnchor),
            overlay.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            overlay.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])

        NSLayoutConstraint.activate([
            dotsContainer.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            dotsContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            dotsContainer.widthAnchor.constraint(equalToConstant: 30),
            dotsContainer.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([
            dotsIcon.centerXAnchor.constraint(equalTo: dotsContainer.centerXAnchor, constant: 0),
            dotsIcon.centerYAnchor.constraint(equalTo: dotsContainer.centerYAnchor, constant: 0),
            dotsIcon.widthAnchor.constraint(equalToConstant: 15),
            dotsIcon.heightAnchor.constraint(equalToConstant: 15)
            ])
        
        NSLayoutConstraint.activate([
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10)
            ])
    }
}
