//
//  FilialSelectedCell.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class FilialSelectedCell: UITableViewCell {
    
    var modelView: FilialViewModel? {
        didSet {
            if let viewModel = modelView {
                filialName.text = viewModel.getName
            }
        }
    }
    
    let filialName: UILabel = {
        let filialName = UILabel(frame: CGRect.zero)
        filialName.text = ""
        filialName.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        filialName.textColor = UIColor.white
        filialName.backgroundColor = UIColor.clear
        filialName.translatesAutoresizingMaskIntoConstraints = false
        return filialName
    }()
    
    let customDisclosure: UIImageView = {
        let customDisclosure = UIImageView(frame: CGRect.zero)
        customDisclosure.image = UIImage(named: "uncheckedIcon")
        customDisclosure.contentMode = UIView.ContentMode.scaleAspectFit
        customDisclosure.translatesAutoresizingMaskIntoConstraints = false
        return customDisclosure
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "selectFilialCell")
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes de la celda
    private func setSubviews() {
        
        self.addSubview(filialName)
        self.addSubview(customDisclosure)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            filialName.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            filialName.topAnchor.constraint(equalTo: self.topAnchor),
            filialName.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            filialName.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.75)
            ])
        
        NSLayoutConstraint.activate([
            customDisclosure.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            customDisclosure.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            customDisclosure.widthAnchor.constraint(equalToConstant: 20),
            customDisclosure.heightAnchor.constraint(equalToConstant: 20)
            ])
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected != true {
            self.customDisclosure.image = UIImage(named: "uncheckedIcon")
        } else {
            self.customDisclosure.image = UIImage(named: "checkedIcon")
        }
    }
    
}
