//
//  FaqCell.swift
//  MSIContigo
//
//  Created by David Valencia on 9/2/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class FaqCell: UITableViewCell {
    
    let questionLabel: CustomLabel = {
        let questionLabel = CustomLabel(frame: CGRect.zero)
        questionLabel.text = ""
        questionLabel.layer.masksToBounds = true
        questionLabel.layer.cornerRadius = 6.0
        questionLabel.layer.maskedCorners = [CACornerMask.layerMinXMaxYCorner, CACornerMask.layerMaxXMaxYCorner]
        questionLabel.numberOfLines = 0
        if #available(iOS 13.0, *) {
            questionLabel.backgroundColor = UIColor.systemBackground
        } else {
            questionLabel.backgroundColor = UIColor.white
        }
        if #available(iOS 13.0, *) {
            questionLabel.textColor = UIColor.label
        } else {
            questionLabel.textColor = UIColor.black
        }
        questionLabel.layer.borderColor = UIColor.lightGray.cgColor
        questionLabel.layer.borderWidth = 1
        questionLabel.translatesAutoresizingMaskIntoConstraints = false
        return questionLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "faqCell")
        
        self.addSubview(questionLabel)
        
        NSLayoutConstraint.activate([
            questionLabel.topAnchor.constraint(equalTo: self.topAnchor),
            questionLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            questionLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.93),
            questionLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
