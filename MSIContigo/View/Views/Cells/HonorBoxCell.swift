//
//  HonorBoxCell.swift
//  MSIContigo
//
//  Created by David Valencia on 9/4/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class HonorBoxCell: UICollectionViewCell {
    
    var modelView: HonorBoxViewModel? {
        didSet {
            if let viewModel = modelView {
                let urlString = Environment.baseURL.absoluteString + viewModel.getImage
                if let url = URL(string: urlString) {
                    profileImage.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
                }
                
                nameLabel.text = viewModel.getName
                dateLabel.text = viewModel.getDate
            }
        }
    }
    
    let profileImage: UIImageView = {
        let profileImage = UIImageView(frame: .zero)
        profileImage.image = UIImage(named: "profilePlaceholder")
//        profileImage.layer.cornerRadius = 40
        profileImage.layer.masksToBounds = true
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        return profileImage
    }()
    
    let nameLabel: UILabel = {
        let nameLabel = UILabel(frame: CGRect.zero)
        nameLabel.text = "Miguel Gómez J."
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.bold)
        nameLabel.textAlignment = NSTextAlignment.center
        nameLabel.textColor = UIColor.customGreen1
        nameLabel.backgroundColor = UIColor.clear
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()
    
    let dateLabel: UILabel = {
        let dateLabel = UILabel(frame: CGRect.zero)
        dateLabel.text = "Septiembre 2019"
        dateLabel.numberOfLines = 0
        dateLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        dateLabel.textAlignment = NSTextAlignment.center
        dateLabel.textColor = UIColor.customGray3
        dateLabel.backgroundColor = UIColor.clear
        dateLabel.adjustsFontSizeToFitWidth = true
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        return dateLabel
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(profileImage)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(dateLabel)
        self.addSubview(stackView)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            profileImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            profileImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            profileImage.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.55),
            profileImage.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.55)
            ])
        
        profileImage.layer.cornerRadius = self.frame.width * 0.275
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 10),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
            ])
    }
}
