//
//  NewsView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class NewsView: UIView {
    
    let newsCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        
        let newsCollection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        newsCollection.register(NewsCell.self, forCellWithReuseIdentifier: "newsCell")
        newsCollection.register(NewsFooterSection.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "footer")
        newsCollection.backgroundColor = UIColor.clear
        newsCollection.translatesAutoresizingMaskIntoConstraints = false
        return newsCollection
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(newsCollection)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            newsCollection.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
            newsCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            newsCollection.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            newsCollection.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
            ])
    }

}
