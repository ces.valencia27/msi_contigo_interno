//
//  FilialModulesView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class FilialModulesView: UIView {
    
    let modulesCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 20
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        
        let modulesCollection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        modulesCollection.register(ModulesCell.self, forCellWithReuseIdentifier: "modulesCell")
        modulesCollection.backgroundColor = UIColor.clear
        modulesCollection.isScrollEnabled = false
        modulesCollection.translatesAutoresizingMaskIntoConstraints = false
        
        return modulesCollection    
    }()
    
    let addFilialBtn: CustomButton3 = {
        let addFilialBtn = CustomButton3(type: UIButton.ButtonType.custom)
        addFilialBtn.setAttributedTitle(NSAttributedString(string: "Nombre de Filial", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
//        addFilialBtn.backgroundColor = UIColor.white
//        addFilialBtn.layer.masksToBounds = false
//        addFilialBtn.layer.cornerRadius = 6.0
//        addFilialBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
//        addFilialBtn.layer.shadowRadius = 7.0
//        addFilialBtn.layer.shadowColor = UIColor.customBlue2.cgColor
//        addFilialBtn.layer.shadowOpacity = 0.5
//        addFilialBtn.layer.borderColor = UIColor.customBlue2.cgColor
//        addFilialBtn.layer.borderWidth = 2
        addFilialBtn.tag = 0
        addFilialBtn.translatesAutoresizingMaskIntoConstraints = false
        return addFilialBtn
    }()
    
    let changeFilialBtn: UIButton = {
        let changeFilialBtn = UIButton(type: UIButton.ButtonType.custom)
        changeFilialBtn.setAttributedTitle(NSAttributedString(string: "Cambiar filial favorita", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium), NSAttributedString.Key.foregroundColor : UIColor.customGray2.withAlphaComponent(0.5)]), for: UIControl.State.normal)
        changeFilialBtn.tag = 1
        changeFilialBtn.isEnabled = false
        changeFilialBtn.translatesAutoresizingMaskIntoConstraints = false
        return changeFilialBtn
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillEqually
        stackView.spacing = 0
        stackView.isHidden = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        stackView.addArrangedSubview(addFilialBtn)
        stackView.addArrangedSubview(changeFilialBtn)
        self.addSubview(stackView)
        self.addSubview(modulesCollection)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            stackView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -10),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            stackView.heightAnchor.constraint(equalToConstant: 110)
            ])
        
        NSLayoutConstraint.activate([
            modulesCollection.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20),
            modulesCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            modulesCollection.bottomAnchor.constraint(equalTo: stackView.topAnchor, constant: -30),
            modulesCollection.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20)
            ])
    }
    
}
