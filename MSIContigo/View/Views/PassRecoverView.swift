//
//  PassRecoverView.swift
//  MSIContigo
//
//  Created by David Valencia on 28/09/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class PassRecoverView: UIView {

        let welcomeText: UILabel = {
            let welcomeText = UILabel(frame: CGRect.zero)
            welcomeText.text = "Recuperar contraseña"
            welcomeText.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.black)
            welcomeText.textColor = UIColor.msiYellow
            welcomeText.textAlignment = NSTextAlignment.left
            welcomeText.numberOfLines = 0
            welcomeText.translatesAutoresizingMaskIntoConstraints = false
            return welcomeText
        }()

        let employeeNumberField: CustomTextField = {
            let employeeNumberField = CustomTextField(frame: CGRect.zero)
            employeeNumberField.layer.masksToBounds = true
            employeeNumberField.layer.cornerRadius = 5.0
            employeeNumberField.attributedPlaceholder = NSAttributedString(string: "RFC", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
            employeeNumberField.textColor = UIColor.customGray1
            employeeNumberField.backgroundColor = UIColor.customBlue4
            employeeNumberField.keyboardType = UIKeyboardType.numberPad
            employeeNumberField.keyboardAppearance = UIKeyboardAppearance.dark
            employeeNumberField.layer.borderWidth = 1
            employeeNumberField.layer.borderColor = UIColor.clear.cgColor
            employeeNumberField.clearButtonMode = UITextField.ViewMode.whileEditing
            employeeNumberField.tag = 0
            employeeNumberField.translatesAutoresizingMaskIntoConstraints = false
            return employeeNumberField
        }()
        
        let mailField: CustomTextField = {
            let mailField = CustomTextField(frame: CGRect.zero)
            mailField.layer.masksToBounds = true
            mailField.layer.cornerRadius = 5.0
            mailField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
            mailField.textColor = UIColor.customGray1
            mailField.backgroundColor = UIColor.customBlue4
            mailField.keyboardType = UIKeyboardType.emailAddress
            mailField.keyboardAppearance = UIKeyboardAppearance.dark
            mailField.layer.borderWidth = 1
            mailField.layer.borderColor = UIColor.clear.cgColor
            mailField.clearButtonMode = UITextField.ViewMode.whileEditing
            mailField.tag = 1
            mailField.translatesAutoresizingMaskIntoConstraints = false
            return mailField
        }()
        
        let stackView: UIStackView = {
            let stackView = UIStackView(frame: CGRect.zero)
            stackView.axis = NSLayoutConstraint.Axis.vertical
            stackView.distribution = UIStackView.Distribution.fillEqually
            stackView.spacing = 10
            stackView.translatesAutoresizingMaskIntoConstraints = false
            return stackView
        }()
        
        let requestBtn: PrimaryButton = {
            let requestBtn = PrimaryButton(type: UIButton.ButtonType.custom)
            requestBtn.setAttributedTitle(NSAttributedString(string: "Solicitar", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.msiBlue]), for: UIControl.State.normal)
            requestBtn.tag = 0
            requestBtn.translatesAutoresizingMaskIntoConstraints = false
            return requestBtn
        }()
        
        let toLoginBtn: UIButton = {
            let toLoginBtn = UIButton(type: UIButton.ButtonType.custom)
            toLoginBtn.setAttributedTitle(NSAttributedString(string: "Ya tengo cuenta", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium), NSAttributedString.Key.foregroundColor : UIColor.customGray2]), for: UIControl.State.normal)
            toLoginBtn.tag = 1
            toLoginBtn.translatesAutoresizingMaskIntoConstraints = false
            return toLoginBtn
        }()

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setSubviews()
            setAutolayout()
        }
        
        // Método para inicializar los componentes
        private func setSubviews() {
            
            self.addSubview(welcomeText)
            stackView.addArrangedSubview(employeeNumberField)
            stackView.addArrangedSubview(mailField)
            stackView.addArrangedSubview(requestBtn)
            stackView.addArrangedSubview(toLoginBtn)
            self.addSubview(stackView)
        }
        
        // Método para definir el autolayout de los componentes de la vista
        private func setAutolayout() {
            
            NSLayoutConstraint.activate([
                welcomeText.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
                welcomeText.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
                welcomeText.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23)
                ])
            
            NSLayoutConstraint.activate([
                stackView.topAnchor.constraint(equalTo: welcomeText.bottomAnchor, constant: 50),
                stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
                stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
                stackView.heightAnchor.constraint(equalToConstant: 290)
                ])

        }

}
