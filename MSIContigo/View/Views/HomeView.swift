//
//  HomeView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class HomeView: UIView {
    
    let generalTitle: UILabel = {
        let generalTitle = UILabel(frame: CGRect.zero)
        generalTitle.text = "Generales"
        generalTitle.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.bold)
        generalTitle.textColor = UIColor.customBlue5
        generalTitle.textAlignment = NSTextAlignment.left
        generalTitle.numberOfLines = 0
        generalTitle.translatesAutoresizingMaskIntoConstraints = false
        return generalTitle
    }()
    
    let generalCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        
        let academyCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        academyCollectionView.register(GeneralCell.self, forCellWithReuseIdentifier: "general")
        academyCollectionView.backgroundColor = UIColor.clear
        academyCollectionView.translatesAutoresizingMaskIntoConstraints = false
        return academyCollectionView
    }()
    
    let msiTitle: UILabel = {
        let msiTitle = UILabel(frame: CGRect.zero)
        msiTitle.text = "MSI Contigo"
        msiTitle.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.bold)
        msiTitle.textColor = UIColor.customBlue5
        msiTitle.textAlignment = NSTextAlignment.left
        msiTitle.numberOfLines = 0
        msiTitle.translatesAutoresizingMaskIntoConstraints = false
        return msiTitle
    }()
    
    let msiCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        
        let msiCollection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        msiCollection.register(MsiCell.self, forCellWithReuseIdentifier: "msi")
        msiCollection.backgroundColor = UIColor.clear
        msiCollection.translatesAutoresizingMaskIntoConstraints = false
        return msiCollection
    }()
    
    let addFilialBtn: UIButton = {
        let addFilialBtn = UIButton(type: UIButton.ButtonType.custom)
        addFilialBtn.setAttributedTitle(NSAttributedString(string: "Agregar fililal favorita", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.customBlue6]), for: UIControl.State.normal)
        addFilialBtn.backgroundColor = UIColor.white
        addFilialBtn.layer.masksToBounds = false
        addFilialBtn.layer.cornerRadius = 6.0
        addFilialBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        addFilialBtn.layer.shadowRadius = 7.0
        addFilialBtn.layer.shadowColor = UIColor.customBlue6.cgColor
        addFilialBtn.layer.shadowOpacity = 0.5
        addFilialBtn.layer.borderColor = UIColor.customBlue6.cgColor
        addFilialBtn.layer.borderWidth = 2
        addFilialBtn.tag = 0
        addFilialBtn.translatesAutoresizingMaskIntoConstraints = false
        return addFilialBtn
    }()
    
    let changeFilialBtn: UIButton = {
        let changeFilialBtn = UIButton(type: UIButton.ButtonType.custom)
        changeFilialBtn.setAttributedTitle(NSAttributedString(string: "Cambiar filial favorita", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium), NSAttributedString.Key.foregroundColor : UIColor.customGray2.withAlphaComponent(0.5)]), for: UIControl.State.normal)
        changeFilialBtn.tag = 1
        changeFilialBtn.isEnabled = false
        changeFilialBtn.translatesAutoresizingMaskIntoConstraints = false
        return changeFilialBtn
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillEqually
        stackView.spacing = 0
        stackView.isHidden = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(generalTitle)
        self.addSubview(generalCollection)
        self.addSubview(msiTitle)
        self.addSubview(msiCollection)
        stackView.addArrangedSubview(addFilialBtn)
        stackView.addArrangedSubview(changeFilialBtn)
        self.addSubview(stackView)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            generalTitle.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20),
            generalTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            generalTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
        
        NSLayoutConstraint.activate([
            generalCollection.topAnchor.constraint(equalTo: generalTitle.bottomAnchor, constant: -20),
            generalCollection.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            generalCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            generalCollection.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.28)
            ])
        
        NSLayoutConstraint.activate([
            msiTitle.topAnchor.constraint(equalTo: generalCollection.bottomAnchor, constant: 20),
            msiTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            msiTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
        
        NSLayoutConstraint.activate([
            msiCollection.topAnchor.constraint(equalTo: msiTitle.bottomAnchor, constant: -20),
            msiCollection.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            msiCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            msiCollection.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.28)
            ])
        
        NSLayoutConstraint.activate([
            stackView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -10),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            stackView.heightAnchor.constraint(equalToConstant: 110)
            ])
    }
    
}
