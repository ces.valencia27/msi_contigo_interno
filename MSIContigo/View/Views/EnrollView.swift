//
//  EnrollView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class EnrollView: UIView {
    
    let welcomeText: UILabel = {
        let welcomeText = UILabel(frame: CGRect.zero)
        welcomeText.text = "Crear cuenta"
        welcomeText.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.black)
        welcomeText.textColor = UIColor.msiYellow
        welcomeText.textAlignment = NSTextAlignment.left
        welcomeText.numberOfLines = 0
        welcomeText.translatesAutoresizingMaskIntoConstraints = false
        return welcomeText
    }()
    
    let welcomeSubtitle: UILabel = {
        let welcomeSubtitle = UILabel(frame: CGRect.zero)
        welcomeSubtitle.text = "Grupo Multisitemas SA de CV"
        welcomeSubtitle.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        welcomeSubtitle.textColor = UIColor.customGray1
        welcomeSubtitle.textAlignment = NSTextAlignment.left
        welcomeSubtitle.numberOfLines = 0
        welcomeSubtitle.translatesAutoresizingMaskIntoConstraints = false
        return welcomeSubtitle
    }()
    
    let employeeNumberField: CustomTextField = {
        let employeeNumberField = CustomTextField(frame: CGRect.zero)
        employeeNumberField.layer.masksToBounds = true
        employeeNumberField.layer.cornerRadius = 5.0
        employeeNumberField.attributedPlaceholder = NSAttributedString(string: "RFC", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        employeeNumberField.textColor = UIColor.customGray1
        employeeNumberField.backgroundColor = UIColor.customBlue4
        employeeNumberField.keyboardType = UIKeyboardType.default
        employeeNumberField.keyboardAppearance = UIKeyboardAppearance.dark
        employeeNumberField.tag = 0
        employeeNumberField.translatesAutoresizingMaskIntoConstraints = false
        return employeeNumberField
    }()
    
    let mailField: CustomTextField = {
        let mailField = CustomTextField(frame: CGRect.zero)
        mailField.layer.masksToBounds = true
        mailField.layer.cornerRadius = 5.0
        mailField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        mailField.textColor = UIColor.customGray1
        mailField.backgroundColor = UIColor.customBlue4
        mailField.keyboardType = UIKeyboardType.emailAddress
        mailField.keyboardAppearance = UIKeyboardAppearance.dark
        mailField.tag = 1
        mailField.translatesAutoresizingMaskIntoConstraints = false
        return mailField
    }()
    
    let filialField: CustomTextField = {
        let filialField = CustomTextField(frame: CGRect.zero)
        filialField.layer.masksToBounds = true
        filialField.layer.cornerRadius = 5.0
        filialField.attributedPlaceholder = NSAttributedString(string: "Filial", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        filialField.textColor = UIColor.customGray1
        filialField.backgroundColor = UIColor.customBlue4
        filialField.keyboardType = UIKeyboardType.emailAddress
        filialField.keyboardAppearance = UIKeyboardAppearance.dark
        filialField.tag = 2
        filialField.translatesAutoresizingMaskIntoConstraints = false
        return filialField
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillEqually
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let filialPicker: UIPickerView = {
        let filialPicker = UIPickerView(frame: CGRect.zero)
        filialPicker.showsSelectionIndicator = true
        filialPicker.translatesAutoresizingMaskIntoConstraints = true
        return filialPicker
    }()
    
    let requestBtn: PrimaryButton = {
        let requestBtn = PrimaryButton(type: UIButton.ButtonType.custom)
        requestBtn.setAttributedTitle(NSAttributedString(string: "Enviar solicitud", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.msiBlue]), for: UIControl.State.normal)
        requestBtn.tag = 0
        requestBtn.translatesAutoresizingMaskIntoConstraints = false
        return requestBtn
    }()
    
    let footerText: UILabel = {
        let footerText = UILabel(frame: CGRect.zero)
        footerText.text = "Al ingresar, usted acepta"
        footerText.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        footerText.textColor = UIColor.customGray1
        footerText.textAlignment = NSTextAlignment.center
        footerText.numberOfLines = 0
        footerText.translatesAutoresizingMaskIntoConstraints = false
        return footerText
    }()
    
    let termsBtn: UIButton = {
        let termsBtn = UIButton(type: UIButton.ButtonType.custom)
        termsBtn.setAttributedTitle(NSAttributedString(string: "Términos y Privacidad", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customBlue1]), for: UIControl.State.normal)
        termsBtn.tag = 1
        termsBtn.translatesAutoresizingMaskIntoConstraints = false
        return termsBtn
    }()
    
    let footerStack: UIStackView = {
        let footerStack = UIStackView(frame: CGRect.zero)
        footerStack.axis = NSLayoutConstraint.Axis.vertical
        footerStack.distribution = UIStackView.Distribution.fillEqually
        footerStack.translatesAutoresizingMaskIntoConstraints = false
        return footerStack
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(welcomeText)
        self.addSubview(welcomeSubtitle)
        self.addSubview(stackView)
        stackView.addArrangedSubview(employeeNumberField)
        stackView.addArrangedSubview(mailField)
        stackView.addArrangedSubview(filialField)
        self.addSubview(requestBtn)
        footerStack.addArrangedSubview(footerText)
        footerStack.addArrangedSubview(termsBtn)
        self.addSubview(footerStack)
    }
    
    // MARK: - Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            welcomeText.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
            welcomeText.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            welcomeText.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            welcomeSubtitle.topAnchor.constraint(equalTo: welcomeText.bottomAnchor, constant: 5),
            welcomeSubtitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            welcomeSubtitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: welcomeSubtitle.bottomAnchor, constant: 50),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            stackView.heightAnchor.constraint(equalToConstant: 180)
            ])
        
        NSLayoutConstraint.activate([
            requestBtn.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 60),
            requestBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            requestBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            requestBtn.heightAnchor.constraint(equalToConstant: 50)
            ])
        
        NSLayoutConstraint.activate([
            footerStack.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -10),
            footerStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            footerStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            footerStack.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.07)
            ])
        
    }
}
