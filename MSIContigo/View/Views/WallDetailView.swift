//
//  WallDetailView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class WallDetailView: UIView {
    
    var modelView: WallDetailViewModel? {
        didSet {
            if let viewModel = modelView {
                let urlString = Environment.baseURL.absoluteString + viewModel.getImage
                if let url = URL(string: urlString) {
                    bannerImage.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
                }
                dateLabel.text = viewModel.getDate
                titleLabel.text = viewModel.getTitle
                sucursalLabel.text = viewModel.getFilial
                detailContent.text = viewModel.getContent
            }
        }
    }
    
    let scrollContainer: UIScrollView = {
        let scrollContainer = UIScrollView(frame: CGRect.zero)
        scrollContainer.backgroundColor = UIColor.clear
        scrollContainer.translatesAutoresizingMaskIntoConstraints = false
        return scrollContainer
    }()
    
    let bannerImage: UIImageView = {
        let bannerImage = UIImageView(frame: CGRect.zero)
        bannerImage.image = UIImage(named: "imgPlaceholder")
        bannerImage.contentMode = UIView.ContentMode.scaleToFill
        bannerImage.backgroundColor = UIColor.customWhite1
        bannerImage.translatesAutoresizingMaskIntoConstraints = false
        return bannerImage
    }()
    
    let zoomBtn: UIButton = {
        let zoomBtn = UIButton(type: UIButton.ButtonType.custom)
        zoomBtn.layer.cornerRadius = 20
        zoomBtn.setImage(UIImage(named: "zoomIcon"), for: UIControl.State.normal)
        zoomBtn.translatesAutoresizingMaskIntoConstraints = false
        return zoomBtn
    }()
    
    let dateLabel: UILabel = {
        let dateLabel = UILabel(frame: CGRect.zero)
        dateLabel.text = "05 Aug 2018"
        dateLabel.numberOfLines = 0
        dateLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        dateLabel.textColor = UIColor.customGray3
        dateLabel.backgroundColor = UIColor.clear
        dateLabel.adjustsFontSizeToFitWidth = true
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        return dateLabel
    }()
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel(frame: CGRect.zero)
        titleLabel.text = "MSI garantiza que puedes incrementar tus ingresos"
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 30, weight: UIFont.Weight.bold)
        titleLabel.textColor = UIColor.customGreen1
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    let icon: UIImageView = {
        let icon = UIImageView(frame: CGRect.zero)
        icon.layer.cornerRadius = 15
        icon.image = UIImage(named: "starIcon")
        icon.contentMode = UIView.ContentMode.scaleToFill
        icon.backgroundColor = UIColor.customWhite1
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    let sucursalLabel: UILabel = {
        let sucursalLabel = UILabel(frame: CGRect.zero)
        sucursalLabel.text = "MSI Centro"
        sucursalLabel.numberOfLines = 0
        sucursalLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        sucursalLabel.textColor = UIColor.customGreen1
        sucursalLabel.backgroundColor = UIColor.clear
        sucursalLabel.adjustsFontSizeToFitWidth = true
        sucursalLabel.translatesAutoresizingMaskIntoConstraints = false
        return sucursalLabel
    }()
    
    let line: UIView = {
        let line = UIView(frame: CGRect.zero)
        line.backgroundColor = UIColor.lightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    let detailContent: UILabel = {
        
        let text1: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text2: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text3: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text4: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text5: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text6: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text7: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text8: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text9: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        
        let textArray = [text1, text2, text3, text4, text5, text6, text7, text8, text9]
        
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        
        for item in textArray {
            let attributed = NSAttributedString(string: "\(item)\n\n")
            mutable.append(attributed)
        }
        
        let detailContent = UILabel(frame: CGRect.zero)
//        detailContent.attributedText = mutable
        //        newsContent.text = """
        //        \(text1)\n
        //        \(text2)\n
        //        \(text3)\n
        //        \(text4)\n
        //        \(text5)\n
        //        \(text6)\n
        //        \(text7)\n
        //        \(text8)\n
        //        \(text9)
        //        """
        
        detailContent.numberOfLines = 0
        detailContent.lineBreakMode = NSLineBreakMode.byWordWrapping
        detailContent.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        detailContent.textColor = UIColor.customGreen1
        detailContent.backgroundColor = UIColor.clear
        detailContent.translatesAutoresizingMaskIntoConstraints = false
        return detailContent
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(scrollContainer)
        scrollContainer.addSubview(bannerImage)
        scrollContainer.addSubview(zoomBtn)
        scrollContainer.addSubview(dateLabel)
        scrollContainer.addSubview(titleLabel)
        scrollContainer.addSubview(icon)
        scrollContainer.addSubview(sucursalLabel)
        scrollContainer.addSubview(line)
        scrollContainer.addSubview(detailContent)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            scrollContainer.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            scrollContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            scrollContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            bannerImage.topAnchor.constraint(equalTo: scrollContainer.topAnchor),
            bannerImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bannerImage.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            bannerImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.25)
            ])
        
        NSLayoutConstraint.activate([
            zoomBtn.topAnchor.constraint(equalTo: bannerImage.topAnchor, constant: 10),
            zoomBtn.trailingAnchor.constraint(equalTo: bannerImage.trailingAnchor, constant: -10),
            zoomBtn.widthAnchor.constraint(equalToConstant: 40),
            zoomBtn.heightAnchor.constraint(equalToConstant: 40)
            ])
        
        NSLayoutConstraint.activate([
            dateLabel.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 20),
            dateLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            dateLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
        
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            icon.widthAnchor.constraint(equalToConstant: 30),
            icon.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([
            sucursalLabel.centerYAnchor.constraint(equalTo: icon.centerYAnchor),
            sucursalLabel.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 2),
            sucursalLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
        
        NSLayoutConstraint.activate([
            line.topAnchor.constraint(equalTo: icon.bottomAnchor, constant: 20),
            line.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            line.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.45),
            line.heightAnchor.constraint(equalToConstant: 1)
            ])
        
        NSLayoutConstraint.activate([
            detailContent.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 20),
            detailContent.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            detailContent.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            detailContent.bottomAnchor.constraint(equalTo: scrollContainer.bottomAnchor, constant: 0)
            ])
    }

}
