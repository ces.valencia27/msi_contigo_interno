//
//  HonorBoxDetailView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class HonorBoxDetailView: UIView {
    
    var modelView: HonorBoxDetailViewModel? {
        didSet {
            if let viewModel = modelView {
                let urlString = Environment.baseURL.absoluteString + viewModel.getImage
                if let url = URL(string: urlString) {
                    bannerImage.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
                }
                
                let mutableYear: NSMutableAttributedString = NSMutableAttributedString()
                let year = NSAttributedString(string: viewModel.getYear, attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)])
                
                mutableYear.append(year)
                dateLabel.attributedText = mutableYear
                nameLabel.text = viewModel.getName
                
                let inMutable: NSMutableAttributedString = NSMutableAttributedString()
                let inM = NSAttributedString(string: "de", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)])
                let inM2 = NSAttributedString(string: " \(viewModel.getMonth)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)])
                
                inMutable.append(inM)
                inMutable.append(inM2)
                
                contentLabel.text = viewModel.getDescription
            }
        }
    }

    let scrollContainer: UIScrollView = {
        let scrollContainer = UIScrollView(frame: CGRect.zero)
        scrollContainer.backgroundColor = UIColor.clear
        scrollContainer.translatesAutoresizingMaskIntoConstraints = false
        return scrollContainer
    }()
    
    let bannerImage: UIImageView = {
        let bannerImage = UIImageView(frame: CGRect.zero)
        bannerImage.contentMode = UIView.ContentMode.scaleToFill
        bannerImage.backgroundColor = UIColor.customWhite2
        bannerImage.image = UIImage(named: "underTest")
        bannerImage.translatesAutoresizingMaskIntoConstraints = false
        return bannerImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let dateLabel: UILabel = {
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        let text1 = NSAttributedString(string: "2019", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)])
        
        mutable.append(text1)
        
        let typeDateLabel = UILabel(frame: CGRect.zero)
        typeDateLabel.attributedText = mutable
        typeDateLabel.numberOfLines = 0
        typeDateLabel.textAlignment = NSTextAlignment.center
        typeDateLabel.backgroundColor = UIColor.clear
        typeDateLabel.adjustsFontSizeToFitWidth = true
        typeDateLabel.translatesAutoresizingMaskIntoConstraints = false
        return typeDateLabel
    }()
    
    let nameLabel: UILabel = {
        let nameLabel = UILabel(frame: CGRect.zero)
        nameLabel.text = "Miguel Gómez Jiménez"
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.bold)
        nameLabel.textAlignment = NSTextAlignment.center
        nameLabel.textColor = UIColor.white
        nameLabel.backgroundColor = UIColor.clear
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()
    
    let profileImage: UIImageView = {
        let profileImage = UIImageView(frame: CGRect.zero)
        profileImage.contentMode = UIView.ContentMode.scaleToFill
        profileImage.backgroundColor = UIColor.clear
        profileImage.layer.cornerRadius = 15
        profileImage.image = UIImage(named: "profilePlaceholder")
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        return profileImage
    }()
    
    let honorLabel: UILabel = {
        let honorLabel = UILabel(frame: CGRect.zero)
        honorLabel.text = "Cuadro de honor"
        honorLabel.numberOfLines = 0
        honorLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        honorLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        honorLabel.textAlignment = NSTextAlignment.center
        honorLabel.textColor = UIColor.white
        honorLabel.backgroundColor = UIColor.clear
        honorLabel.adjustsFontSizeToFitWidth = true
        honorLabel.translatesAutoresizingMaskIntoConstraints = false
        return honorLabel
    }()
    
    let inLabel: UILabel = {
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        let text1 = NSAttributedString(string: "de", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)])
        let text2 = NSAttributedString(string: " Junio", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)])
        
        mutable.append(text1)
        mutable.append(text2)
        
        let inLabel = UILabel(frame: CGRect.zero)
        inLabel.attributedText = mutable
        inLabel.numberOfLines = 0
        inLabel.textAlignment = NSTextAlignment.center
        inLabel.backgroundColor = UIColor.clear
        inLabel.adjustsFontSizeToFitWidth = true
        inLabel.translatesAutoresizingMaskIntoConstraints = false
        return inLabel
    }()
    
    let line: UIView = {
        let line = UIView(frame: CGRect.zero)
        line.backgroundColor = UIColor.lightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    let contentLabel: UILabel = {
        
        let text1: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text2: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text3: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text4: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text5: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text6: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text7: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text8: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text9: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        
        let textArray = [text1, text2, text3, text4, text5, text6, text7, text8, text9]
        
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        
        for item in textArray {
            let attributed = NSAttributedString(string: "\(item)\n\n")
            mutable.append(attributed)
        }
        
        let contentLabel = UILabel(frame: CGRect.zero)
//        contentLabel.attributedText = mutable
        //        newsContent.text = """
        //        \(text1)\n
        //        \(text2)\n
        //        \(text3)\n
        //        \(text4)\n
        //        \(text5)\n
        //        \(text6)\n
        //        \(text7)\n
        //        \(text8)\n
        //        \(text9)
        //        """
        
        contentLabel.numberOfLines = 0
        contentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        contentLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        contentLabel.textColor = UIColor.customGreen1
        contentLabel.backgroundColor = UIColor.clear
        contentLabel.translatesAutoresizingMaskIntoConstraints = false
        return contentLabel
    }()
    
    let lastMonthBtn: CustomButton3 = {
        let lastMonthBtn = CustomButton3(type: UIButton.ButtonType.custom)
        lastMonthBtn.setAttributedTitle(NSAttributedString(string: "Mes anterior", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
        lastMonthBtn.tag = 0
        lastMonthBtn.isHidden = true
        lastMonthBtn.translatesAutoresizingMaskIntoConstraints = false
        return lastMonthBtn
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(scrollContainer)
        scrollContainer.addSubview(bannerImage)
        bannerImage.addSubview(overlay)
        overlay.addSubview(inLabel)
        overlay.addSubview(honorLabel)
        overlay.addSubview(profileImage)
        overlay.addSubview(nameLabel)
        overlay.addSubview(dateLabel)
        scrollContainer.addSubview(line)
        scrollContainer.addSubview(contentLabel)
        scrollContainer.addSubview(lastMonthBtn)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            scrollContainer.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            scrollContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            bannerImage.topAnchor.constraint(equalTo: scrollContainer.topAnchor, constant: 0),
            bannerImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            bannerImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            bannerImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.55)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: bannerImage.topAnchor, constant: 0),
            overlay.leadingAnchor.constraint(equalTo: bannerImage.leadingAnchor, constant: 0),
            overlay.trailingAnchor.constraint(equalTo: bannerImage.trailingAnchor, constant: 0),
            overlay.bottomAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            inLabel.bottomAnchor.constraint(equalTo: overlay.bottomAnchor, constant: -40),
            inLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 23),
            inLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            honorLabel.bottomAnchor.constraint(equalTo: inLabel.topAnchor, constant: -5),
            honorLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 23),
            honorLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            profileImage.bottomAnchor.constraint(equalTo: honorLabel.topAnchor, constant: -10),
            profileImage.centerXAnchor.constraint(equalTo: overlay.centerXAnchor),
            profileImage.widthAnchor.constraint(equalToConstant: 30),
            profileImage.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([
            nameLabel.bottomAnchor.constraint(equalTo: profileImage.topAnchor, constant: -15),
            nameLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 40),
            nameLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -40)
            ])
        
        NSLayoutConstraint.activate([
            dateLabel.bottomAnchor.constraint(equalTo: nameLabel.topAnchor, constant: -15),
            dateLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 40),
            dateLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -40)
            ])
        
        NSLayoutConstraint.activate([
            line.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 20),
            line.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            line.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.45),
            line.heightAnchor.constraint(equalToConstant: 1)
            ])
        
        NSLayoutConstraint.activate([
            contentLabel.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 20),
            contentLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            contentLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23)
//            contentLabel.bottomAnchor.constraint(equalTo: scrollContainer.bottomAnchor, constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            lastMonthBtn.topAnchor.constraint(equalTo: contentLabel.bottomAnchor, constant: 10),
            lastMonthBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            lastMonthBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            lastMonthBtn.heightAnchor.constraint(equalToConstant: 50),
            lastMonthBtn.bottomAnchor.constraint(equalTo: scrollContainer.bottomAnchor, constant: -20)
            ])
    }

}
