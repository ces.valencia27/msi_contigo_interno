//
//  LoginView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    let welcomeText: UILabel = {
        let welcomeText = UILabel(frame: CGRect.zero)
        welcomeText.text = "Bienvenido!"
        welcomeText.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.black)
        welcomeText.textColor = UIColor.msiYellow
        welcomeText.textAlignment = NSTextAlignment.left
        welcomeText.numberOfLines = 0
        welcomeText.translatesAutoresizingMaskIntoConstraints = false
        return welcomeText
    }()
    
    let welcomeSubtitle: UILabel = {
        let welcomeSubtitle = UILabel(frame: CGRect.zero)
        welcomeSubtitle.text = "Grupo Multisistemas SA de CV"
        welcomeSubtitle.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        welcomeSubtitle.textColor = UIColor.customGray1
        welcomeSubtitle.textAlignment = NSTextAlignment.left
        welcomeSubtitle.numberOfLines = 0
        welcomeSubtitle.translatesAutoresizingMaskIntoConstraints = false
        return welcomeSubtitle
    }()
    
    let employeeNumberField: CustomTextField = {
        let employeeNumberField = CustomTextField(frame: CGRect.zero)
        employeeNumberField.layer.masksToBounds = true
        employeeNumberField.layer.cornerRadius = 5.0
        employeeNumberField.attributedPlaceholder = NSAttributedString(string: "RFC", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        employeeNumberField.textColor = UIColor.customGray1
        employeeNumberField.backgroundColor = UIColor.customBlue4
        employeeNumberField.keyboardType = UIKeyboardType.default
        employeeNumberField.keyboardAppearance = UIKeyboardAppearance.dark
        employeeNumberField.layer.borderWidth = 1
        employeeNumberField.layer.borderColor = UIColor.clear.cgColor
        employeeNumberField.clearButtonMode = UITextField.ViewMode.whileEditing
        employeeNumberField.tag = 0
        employeeNumberField.translatesAutoresizingMaskIntoConstraints = false
        return employeeNumberField
    }()
    
    let mailField: CustomTextField = {
        let mailField = CustomTextField(frame: CGRect.zero)
        mailField.layer.masksToBounds = true
        mailField.layer.cornerRadius = 5.0
        mailField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        mailField.textColor = UIColor.customGray1
        mailField.backgroundColor = UIColor.customBlue4
        mailField.keyboardType = UIKeyboardType.emailAddress
        mailField.keyboardAppearance = UIKeyboardAppearance.dark
        mailField.layer.borderWidth = 1
        mailField.layer.borderColor = UIColor.clear.cgColor
        mailField.clearButtonMode = UITextField.ViewMode.whileEditing
        mailField.tag = 1
        mailField.translatesAutoresizingMaskIntoConstraints = false
        return mailField
    }()
    
    let passField: CustomTextField = {
        let passField = CustomTextField(frame: CGRect.zero)
        passField.layer.masksToBounds = true
        passField.layer.cornerRadius = 5.0
        passField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customGray1])
        passField.textColor = UIColor.customGray1
        passField.backgroundColor = UIColor.customBlue4
        passField.keyboardType = UIKeyboardType.default
        passField.isSecureTextEntry = true
        passField.keyboardAppearance = UIKeyboardAppearance.dark
        passField.layer.borderWidth = 1
        passField.layer.borderColor = UIColor.clear.cgColor
        passField.clearButtonMode = UITextField.ViewMode.whileEditing
        passField.tag = 2
        passField.translatesAutoresizingMaskIntoConstraints = false
        return passField
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillEqually
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let enterBtn: PrimaryButton = {
        let enterBtn = PrimaryButton(type: UIButton.ButtonType.custom)
        enterBtn.setAttributedTitle(NSAttributedString(string: "Entrar", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.msiBlue]), for: UIControl.State.normal)
//        enterBtn.backgroundColor = UIColor.customBlue2
//        enterBtn.layer.masksToBounds = false
//        enterBtn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
//        enterBtn.layer.shadowRadius = 7.0
//        enterBtn.layer.shadowColor = UIColor.customBlue2.cgColor
//        enterBtn.layer.shadowOpacity = 0.5
//        enterBtn.layer.cornerRadius = 6.0
        enterBtn.tag = 0
        enterBtn.translatesAutoresizingMaskIntoConstraints = false
        return enterBtn
    }()
    
    let passRecoverBtn: UIButton = {
        let passRecoverBtn = UIButton(type: UIButton.ButtonType.custom)
        passRecoverBtn.setAttributedTitle(NSAttributedString(string: "Olvidé contraseña", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.customGray2]), for: UIControl.State.normal)
        passRecoverBtn.tag = 1
        passRecoverBtn.translatesAutoresizingMaskIntoConstraints = false
        return passRecoverBtn
    }()
    
    let footerText: UILabel = {
        let footerText = UILabel(frame: CGRect.zero)
        footerText.text = "Al ingresar, usted acepta"
        footerText.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        footerText.textColor = UIColor.customGray1
        footerText.textAlignment = NSTextAlignment.center
        footerText.numberOfLines = 0
        footerText.translatesAutoresizingMaskIntoConstraints = false
        return footerText
    }()
    
    let termsBtn: UIButton = {
        let termsBtn = UIButton(type: UIButton.ButtonType.custom)
        termsBtn.setAttributedTitle(NSAttributedString(string: "Términos y Privacidad", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular), NSAttributedString.Key.foregroundColor : UIColor.customBlue1]), for: UIControl.State.normal)
        termsBtn.tag = 3
        termsBtn.translatesAutoresizingMaskIntoConstraints = false
        return termsBtn
    }()
    
    let footerStack: UIStackView = {
        let footerStack = UIStackView(frame: CGRect.zero)
        footerStack.axis = NSLayoutConstraint.Axis.vertical
        footerStack.distribution = UIStackView.Distribution.fillEqually
        footerStack.translatesAutoresizingMaskIntoConstraints = false
        return footerStack
    }()
    
    let registerBtn: UIButton = {
        let registerBtn = UIButton(type: UIButton.ButtonType.custom)
        registerBtn.setAttributedTitle(NSAttributedString(string: "No tengo cuenta", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.customBlue1]), for: UIControl.State.normal)
        registerBtn.tag = 2
        registerBtn.translatesAutoresizingMaskIntoConstraints = false
        return registerBtn
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para inicializar los componentes
    private func setSubviews() {
        
        self.addSubview(welcomeText)
        self.addSubview(welcomeSubtitle)
        stackView.addArrangedSubview(employeeNumberField)
        stackView.addArrangedSubview(mailField)
        stackView.addArrangedSubview(passField)
        stackView.addArrangedSubview(enterBtn)
        stackView.addArrangedSubview(passRecoverBtn)
        self.addSubview(stackView)
        footerStack.addArrangedSubview(footerText)
        footerStack.addArrangedSubview(termsBtn)
        self.addSubview(footerStack)
        self.addSubview(registerBtn)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            welcomeText.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
            welcomeText.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            welcomeText.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            welcomeSubtitle.topAnchor.constraint(equalTo: welcomeText.bottomAnchor, constant: 5),
            welcomeSubtitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            welcomeSubtitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: welcomeSubtitle.bottomAnchor, constant: 50),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            stackView.heightAnchor.constraint(equalToConstant: 290)
            ])
        
        NSLayoutConstraint.activate([
            footerStack.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -10),
            footerStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            footerStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            footerStack.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.07)
            ])
        
        NSLayoutConstraint.activate([
            registerBtn.bottomAnchor.constraint(equalTo: footerStack.topAnchor, constant: -10),
            registerBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            registerBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            registerBtn.heightAnchor.constraint(equalToConstant: 50)
            ])
    }
}

