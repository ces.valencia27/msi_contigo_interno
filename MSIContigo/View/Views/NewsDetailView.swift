//
//  NewsDetailView.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class NewsDetailView: UIView {
    
    var modelView: NewsDetailViewModel? {
        didSet {
            if let viewModel = modelView {
                let urlString = Environment.baseURL.absoluteString + viewModel.getImage
                if let url = URL(string: urlString) {
                    bannerImage.downloaded(from: url, contentMode: UIView.ContentMode.scaleToFill)
                }
                
                let mutable: NSMutableAttributedString = NSMutableAttributedString()
                let text1 = NSAttributedString(string: viewModel.getCategory, attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)])
                let text2 = NSAttributedString(string: " \(viewModel.getDate)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)])
                mutable.append(text1)
                mutable.append(text2)
                typeDateLabel.attributedText = mutable
                newsTitle.text = viewModel.getTitle
                userName.text = viewModel.getAuthor
                
                let mutable2: NSMutableAttributedString = NSMutableAttributedString()
                let text3 = NSAttributedString(string: "en", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)])
                let text4 = NSAttributedString(string: " \(viewModel.getFilial)", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)])
                
                mutable2.append(text3)
                mutable2.append(text4)
                
                inLabel.attributedText = mutable2
                
                let mutable3 = NSMutableAttributedString()
                let attributed = NSAttributedString(string: "\(viewModel.getContent)\n")
                mutable3.append(attributed)
                
                newsContent.attributedText = mutable3
            }
        }
    }
    
    let scrollContainer: UIScrollView = {
        let scrollContainer = UIScrollView(frame: CGRect.zero)
        scrollContainer.backgroundColor = UIColor.clear
        scrollContainer.translatesAutoresizingMaskIntoConstraints = false
        return scrollContainer
    }()
    
    let bannerImage: UIImageView = {
        let bannerImage = UIImageView(frame: CGRect.zero)
        bannerImage.contentMode = UIView.ContentMode.scaleToFill
        bannerImage.backgroundColor = UIColor.customWhite2
        bannerImage.image = UIImage(named: "underTest")
        bannerImage.translatesAutoresizingMaskIntoConstraints = false
        return bannerImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let typeDateLabel: UILabel = {
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        let text1 = NSAttributedString(string: "SEGURIDAD", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.bold)])
        let text2 = NSAttributedString(string: " 30 Nov 2019", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)])
        
        mutable.append(text1)
        mutable.append(text2)
        
        let typeDateLabel = UILabel(frame: CGRect.zero)
        typeDateLabel.attributedText = mutable
        typeDateLabel.numberOfLines = 0
        typeDateLabel.textAlignment = NSTextAlignment.center
        typeDateLabel.backgroundColor = UIColor.clear
        typeDateLabel.adjustsFontSizeToFitWidth = true
        typeDateLabel.translatesAutoresizingMaskIntoConstraints = false
        return typeDateLabel
    }()
    
    let newsTitle: UILabel = {
        let newsTitle = UILabel(frame: CGRect.zero)
        newsTitle.text = "10 precausiones a tener en la CDMX"
        newsTitle.numberOfLines = 0
        newsTitle.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.bold)
        newsTitle.textAlignment = NSTextAlignment.center
        newsTitle.textColor = UIColor.white
        newsTitle.backgroundColor = UIColor.clear
        newsTitle.adjustsFontSizeToFitWidth = true
        newsTitle.translatesAutoresizingMaskIntoConstraints = false
        return newsTitle
    }()
    
    let profileImage: UIImageView = {
        let profileImage = UIImageView(frame: CGRect.zero)
        profileImage.contentMode = UIView.ContentMode.scaleToFill
        profileImage.backgroundColor = UIColor.clear
        profileImage.layer.cornerRadius = 15
        profileImage.image = UIImage(named: "profilePlaceholder")
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        return profileImage
    }()
    
    let userName: UILabel = {
        let userName = UILabel(frame: CGRect.zero)
        userName.text = "Alicia Rosas"
        userName.numberOfLines = 0
        userName.lineBreakMode = NSLineBreakMode.byWordWrapping
        userName.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        userName.textAlignment = NSTextAlignment.center
        userName.textColor = UIColor.white
        userName.backgroundColor = UIColor.clear
        userName.adjustsFontSizeToFitWidth = true
        userName.translatesAutoresizingMaskIntoConstraints = false
        return userName
    }()
    
    let inLabel: UILabel = {
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        let text1 = NSAttributedString(string: "en", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)])
        let text2 = NSAttributedString(string: " MSI Digital", attributes: [NSAttributedString.Key.foregroundColor : UIColor.customPurple1, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)])
        
        mutable.append(text1)
        mutable.append(text2)
        
        let inLabel = UILabel(frame: CGRect.zero)
        inLabel.attributedText = mutable
        inLabel.numberOfLines = 0
        inLabel.textAlignment = NSTextAlignment.center
        inLabel.backgroundColor = UIColor.clear
        inLabel.adjustsFontSizeToFitWidth = true
        inLabel.translatesAutoresizingMaskIntoConstraints = false
        return inLabel
    }()
    
    let line: UIView = {
        let line = UIView(frame: CGRect.zero)
        line.backgroundColor = UIColor.lightGray
        line.translatesAutoresizingMaskIntoConstraints = false
        return line
    }()
    
    let newsContent: UILabel = {
        
        let text1: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text2: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text3: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text4: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text5: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text6: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text7: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text8: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        let text9: String = "Those crafty Chinese climate hoaxsters have been at it again. Last week, there were wildfires in Greece that got so bad that people were jumping into the ocean for safety. There are now strong indications that these fires may have been set, but it was those clever Chinese hoaxsters and their climate scam that made the fires as bad as they were. From The Guardian:"
        
        let textArray = [text1, text2, text3, text4, text5, text6, text7, text8, text9]
        
        var mutable: NSMutableAttributedString = NSMutableAttributedString()
        
        for item in textArray {
            let attributed = NSAttributedString(string: "\(item)\n\n")
            mutable.append(attributed)
        }
        
        let newsContent = UILabel(frame: CGRect.zero)
        newsContent.attributedText = mutable
        
        newsContent.numberOfLines = 0
        newsContent.lineBreakMode = NSLineBreakMode.byWordWrapping
        newsContent.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        newsContent.textColor = UIColor.customGreen1
        newsContent.backgroundColor = UIColor.clear
        newsContent.translatesAutoresizingMaskIntoConstraints = false
        return newsContent
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(scrollContainer)
        scrollContainer.addSubview(bannerImage)
        bannerImage.addSubview(overlay)
        overlay.addSubview(inLabel)
        overlay.addSubview(userName)
        overlay.addSubview(profileImage)
        overlay.addSubview(newsTitle)
        overlay.addSubview(typeDateLabel)
        scrollContainer.addSubview(line)
        scrollContainer.addSubview(newsContent)
    }
    
    // MARK: - Método para definier al autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            scrollContainer.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            scrollContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            bannerImage.topAnchor.constraint(equalTo: scrollContainer.topAnchor, constant: 0),
            bannerImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            bannerImage.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            bannerImage.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.55)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: bannerImage.topAnchor, constant: 0),
            overlay.leadingAnchor.constraint(equalTo: bannerImage.leadingAnchor, constant: 0),
            overlay.trailingAnchor.constraint(equalTo: bannerImage.trailingAnchor, constant: 0),
            overlay.bottomAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            inLabel.bottomAnchor.constraint(equalTo: overlay.bottomAnchor, constant: -40),
            inLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 23),
            inLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            userName.bottomAnchor.constraint(equalTo: inLabel.topAnchor, constant: -5),
            userName.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 23),
            userName.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -23)
            ])
        
        NSLayoutConstraint.activate([
            profileImage.bottomAnchor.constraint(equalTo: userName.topAnchor, constant: -10),
            profileImage.centerXAnchor.constraint(equalTo: overlay.centerXAnchor),
            profileImage.widthAnchor.constraint(equalToConstant: 30),
            profileImage.heightAnchor.constraint(equalToConstant: 30)
            ])
        
        NSLayoutConstraint.activate([
            newsTitle.bottomAnchor.constraint(equalTo: profileImage.topAnchor, constant: -15),
            newsTitle.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 40),
            newsTitle.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -40)
            ])
        
        NSLayoutConstraint.activate([
            typeDateLabel.bottomAnchor.constraint(equalTo: newsTitle.topAnchor, constant: -15),
            typeDateLabel.leadingAnchor.constraint(equalTo: overlay.leadingAnchor, constant: 40),
            typeDateLabel.trailingAnchor.constraint(equalTo: overlay.trailingAnchor, constant: -40)
            ])
        
        NSLayoutConstraint.activate([
            line.topAnchor.constraint(equalTo: bannerImage.bottomAnchor, constant: 20),
            line.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            line.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.45),
            line.heightAnchor.constraint(equalToConstant: 1)
            ])
        
        NSLayoutConstraint.activate([
            newsContent.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 20),
            newsContent.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23),
            newsContent.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -23),
            newsContent.bottomAnchor.constraint(equalTo: scrollContainer.bottomAnchor, constant: 0)
            ])
    }

}
