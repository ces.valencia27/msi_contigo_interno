//
//  WallView.swift
//  MSIContigo
//
//  Created by David Valencia on 9/14/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import UIKit

class WallView: UIView {
    
    let wallCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        
        let wallCollection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        wallCollection.register(WallCell.self, forCellWithReuseIdentifier: "wallCell")
        wallCollection.backgroundColor = UIColor.clear
        wallCollection.isPagingEnabled = true
        wallCollection.bounces = false
        wallCollection.translatesAutoresizingMaskIntoConstraints = false
        return wallCollection
    }()
    
    let pageControl: UIPageControl = {
        let pageControl = UIPageControl(frame: CGRect.zero)
        pageControl.currentPage = 0
        pageControl.numberOfPages = 4
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.gray
        pageControl.backgroundColor = UIColor.clear
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        return pageControl
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // MARK: - Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(wallCollection)
        self.addSubview(pageControl)
    }
    
    // MARK: - Método para definir el autolyaout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            wallCollection.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            wallCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            wallCollection.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor),
            wallCollection.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            pageControl.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            pageControl.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -10)
            ])
    }
}
