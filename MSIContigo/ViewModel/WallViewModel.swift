//
//  WallViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct WallViewModel {
    
    fileprivate let wall: WallModel
    
    init(wall: WallModel) {
        
        self.wall = wall
    }
    
    public var getID: Int { return wall.id }
    public var getTitle: String { return wall.titulo }
    public var getSubtitle: String { return wall.subtitulo }
    public var getFilial: String { return wall.filial }
    public var getImage: String { return wall.imagen }
}
