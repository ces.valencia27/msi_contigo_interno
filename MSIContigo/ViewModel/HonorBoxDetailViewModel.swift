//
//  HonorBoxDetailViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct HonorBoxDetailViewModel {
    
    fileprivate let detail: HonorDetailModel
    
    init(detail: HonorDetailModel) {
        
        self.detail = detail
    }
    
    public var getName: String { return detail.nombre }
    public var getDescription: String { return detail.descripcion }
    public var getYear: String { return detail.anio }
    public var getMonth: String { return detail.mes }
    public var getImage: String { return detail.imagen }
    public var getDate: Int { return detail.fecha_unix }
}
