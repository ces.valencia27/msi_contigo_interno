//
//  EmployeeViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/15/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct EmployeeViewModel {
    
    private let employee: EmployeeModel
    
    init(employee: EmployeeModel) {
        
        self.employee = employee
    }
    
    var getName: String { return employee.nombre }
    var getMail: String { return employee.email }
    var getNumber: String { return employee.numero }
}
