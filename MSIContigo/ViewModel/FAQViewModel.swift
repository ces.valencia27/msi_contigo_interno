//
//  FAQViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct FAQViewModel {
    
    fileprivate let faq: FAQModel
    
    init(faq: FAQModel) {
        
        self.faq = faq
    }
    
    public var getQuestion: String { return faq.pregunta }
    public var getAnswer: String { return faq.respuesta }
}
