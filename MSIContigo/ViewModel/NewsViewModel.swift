//
//  NewsViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/16/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

struct NewsViewModel {
    
    fileprivate let noticia: NewsModel
    
    init(noticia: NewsModel) {
        
        self.noticia = noticia
    }
    
    var getID: Int { return noticia.id }
    var getTitle: String { return noticia.titulo }
    var getDate: String { return noticia.fecha }
    var getImg: String { return noticia.imagen }
}
