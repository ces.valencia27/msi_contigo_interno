//
//  HomeCellViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

struct HomeCellViewModel {
    
    private let cellModel: HomeCellModel
    
    init(cellModel: HomeCellModel) {
        self.cellModel = cellModel
    }
    
    var getIcon: UIImage { return cellModel.icon }
    var getName: String { return cellModel.name }
}
