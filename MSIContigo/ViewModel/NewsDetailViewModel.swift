//
//  NewsDetailViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct NewsDetailViewModel {
    
    fileprivate let detail: NewsDetailModel
    
    init(detail: NewsDetailModel) {
        
        self.detail = detail
    }
    
    public var getTitle: String { return detail.titulo }
    public var getContent: String { return detail.contenido }
    public var getAuthor: String { return detail.autor }
    public var getCategory: String { return detail.categoria }
    public var getDate: String { return detail.fecha }
    public var getFilial: String { return detail.filial }
    public var getImage: String { return detail.imagen }
}
