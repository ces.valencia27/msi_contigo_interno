//
//  AvisosViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/16/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

struct AvisosViewModel {
    
    private let aviso: AddModel
    
    init(aviso: AddModel) {
        self.aviso = aviso
    }
    
    var getEmisor: String { return aviso.emisor }
    var getTitutlo: String { return aviso.titulo }
    var getMensaje: String { return aviso.mensaje }
    var getFecha: String { return aviso.fecha }
}
