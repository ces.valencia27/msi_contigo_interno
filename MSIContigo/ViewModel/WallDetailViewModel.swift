//
//  WallDetailViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct WallDetailViewModel {
    
    fileprivate let detail: WallDetailModel
    
    init(detail: WallDetailModel) {
        
        self.detail = detail
    }
    
    public var getTitle: String { return detail.titulo }
    public var getSubtitle: String { return detail.subtitulo }
    public var getContent: String { return detail.contenido }
    public var getDate: String { return detail.fecha }
    public var getFilial: String { return detail.filial }
    public var getImage: String { return detail.imagen }
}
