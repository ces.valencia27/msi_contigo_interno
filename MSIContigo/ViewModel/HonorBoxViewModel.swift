//
//  HonorBoxViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct HonorBoxViewModel {
    
    fileprivate let person: PersonModel
    
    init(person: PersonModel) {
        
        self.person = person
    }
    
    public var getID: Int { return person.id }
    public var getName: String { return person.nombre }
    public var getDate: String { return person.fecha }
    public var getImage: String { return person.imagen }
}
