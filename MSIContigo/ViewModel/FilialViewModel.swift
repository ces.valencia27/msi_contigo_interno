//
//  FilialViewModel.swift
//  MSIContigo
//
//  Created by David Valencia on 9/16/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct FilialViewModel {
    
    fileprivate let filial: FilialModel
    
    init(filial: FilialModel) {
        
        self.filial = filial
    }
    
    var getID: Int { return filial.id }
    var getName: String { return filial.nombre }
}
