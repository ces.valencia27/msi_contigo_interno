//
//  AppURLS.swift
//  CodeChallenge
//
//  Created by David Valencia on 9/5/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation

public enum Environment {

    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError("El archivo con extensión .plist no fue encontrado")
        }
        return dict
    }()
    
    static let baseURL: URL = {
//        guard let rootURLstring = Environment.infoDictionary["ROOT_URL"] as? String else {
//            fatalError("La URL Base no existe en el archivo info.plist")
//        }
        let rootURLstring = "http://165.22.184.148"
        guard let url = URL(string: rootURLstring) else {
            fatalError("La URL es inválida")
        }
        return url
    }()
}

public enum Endpoints {
    
    static var login: String { return "/services/login" }
    static var getEmpleadoInfo: String { return "/services/getEmpleadoInfo" }
    static var getFiliales: String { return "/services/getFiliales" }
    static var getAvisos: String { return "/services/getAvisos" }
    static var getNoticias: String { return "/services/getNoticias" }
    static var getNoticiaDetail: String { return "/services/noticiaDetail" }
    static var getNoticiasByFilial: String { return "/services/getNoticiasByFilial" }
    static var getMurales: String { return "/services/getMurales" }
    static var getMuralDetail: String { return "/services/muralDetail" }
    static var getC_Honor: String { return "/services/getC_Honor" }
    static var getC_HonorDetail: String { return "/services/getC_HonorDetail" }
    static var getFAQ: String { return "/services/getFAQ" }
    static var getC_HonorDetailPreviusMonth: String { return "/services/getC_HonorDetailPreviusMonth" }
    static var getAreas: String { return "/services/getAreas" }
    static var newMensaje: String { return "/services/newMensaje" }
    static var updateOneSignal: String { return "/services/updateOneSignal" }
    static var restorePassword: String { return "/services/restorePassword" }
    static var newSolicitud: String { return "/services/newSolicitud" }
}

public enum KeychainValues {

    static var service: String { return "msiService" }
    static var account: String { return "msiAccount" }
}
