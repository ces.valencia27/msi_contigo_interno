//
//  NetworkManager.swift
//  CodeChallenge
//
//  Created by David Valencia on 9/5/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation
import UIKit

enum ApiError: Error {
    
    case responseError
    case decodingError
    case otherError
}

protocol URL_SessionDelegate: class {
    
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary)
    func connectionFinishWithError(session: URL_Session, error: Error)
}

class URL_Session: NSObject, URLSessionDelegate, URLSessionDataDelegate {
    
    var dataTask: URLSessionDataTask?
    var responseData: Data = Data()
    var httpResponse: HTTPURLResponse?
    let headers = [
        "Content-Type": "application/json",
        "Authorization": KeychainService.loadPassword(service: KeychainValues.service, account: KeychainValues.account) ?? ""
    ]
    weak var delegate: URL_SessionDelegate?
    
    override init() {
        super.init()
    }
    
    // MARK: - Método POST para consumir el servicio de newSolicitud
    func newSolicitudRequest(_ parametros: NewSolicitudRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.newSolicitud
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "POST"
            request.timeoutInterval = 60
            responseData = Data()
            
            let enconded = try? JSONEncoder().encode(parametros)
            let json = try! JSONSerialization.jsonObject(with: enconded!, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            if let postData = enconded {
                request.httpBody = postData
                print(json)
                
                let localHeaders = [
                    "Content-Type": "application/json"
                ]
                
                request.allHTTPHeaderFields = localHeaders
            } else {
                print("No estás enviando un tipo de dato DATA")
            }
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            debugPrint("Error en el desempaquetado")
        }

    }
    
    // MARK: - Método POST para consumir el servicio de login
    func loginRequest(_ parametros: LoginRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.login
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "POST"
            request.timeoutInterval = 60
            responseData = Data()
            
            let enconded = try? JSONEncoder().encode(parametros)
            let json = try! JSONSerialization.jsonObject(with: enconded!, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            if let postData = enconded {
                request.httpBody = postData
                print(json)
                request.allHTTPHeaderFields = headers
            } else {
                print("No estás enviando un tipo de dato DATA")
            }
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            debugPrint("Error en el desempaquetado")
        }

    }

    // MARK: - Método GET para consumir el servicio de getEmpleadoInfo
    func getEmpleadoInfoRequest(_ parametros: EmployeeRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.getEmpleadoInfo
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
//        urlComponents?.queryItems = [
//            URLQueryItem(name: "token", value: parametros.token)
//        ]
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
    // MARK: - Método GET para consumir el servicio de getFiliales
    func getFilialesRequest() {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.getFiliales
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
//        urlComponents?.queryItems = [
//            URLQueryItem(name: "token", value: parametros.token)
//        ]
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
    // MARK: - Método GET para consumir el servicio de getAvisos
    func getAvisosRequest(_ parametros: AddRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.getAvisos
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
//        urlComponents?.queryItems = [
//            URLQueryItem(name: "token", value: parametros.token)
//        ]
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
    // MARK: - Método GET para consumir el servicio de getNoticias
    func getNoticiasRequest(_ parametros: NewsRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.getNoticias
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
//        urlComponents?.queryItems = [
//            URLQueryItem(name: "token", value: parametros.token)
//        ]
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
        // MARK: - Método GET para consumir el servicio de getNoticiaDetail
        func getNoticiaDetailRequest(_ parametros: NewsDetailRequestModel) {
            
            //TIP: Esto evita que se hagan peticiones de forma simultánea
            if dataTask != nil {
                dataTask?.cancel()
            }
            
            let urlString = Environment.baseURL.absoluteString + Endpoints.getNoticiaDetail + "/" + (parametros.noticia_id ?? "")
            let urlComponents = NSURLComponents(string: urlString)
            let sessionCofiguration = URLSessionConfiguration.default
            let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
            
//            urlComponents?.queryItems = [
//                URLQueryItem(name: "", value: parametros.noticia_id)
//            ]
            
            if let urlComp = urlComponents {
                
                var request = URLRequest(url: urlComp.url!)
                request.httpMethod = "GET"
                request.timeoutInterval = 60
                request.allHTTPHeaderFields = headers
                
                responseData = Data()
                
                dataTask = defaultSession.dataTask(with: request)
                dataTask?.resume()
            } else {
                print("Error en los componentes")
            }
        }
    
        // MARK: - Método GET para consumir el servicio de getMurales
        func getMuralesRequest(_ parametros: WallRequestModel) {
            
            //TIP: Esto evita que se hagan peticiones de forma simultánea
            if dataTask != nil {
                dataTask?.cancel()
            }
            
            let urlString = Environment.baseURL.absoluteString + Endpoints.getMurales
            let urlComponents = NSURLComponents(string: urlString)
            let sessionCofiguration = URLSessionConfiguration.default
            let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
            
    //        urlComponents?.queryItems = [
    //            URLQueryItem(name: "token", value: parametros.token)
    //        ]
            
            if let urlComp = urlComponents {
                
                var request = URLRequest(url: urlComp.url!)
                request.httpMethod = "GET"
                request.timeoutInterval = 60
                request.allHTTPHeaderFields = headers
                
                responseData = Data()
                
                dataTask = defaultSession.dataTask(with: request)
                dataTask?.resume()
            } else {
                print("Error en los componentes")
            }
        }
    
    // MARK: - Método GET para consumir el servicio de getNoticiaDetail
    func getMuralDetail(_ parametros: WallDetailRequestModel) {
                
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
                
        let urlString = Environment.baseURL.absoluteString + Endpoints.getMuralDetail + "/" + "\(parametros.mural_id ?? 0)"
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
                
    //            urlComponents?.queryItems = [
    //                URLQueryItem(name: "", value: parametros.noticia_id)
    //            ]
                
        if let urlComp = urlComponents {
                    
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
        // MARK: - Método GET para consumir el servicio de getC_Honor
    func getC_Honor(_ parametros: HonorBoxRequestModel) {
            
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
            }
            
        let urlString = Environment.baseURL.absoluteString + Endpoints.getC_Honor
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
            
//        urlComponents?.queryItems = [
//            URLQueryItem(name: "token", value: parametros.token)
//        ]
            
        if let urlComp = urlComponents {
                
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
    // MARK: - Método GET para consumir el servicio de getC_HonorDetail
    func getC_HonorDetail(_ parametros: HonorDetailRequestModel) {
                
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
                
        let urlString = Environment.baseURL.absoluteString + Endpoints.getC_HonorDetail + "/" + "\(parametros.c_honor_id ?? 0)"
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
                
    //            urlComponents?.queryItems = [
    //                URLQueryItem(name: "", value: parametros.noticia_id)
    //            ]
                
        if let urlComp = urlComponents {
                    
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }

        // MARK: - Método GET para consumir el servicio de getFAQ
        func getFAQ(_ parametros: FAQRequestModel) {
            
            //TIP: Esto evita que se hagan peticiones de forma simultánea
            if dataTask != nil {
                dataTask?.cancel()
            }
            
            let urlString = Environment.baseURL.absoluteString + Endpoints.getFAQ
            let urlComponents = NSURLComponents(string: urlString)
            let sessionCofiguration = URLSessionConfiguration.default
            let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
            
    //        urlComponents?.queryItems = [
    //            URLQueryItem(name: "token", value: parametros.token)
    //        ]
            
            if let urlComp = urlComponents {
                
                var request = URLRequest(url: urlComp.url!)
                request.httpMethod = "GET"
                request.timeoutInterval = 60
                request.allHTTPHeaderFields = headers
                
                responseData = Data()
                
                dataTask = defaultSession.dataTask(with: request)
                dataTask?.resume()
            } else {
                print("Error en los componentes")
            }
        }
    
    // MARK: - Método GET para consumir el servicio de getC_HonorDetailPreviusMonth
    func getC_HonorDetailPreviusMonth(_ parametros: LastHonorDetailRequestModel) {
                
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
                
        let urlString = Environment.baseURL.absoluteString + Endpoints.getC_HonorDetailPreviusMonth + "/\(parametros.fecha_unix ?? "")"
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
                
    //            urlComponents?.queryItems = [
    //                URLQueryItem(name: "", value: parametros.noticia_id)
    //            ]
                
        if let urlComp = urlComponents {
                    
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
    // MARK: - Método GET para consumir el servicio de getAreas
    func getAreas(_ parametros: AreasRequestModel) {
                
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
                
        let urlString = Environment.baseURL.absoluteString + Endpoints.getAreas
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
                
    //            urlComponents?.queryItems = [
    //                URLQueryItem(name: "", value: parametros.noticia_id)
    //            ]
                
        if let urlComp = urlComponents {
                    
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            request.allHTTPHeaderFields = headers
            
            responseData = Data()
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            print("Error en los componentes")
        }
    }
    
    // MARK: - Método POST para consumir el servicio de newMensaje
    func newMensaje(_ parametros: NewMessageRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.newMensaje
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "POST"
            request.timeoutInterval = 60
            responseData = Data()
            
            let enconded = try? JSONEncoder().encode(parametros)
            let json = try! JSONSerialization.jsonObject(with: enconded!, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            if let postData = enconded {
                request.httpBody = postData
                print(json)
                request.allHTTPHeaderFields = headers
            } else {
                print("No estás enviando un tipo de dato DATA")
            }
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            debugPrint("Error en el desempaquetado")
        }

    }
    
    // MARK: - Método POST para consumir el servicio de restorePassword
    func restorePassword(_ parametros: PassRecoverRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.restorePassword
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "POST"
            request.timeoutInterval = 60
            responseData = Data()
            
            let enconded = try? JSONEncoder().encode(parametros)
            let json = try! JSONSerialization.jsonObject(with: enconded!, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            if let postData = enconded {
                request.httpBody = postData
                print(json)
                request.allHTTPHeaderFields = headers
            } else {
                print("No estás enviando un tipo de dato DATA")
            }
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            debugPrint("Error en el desempaquetado")
        }

    }
    
    // MARK: - Método POST para consumir el servicio de restorePassword
    func updateOneSignal(_ parametros: UpdatePushRequestModel) {
        
        //TIP: Esto evita que se hagan peticiones de forma simultánea
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + Endpoints.updateOneSignal
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "POST"
            request.timeoutInterval = 60
            responseData = Data()
            
            let enconded = try? JSONEncoder().encode(parametros)
            let json = try! JSONSerialization.jsonObject(with: enconded!, options: JSONSerialization.ReadingOptions.mutableContainers)
            
            if let postData = enconded {
                request.httpBody = postData
                print(json)
                request.allHTTPHeaderFields = headers
            } else {
                print("No estás enviando un tipo de dato DATA")
            }
            
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            debugPrint("Error en el desempaquetado")
        }

    }
    
    // Método que evalúa si el task finalizó con éxito o error
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        if error == nil {
            if let resultado = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? NSDictionary {
                
                delegate?.connectionFinishSuccessfull(session: self, response: resultado)
            } else {
                debugPrint("Ocurrió un error en la serialización del JSON")
            }
        } else {
            delegate?.connectionFinishWithError(session: self, error: error!)
        }
    }
    
    // Método que descarga la representación de bytes
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        responseData.append(data)
    }
    
    // Método que cacha la respuesta del servidor
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        httpResponse = response as? HTTPURLResponse
        
        if httpResponse?.statusCode == 200 {
            completionHandler(URLSession.ResponseDisposition.allow)
        } else {
            completionHandler(URLSession.ResponseDisposition.cancel)
        }
        
    }
    
}



