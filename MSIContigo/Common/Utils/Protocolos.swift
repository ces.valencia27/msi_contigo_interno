//
//  Protocolos.swift
//  MSIContigo
//
//  Created by David Valencia on 8/30/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

// MARK: Protocolo para obtener el nombre de la filial seleccionada en AddFilialVC
protocol GetFilialProtocol: class {
    func selectedFilial(filial: FilialModel)
}
