//
//  CustomLabel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

class CustomLabel: UILabel {
    
    override func draw(_ rect: CGRect) {
        
        let customInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
        super.drawText(in: bounds.inset(by: customInsets))
    }
}

