//
//  UIColorExtension.swift
//  MSIContigo
//
//  Created by David Valencia on 8/25/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static var customBlack1: UIColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
    static var customGray1: UIColor = UIColor(red: 166/255, green: 166/255, blue: 166/255, alpha: 1)
    static var customGray2: UIColor = UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 1)
    static var customGray3: UIColor = UIColor(red: 150/255, green: 159/255, blue: 162/255, alpha: 1)
    static var customGray4: UIColor = UIColor(red: 172/255, green: 172/255, blue: 172/255, alpha: 1)
    static var customWhite1: UIColor = UIColor(red: 244/255, green: 246/255, blue: 246/255, alpha: 1)
    static var customWhite2: UIColor = UIColor(red: 231/255, green: 234/255, blue: 234/255, alpha: 1)
    static var customBlue1: UIColor = UIColor(red: 53/255, green: 123/255, blue: 230/255, alpha: 1)
    static var customBlue2: UIColor = UIColor(red: 50/255, green: 76/255, blue: 188/255, alpha: 1)
    static var customBlue3: UIColor = UIColor(red: 56/255, green: 87/255, blue: 183/255, alpha: 1)
    static var customBlue4: UIColor = UIColor(red: 25/255, green: 38/255, blue: 70/255, alpha: 1)
    static var customBlue5: UIColor = UIColor(red: 32/255, green: 52/255, blue: 102/255, alpha: 1)
    static var customBlue6: UIColor = UIColor(red: 6/255, green: 9/255, blue: 44/255, alpha: 1)
    static var customBlue7: UIColor = UIColor(red: 51/255, green: 58/255, blue: 149/255, alpha: 1)
    static var customBlue8: UIColor = UIColor(red: 26/255, green: 43/255, blue: 97/255, alpha: 1)
    static var msiBlue: UIColor = UIColor(red: 15/255, green: 23/255, blue: 42/255, alpha: 1)
    static var customBlack2: UIColor = UIColor(red: 16/255, green: 16/255, blue: 16/255, alpha: 1)
    static var customBlack3: UIColor = UIColor(red: 59/255, green: 59/255, blue: 59/255, alpha: 1)
    static var customGreen1: UIColor = UIColor(red: 69/255, green: 81/255, blue: 84/255, alpha: 1)
    static var customPurple1: UIColor = UIColor(red: 75/255, green: 90/255, blue: 253/255, alpha: 1)
    static var msiYellow: UIColor = UIColor(red: 238/255, green: 199/255, blue: 45/255, alpha: 1)
    static var customYellow1: UIColor = UIColor(red: 226/255, green: 187/255, blue: 58/255, alpha: 1)
}
