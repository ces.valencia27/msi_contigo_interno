//
//  LoaderView.swift
//  CodeChallenge
//
//  Created by David Valencia on 9/5/19.
//  Copyright © 2019 david. All rights reserved.
//

import Foundation
import UIKit

class LoaderView {
    
    static let sharedProperty = LoaderView()
    let container: UIView = {
        let container = UIView(frame: CGRect.zero)
        container.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    let loadingView: UIView = {
        let loadingView = UIView(frame: CGRect.zero)
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        return loadingView
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect.zero)
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }()
    
    // Método que muestra un activity indicator
    func showActivityIndicatory(uiView: UIView) {
        
        uiView.addSubview(container)
        container.addSubview(loadingView)
        loadingView.addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: uiView.topAnchor),
            container.trailingAnchor.constraint(equalTo: uiView.trailingAnchor),
            container.bottomAnchor.constraint(equalTo: uiView.bottomAnchor),
            container.leadingAnchor.constraint(equalTo: uiView.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            loadingView.centerXAnchor.constraint(equalTo: container.centerXAnchor),
            loadingView.centerYAnchor.constraint(equalTo: container.centerYAnchor),
            loadingView.widthAnchor.constraint(equalToConstant: 80),
            loadingView.heightAnchor.constraint(equalToConstant: 80)
            ])
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor),
            activityIndicator.widthAnchor.constraint(equalToConstant: 40),
            activityIndicator.heightAnchor.constraint(equalToConstant: 40)
            ])
        
        activityIndicator.startAnimating()
    }
    
    // Método que oculta un activity indicator
    func hideActivityIndicator(uiView: UIView) {
        
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    // Método que devuelve una instancia de la clase
    static func shared() -> LoaderView { return sharedProperty }
}


