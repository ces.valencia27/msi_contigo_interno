//
//  StaticMethods.swift
//  MSIContigo
//
//  Created by David Valencia on 8/28/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

class StaticMethods {
    
    static private let sharedInstance = StaticMethods()
    
    func getGradiendBG(_ view: UIButton, title: String, tag: Int) {
        
        // Border:
        
        let borderGradientTop = UIColor.customBlue6.cgColor
        let borderGradientBottom = UIColor.customBlue7.cgColor
        let borderGradient = CAGradientLayer()
        borderGradient.frame = view.bounds
        borderGradient.colors = [borderGradientTop, borderGradientBottom]
        borderGradient.cornerRadius = 6.0
        
        let borderMask = CAShapeLayer()
        borderMask.lineWidth = 1
        borderMask.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 6.0).cgPath
        borderMask.fillColor = UIColor.clear.cgColor
        borderMask.strokeColor = UIColor.black.cgColor
        
        borderGradient.mask = borderMask
        view.layer.insertSublayer(borderGradient, below: view.titleLabel?.layer)
        
        // Background:
        let backgroundGradientTop = UIColor.customBlue6.cgColor
        let backgroundGradientBottom = UIColor.customBlue7.cgColor
        let background = CAGradientLayer()
        background.frame = view.bounds
        background.colors = [backgroundGradientTop, backgroundGradientBottom]
        background.cornerRadius = 6.0
        view.layer.insertSublayer(background, below: borderGradient)
        
        // Shadow:
        view.layer.shadowColor = UIColor.customBlue2.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = 7.0
        view.layer.masksToBounds = false
        
        // Text
        view.setAttributedTitle(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.white]), for: UIControl.State.normal)
        view.tag = tag
    }
    
    func clearGradient(_ view: UIButton, title: String, tag: Int) {
        
//        view.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        view.layer.sublayers?.forEach({ (item) in
            if let layer = item as? CAGradientLayer {
                layer.removeFromSuperlayer()
            } else {
                debugPrint("No se obtuvo valor")
            }
        })
        view.setAttributedTitle(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.black), NSAttributedString.Key.foregroundColor : UIColor.customBlue2]), for: UIControl.State.normal)
        view.backgroundColor = UIColor.white
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 6.0
        view.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        view.layer.shadowRadius = 7.0
        view.layer.shadowColor = UIColor.customBlue2.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.borderColor = UIColor.customBlue2.cgColor
        view.layer.borderWidth = 2
        view.tag = tag
    
    }
    
    static public func shared() -> StaticMethods { return sharedInstance }
}
