//
//  CustomButtom.swift
//  MSIContigo
//
//  Created by David Valencia on 8/25/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

class CustomButon: UIButton {
    
    override func draw(_ rect: CGRect) {
        
        // Gradiente Vertical
        let reference = UIGraphicsGetCurrentContext()
        let color1 = UIColor(red: 174/255, green: 174/255, blue: 185/255, alpha: 1).cgColor
        let color2 = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
        
        
        let locations: [CGFloat] = [0.0, 1.0]
        let colors = [color1, color2]
        let space = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: space, colors: colors as CFArray, locations: locations)
        reference?.drawLinearGradient(gradient!, start: CGPoint(x: 0, y: UIScreen.main.bounds.height), end: CGPoint(x: UIScreen.main.bounds.width, y: UIScreen.main.bounds.height), options: .drawsAfterEndLocation)
        
    }
}

class PrimaryButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        
        // Border:
        
        let borderGradientTop = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
        let borderGradientBottom = UIColor(red: 174/255, green: 174/255, blue: 185/255, alpha: 1).cgColor
        let borderGradient = CAGradientLayer()
        borderGradient.frame = self.bounds
        borderGradient.colors = [borderGradientTop, borderGradientBottom]
        borderGradient.cornerRadius = 6.0
        
        let borderMask = CAShapeLayer()
        borderMask.lineWidth = 1
        borderMask.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6.0).cgPath
        borderMask.fillColor = UIColor.clear.cgColor
        borderMask.strokeColor = UIColor.black.cgColor
        
        borderGradient.mask = borderMask
        self.layer.insertSublayer(borderGradient, below: self.titleLabel?.layer)
        
        // Background:
        let backgroundGradientTop = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1).cgColor
        let backgroundGradientBottom = UIColor(red: 174/255, green: 174/255, blue: 185/255, alpha: 1).cgColor
        let background = CAGradientLayer()
        background.frame = self.bounds
        background.colors = [backgroundGradientTop, backgroundGradientBottom]
        background.cornerRadius = 6.0
        self.layer.insertSublayer(background, below: borderGradient)
        
        // Shadow:
        self.layer.shadowColor = UIColor.customWhite1.withAlphaComponent(0.3).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 7.0
        self.layer.masksToBounds = false
        
        // Text
        self.setTitleColor(UIColor.msiBlue, for: .normal)
        self.setTitle(self.titleLabel?.text?.uppercased(), for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    }
    
}

class CustomButton3: UIButton {
    
    override func draw(_ rect: CGRect) {
        
        // Border:
        
        let borderGradientTop = UIColor.customBlue6.cgColor
        let borderGradientBottom = UIColor.customBlue7.cgColor
        let borderGradient = CAGradientLayer()
        borderGradient.frame = self.bounds
        borderGradient.colors = [borderGradientTop, borderGradientBottom]
        borderGradient.cornerRadius = 6.0
        
        let borderMask = CAShapeLayer()
        borderMask.lineWidth = 1
        borderMask.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6.0).cgPath
        borderMask.fillColor = UIColor.clear.cgColor
        borderMask.strokeColor = UIColor.black.cgColor
        
        borderGradient.mask = borderMask
        self.layer.insertSublayer(borderGradient, below: self.titleLabel?.layer)
        
        // Background:
        let backgroundGradientTop = UIColor.customBlue6.cgColor
        let backgroundGradientBottom = UIColor.customBlue7.cgColor
        let background = CAGradientLayer()
        background.frame = self.bounds
        background.colors = [backgroundGradientTop, backgroundGradientBottom]
        background.cornerRadius = 6.0
        self.layer.insertSublayer(background, below: borderGradient)
        
        // Shadow:
        self.layer.shadowColor = UIColor.customBlue6.withAlphaComponent(0.3).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 7.0
        self.layer.masksToBounds = false
        
        // Text
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitle(self.titleLabel?.text?.uppercased(), for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    }
    
}
