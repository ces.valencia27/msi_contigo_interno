//
//  AppDelegate.swift
//  MSIContigo
//
//  Created by David Valencia on 30/09/19.
//  Copyright © 2019 multisistemas. All rights reserved.
//

import UIKit
import CoreData
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSSubscriptionObserver {
    
    var window: UIWindow?

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        UINavigationBar.appearance().barTintColor = UIColor(red:0.08, green:0.23, blue:0.62, alpha:1.0)

        if #available(iOS 13.0, *) {

        } else {
            let firstEntry = UserDefaults.standard.bool(forKey: "firstEntry")
            
            if firstEntry != true {
                let startVC = LoginVC()
                let navigationVC = UINavigationController(rootViewController: startVC)
                navigationVC.navigationBar.barTintColor = UIColor.msiBlue
                navigationVC.navigationBar.barStyle = .black
                navigationVC.navigationBar.tintColor = UIColor.msiYellow
                navigationVC.navigationBar.isTranslucent = false
                navigationVC.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                navigationVC.navigationBar.shadowImage = UIImage()
                
                window = UIWindow(frame: UIScreen.main.bounds)
                window?.rootViewController = navigationVC
                window?.makeKeyAndVisible()
            } else {
                let homeVC = HomeVC()
                let navigationVC = UINavigationController(rootViewController: homeVC)
                navigationVC.modalPresentationStyle = .currentContext
                navigationVC.view.backgroundColor = UIColor.white
                navigationVC.navigationBar.barTintColor = UIColor.white
                navigationVC.navigationBar.tintColor = UIColor.black
                navigationVC.navigationBar.isTranslucent = false
                //        navigationVC.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
                navigationVC.navigationBar.shadowImage = UIImage()
                navigationVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)]
                
                window = UIWindow(frame: UIScreen.main.bounds)
                window?.rootViewController = navigationVC
                window?.makeKeyAndVisible()
            }
        }

        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]

        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
        appId: "c2973782-6adf-4302-9b49-6aa7c6835235",
        handleNotificationAction: nil,
        settings: onesignalInitSettings)

        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
        print("User accepted notifications: \(accepted)")
        })
        
        OneSignal.add(self as OSSubscriptionObserver)
        
        if #available(iOS 13.0, *) {

        } else {
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
           print("Subscribed for OneSignal push notifications!")
        }
        
        if let state = stateChanges {
            print("SubscriptionStateChange: \n\(state)")
        } else {
            debugPrint("No se obtuvo valor de la variable stateChanges")
        }

        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            
            UserDefaults.standard.set(playerId, forKey: "playerID")
        }
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "MSIContigo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

