//
//  PassRecoverModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct PassRecoverRequestModel: Codable {
    
    let numero_empleado: String
    let email: String
    
    init(numero_empleado: String, email: String) {
        
        self.numero_empleado = numero_empleado
        self.email = email
    }
}

struct PassRecoverModel {
    
    static let instance = PassRecoverModel()
    
    let msg: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.msg = dictionary?["msg"] as? String ?? "Ocurrió un error, espere unos segundos y vuelva a intentar"
    }
    
    public func getData(dict: NSDictionary, handler: @escaping (Bool, String) -> Void) {
        
        guard let status = dict["status"] as? Bool else {
            handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = PassRecoverModel(dictionary: data)
            handler(true, responseModel.msg)
        } else {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = PassRecoverModel(dictionary: data)
            handler(false, responseModel.msg)
        }
    }
    
    public static var shared: PassRecoverModel { return instance }
}
