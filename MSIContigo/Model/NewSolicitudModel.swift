//
//  NewSolicitudModel.swift
//  MSIContigo
//
//  Created by David Valencia on 12/11/19.
//  Copyright © 2019 multisistemas. All rights reserved.
//

import Foundation

struct NewSolicitudRequestModel: Codable {
    
    let numero_empleado: String
    let email: String
    let filial_id: String
    
    init(employee_number: String, employee_mail: String, filial_id: String) {
        
        self.numero_empleado = employee_number
        self.email = employee_mail
        self.filial_id = filial_id
    }
}

struct NewSolicitudModel {
    
    static let instance = NewSolicitudModel()
    
    let msg: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.msg = dictionary?["msg"] as? String ?? "Ocurrió un error, espere unos segundos y vuelva a intentar"
    }
    
    public func getData(dict: NSDictionary, handler: @escaping (Bool, String) -> Void) {
        
        guard let status = dict["status"] as? Bool else {
            handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = UpdatePushModel(dictionary: data)
            handler(true, responseModel.msg)
        } else {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = UpdatePushModel(dictionary: data)
            handler(false, responseModel.msg)
        }
    }
    
    static var shared: NewSolicitudModel { return instance}
}

