//
//  ModulesModel.swift
//  MSIContigo
//
//  Created by David Valencia on 28/09/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct ModulesModel {
    
    let iconName: String
    let moduleName: String
}
