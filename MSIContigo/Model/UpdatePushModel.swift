//
//  UpdatePushModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct UpdatePushRequestModel: Codable {
    
    let token: String?
    let onesignal: String?
    
    init(token: String? = nil, onesignal: String? = nil) {
        
        self.token = token
        self.onesignal = onesignal
    }
}

struct UpdatePushModel {
    
    static let instance = UpdatePushModel()
    
    let msg: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.msg = dictionary?["msg"] as? String ?? "Ocurrió un error, espere unos segundos y vuelva a intentar"
    }
    
    public func getData(dict: NSDictionary, handler: @escaping (Bool, String) -> Void) {
        
        guard let status = dict["status"] as? Bool else {
            handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = UpdatePushModel(dictionary: data)
            handler(true, responseModel.msg)
        } else {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = UpdatePushModel(dictionary: data)
            handler(false, responseModel.msg)
        }
    }
    
    public static var shared: UpdatePushModel { return instance }
}

