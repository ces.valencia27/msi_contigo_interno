//
//  LastHonorDetail.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct LastHonorDetailRequestModel {
    
    let token: String?
    let fecha_unix: String?
    
    init(token: String? = nil, fecha_unix: String? = nil) {
        
        self.token = token
        self.fecha_unix = fecha_unix
    }
}

struct LastHonorDetailModel {
    
    static let instance = LastHonorDetailModel()
    
    let nombre: String
    let descripcion: String
    let anio: String
    let mes: String
    let imagen: String
    let fecha_unix: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.nombre = dictionary?["nombre"] as? String ?? ""
        self.descripcion = dictionary?["descripcion"] as? String ?? ""
        self.anio = dictionary?["anio"] as? String ?? ""
        self.mes = dictionary?["mes"] as? String ?? ""
        self.imagen = dictionary?["imagen"] as? String ?? ""
        self.fecha_unix = dictionary?["fecha_unix"] as? String ?? ""
    }
    
    init(entity: LastHonorDetailEntity) {
        
        self.nombre = entity.nombre ?? ""
        self.descripcion = entity.descripcion ?? ""
        self.anio = entity.anio ?? ""
        self.mes = entity.mes ?? ""
        self.imagen = entity.imagen ?? ""
        self.fecha_unix = entity.fecha_unix ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, HonorDetailModel?, String?) -> Void) {
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            if let persona = data["persona"] as? NSDictionary {
                let model = HonorDetailModel(dictionary: persona)
                completionHandler(true, model, nil)
            } else {
                let msg = data["msg"] as? String ?? ""
                completionHandler(false, nil, msg)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: LastHonorDetailModel { return instance }
}




