//
//  WallModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct WallRequestModel {
    
    let token: String?
    
    init(token: String? = nil) {
        
        self.token = token
    }
}

struct WallModel {
    
    static let instance = WallModel()
    
    let id: Int
    let titulo: String
    let subtitulo: String
    let filial: String
    let imagen: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.id = dictionary?["id"] as? Int ?? 0
        self.titulo = dictionary?["titulo"] as? String ?? ""
        self.subtitulo = dictionary?["subtitulo"] as? String ?? ""
        self.filial = dictionary?["filial"] as? String ?? ""
        self.imagen = dictionary?["imagen"] as? String ?? ""
    }
    
    init(entity: WallEntity) {
        
        self.id = Int(entity.id)
        self.titulo = entity.titulo ?? ""
        self.subtitulo = entity.subtitulo ?? ""
        self.filial = entity.filial ?? ""
        self.imagen = entity.imagen ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, [WallModel]?, String?) -> Void) {
        
        var modelArray = [WallModel]()
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            
            guard let noticias = data["murales"] as? [NSDictionary] else {
                if let msg = data["msg"] as? String {
                    completionHandler(false, nil, msg)
                } else {
                    completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                }
                return
            }
            
            if noticias.count > 0 {
                modelArray.removeAll()
                noticias.forEach { (object) in
                    let model = WallModel(dictionary: object)
                    modelArray.append(model)
                }
                completionHandler(true, modelArray, nil)
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: WallModel { return instance }
}

