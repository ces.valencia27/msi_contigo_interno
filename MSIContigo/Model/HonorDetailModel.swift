//
//  HonorDetailModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct HonorDetailRequestModel {
    
    let token: String?
    let c_honor_id: Int?
    
    init(token: String? = nil, c_honor_id: Int? = nil) {
        
        self.token = token
        self.c_honor_id = c_honor_id
    }
}

struct HonorDetailModel {
    
    static let instance = HonorDetailModel()
    
    let nombre: String
    let descripcion: String
    let anio: String
    let mes: String
    let imagen: String
    let fecha_unix: Int
    
    init(dictionary: NSDictionary? = nil) {
        
        self.nombre = dictionary?["nombre"] as? String ?? ""
        self.descripcion = dictionary?["descripcion"] as? String ?? ""
        self.anio = dictionary?["anio"] as? String ?? ""
        self.mes = dictionary?["mes"] as? String ?? ""
        self.imagen = dictionary?["imagen"] as? String ?? ""
        self.fecha_unix = dictionary?["fecha_unix"] as? Int ?? 0
    }
    
    init(entity: HonorDetailEntity) {
        
        self.nombre = entity.nombre ?? ""
        self.descripcion = entity.descripcion ?? ""
        self.anio = entity.anio ?? ""
        self.mes = entity.mes ?? ""
        self.imagen = entity.imagen ?? ""
        self.fecha_unix = Int(entity.fecha_unix)
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, HonorDetailModel?, String?) -> Void) {
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let persona = data["persona"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let model = HonorDetailModel(dictionary: persona)
            completionHandler(true, model, nil)
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: HonorDetailModel { return instance }
}



