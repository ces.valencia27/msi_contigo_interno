//
//  WallDetailModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct WallDetailRequestModel {
    
    let token: String?
    let mural_id: Int?
    
    init(token: String? = nil, mural_id: Int? = nil) {
        
        self.token = token
        self.mural_id = mural_id
    }
}

struct WallDetailModel {
    
    static let instance = WallDetailModel()
    
    let titulo: String
    let subtitulo: String
    let contenido: String
    let fecha: String
    let filial: String
    let imagen: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.titulo = dictionary?["titulo"] as? String ?? ""
        self.subtitulo = dictionary?["subtitulo"] as? String ?? ""
        self.contenido = dictionary?["contenido"] as? String ?? ""
        self.fecha = dictionary?["fecha"] as? String ?? ""
        self.filial = dictionary?["filial"] as? String ?? ""
        self.imagen = dictionary?["imagen"] as? String ?? ""
    }
    
    init(entity: WallDetailEntity) {
        
        self.titulo = entity.titulo ?? ""
        self.subtitulo = entity.subtitulo ?? ""
        self.contenido = entity.contenido ?? ""
        self.fecha = entity.fecha ?? ""
        self.filial = entity.filial ?? ""
        self.imagen = entity.imagen ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, WallDetailModel?, String?) -> Void) {
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let mural = data["mural"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let model = WallDetailModel(dictionary: mural)
            completionHandler(true, model, nil)
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: WallDetailModel { return instance }
}


