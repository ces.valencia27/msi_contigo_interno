//
//  HonorBoxModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct HonorBoxRequestModel {
    
    let token: String?
    let qty: String?
    
    init(token: String? = nil, qty: String? = nil) {
        
        self.token = token
        self.qty = qty
    }
}

struct HonorBoxModel {
    
    static let instance = HonorBoxModel()
    
    let filial: String
    let personas: [PersonModel]
    
    init(dictionary: NSDictionary? = nil) {
        
        self.filial = dictionary?["filial"] as? String ?? ""
        self.personas = dictionary?["personas"] as? [PersonModel] ?? []
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, HonorBoxModel?, String?) -> Void) {
        
        var modelArray = [PersonModel]()
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let filial = data["filial"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let personas = data["personas"] as? [NSDictionary] else {
                if let msg = data["msg"] as? String {
                    completionHandler(false, nil, msg)
                } else {
                    completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                }
                return
            }
            
            if personas.count > 0 {
                modelArray.removeAll()
                personas.forEach { (object) in
                    let model = PersonModel(dictionary: object)
                    modelArray.append(model)
                }
                
                let dict: NSDictionary = ["filial":filial, "personas":modelArray]
        
                let modelo = HonorBoxModel(dictionary: dict)
                completionHandler(true, modelo, nil)
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: HonorBoxModel { return instance }
}

struct PersonModel {
    
    let id: Int
    let nombre: String
    let fecha: String
    let imagen: String
    
    init(dictionary: NSDictionary) {
        
        self.id = dictionary["id"] as? Int ?? 0
        self.nombre = dictionary["nombre"] as? String ?? ""
        self.fecha = dictionary["fecha"] as? String ?? ""
        self.imagen = dictionary["imagen"] as? String ?? ""
    }
    
    init(entity: PersonEntity) {
        
        self.id = Int(entity.id)
        self.nombre = entity.nombre ?? ""
        self.fecha = entity.fecha ?? ""
        self.imagen = entity.imagen ?? ""
    }
}


