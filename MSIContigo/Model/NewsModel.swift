//
//  NewsModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct NewsRequestModel {
    
    let token: String?
    let qty: String?
    
    init(token: String? = nil, qty: String? = nil) {
        
        self.token = token
        self.qty = qty
    }
    

}

struct NewsModel {
    
    static let instance = NewsModel()
    
    let id: Int
    let titulo: String
    let categoria: String
    let fecha: String
    let imagen: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.id = dictionary?["id"] as? Int ?? 0
        self.titulo = dictionary?["titulo"] as? String ?? ""
        self.categoria = dictionary?["categoria"] as? String ?? ""
        self.fecha = dictionary?["fecha"] as? String ?? ""
        self.imagen = dictionary?["imagen"] as? String ?? ""
    }
    
    init(entity: NewsEntity) {
        
        self.id = Int(entity.id)
        self.titulo = entity.titulo ?? ""
        self.categoria = entity.categoria ?? ""
        self.fecha = entity.fecha ?? ""
        self.imagen = entity.imagen ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, [NewsModel]?, String?) -> Void) {
        
        var modelArray = [NewsModel]()
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let noticias = data["noticias"] as? [NSDictionary] else {
                if let msg = data["msg"] as? String {
                    completionHandler(false, nil, msg)
                } else {
                    completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                }
                return
            }
            
            if noticias.count > 0 {
                modelArray.removeAll()
                noticias.forEach { (object) in
                    let model = NewsModel(dictionary: object)
                    modelArray.append(model)
                }
                completionHandler(true, modelArray, nil)
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: NewsModel { return instance }
}
