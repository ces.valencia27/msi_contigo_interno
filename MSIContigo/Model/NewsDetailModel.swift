//
//  NewsDetailModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct NewsDetailRequestModel {
    
    let token: String?
    let noticia_id: String?
    
    init(token: String? = nil, noticia_id: String? = nil) {
        
        self.token = token
        self.noticia_id = noticia_id
    }
}

struct NewsDetailModel {
    
    static let instance = NewsDetailModel()
    
    let titulo: String
    let contenido: String
    let autor: String
    let categoria: String
    let fecha: String
    let filial: String
    let imagen: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.titulo = dictionary?["titulo"] as? String ?? ""
        self.contenido = dictionary?["contenido"] as? String ?? ""
        self.autor = dictionary?["autor"] as? String ?? ""
        self.categoria = dictionary?["categoria"] as? String ?? ""
        self.fecha = dictionary?["fecha"] as? String ?? ""
        self.filial = dictionary?["filial"] as? String ?? ""
        self.imagen = dictionary?["imagen"] as? String ?? ""
    }
    
    init(entity: NewsDetailEntity) {
        
        self.titulo = entity.titulo ?? ""
        self.contenido = entity.contenido ?? ""
        self.autor = entity.autor ?? ""
        self.categoria = entity.categoria ?? ""
        self.fecha = entity.fecha ?? ""
        self.filial = entity.filial ?? ""
        self.imagen = entity.imagen ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, NewsDetailModel?, String?) -> Void) {
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let noticia = data["noticia"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let model = NewsDetailModel(dictionary: noticia)
            completionHandler(true, model, nil)
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: NewsDetailModel { return instance }
}
