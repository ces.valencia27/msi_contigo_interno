//
//  HomeCellModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import UIKit

struct HomeCellModel {
    
    let icon: UIImage
    let name: String
}
