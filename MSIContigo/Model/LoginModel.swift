//
//  LoginModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct LoginRequestModel: Codable {
    
    let numero_empleado: String
    let email: String
    let password: String
    
    init(employee_number: String, employee_mail: String, employee_password: String) {
        
        self.numero_empleado = employee_number
        self.email = employee_mail
        self.password = employee_password
        
    }
}

struct LoginModel {
    
    static let instance = LoginModel()
    
    private let msg: String
    private let token: String
    private let filial: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.msg = dictionary?["msg"] as? String ?? "Ocurrió un error, espere unos segundos y vuelva a intentar"
        self.token = dictionary?["token"] as? String ?? ""
        self.filial = dictionary?["filial"] as? String ?? ""
    }
    
    public func getData(dict: NSDictionary, handler: @escaping (Bool, String) -> Void) {
        
        guard let status = dict["status"] as? Bool else {
            handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = LoginModel(dictionary: data)
            
            UserDefaults.standard.set(responseModel.filial, forKey: "filial_name")
            
            handler(true, responseModel.token)
        } else {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = LoginModel(dictionary: data)
            handler(false, responseModel.msg)
        }
    }
    
    static var shared: LoginModel { return instance}
}
