//
//  FAQModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct FAQRequestModel {
    
    let token: String?
    
    init(token: String? = nil) {
        
        self.token = token
    }
}

struct FAQModel {
    
    static let instance = FAQModel()
    
    let pregunta: String
    let respuesta: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.pregunta = dictionary?["pregunta"] as? String ?? ""
        self.respuesta = dictionary?["respuesta"] as? String ?? ""
    }
    
    init(entity: FAQEntity) {
        
        self.pregunta = entity.pregunta ?? ""
        self.respuesta = entity.respuesta ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, [FAQModel]?, String?) -> Void) {
        
        var modelArray = [FAQModel]()
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let preguntas = data["preguntas"] as? [NSDictionary] else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            
            if preguntas.count > 0 {
                modelArray.removeAll()
                preguntas.forEach { (object) in
                    let model = FAQModel(dictionary: object)
                    modelArray.append(model)
                }
                completionHandler(true, modelArray, nil)
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: FAQModel { return instance }
}


