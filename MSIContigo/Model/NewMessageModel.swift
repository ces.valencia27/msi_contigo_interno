//
//  NewMessageModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct NewMessageRequestModel: Codable {
    
    let token: String?
    let area_id: String?
    let mensaje: String?
    
    init(token: String? = nil, area_id: String? = nil, mensaje: String? = nil) {
        
        self.token = token
        self.area_id = area_id
        self.mensaje = mensaje
    }
}

struct NewMessageModel {
    
    static let instance = NewMessageModel()
    
    let msg: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.msg = dictionary?["msg"] as? String ?? "Ocurrió un error, espere unos segundos y vuelva a intentar"
    }
    
    public func getData(dict: NSDictionary, handler: @escaping (Bool, String) -> Void) {
        
        guard let status = dict["status"] as? Bool else {
            handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = NewMessageModel(dictionary: data)
            handler(true, responseModel.msg)
        } else {
            guard let data = dict["data"] as? NSDictionary else {
                handler(false, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let responseModel = NewMessageModel(dictionary: data)
            handler(false, responseModel.msg)
        }
    }
    
    public static var shared: NewMessageModel { return instance }
}

