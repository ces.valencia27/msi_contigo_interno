//
//  AreasModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct AreasRequestModel {
    
    let token: String?
    
    init(token: String? = nil) {
        
        self.token = token
    }
}

struct AreasModel {
    
    static let instance = AreasModel()
    
    let id: Int
    let nombre: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.id = dictionary?["id"] as? Int ?? 0
        self.nombre = dictionary?["nombre"] as? String ?? ""
    }
    
    init(entity: AreasEntity) {
        
        self.id = Int(entity.id)
        self.nombre = entity.nombre ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, [AreasModel]?, String?) -> Void) {
        
        var modelArray = [AreasModel]()
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let areas = data["areas"] as? [NSDictionary] else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            
            if areas.count > 0 {
                modelArray.removeAll()
                areas.forEach { (object) in
                    let model = AreasModel(dictionary: object)
                    modelArray.append(model)
                }
                completionHandler(true, modelArray, nil)
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: AreasModel { return instance }
}

