//
//  QuestionModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct QuestionModel {
    
    var isExpanded: Bool
    let question: String
    let anwers: [String]
}
