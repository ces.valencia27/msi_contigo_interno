//
//  EmployeeModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct EmployeeRequestModel: Codable {
    
    let token: String
    
    init(token: String) {
        
        self.token = token
    }
}

struct EmployeeModel: Decodable {
    
    static let instance = EmployeeModel()
    
    let nombre: String
    let email: String
    let numero: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.nombre = dictionary?["nombre"] as? String ?? ""
        self.email = dictionary?["email"] as? String ?? ""
        self.numero = dictionary?["numero"] as? String ?? ""
    }
    
    init(entity: EmployeeEntity) {
        
        self.nombre = entity.nombre ?? ""
        self.email = entity.email ?? ""
        self.numero = entity.numero ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, EmployeeModel?, String?) -> Void) {
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let info = data["informacion"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            let model = EmployeeModel(dictionary: info)
            completionHandler(true, model, nil)
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: EmployeeModel { return instance }
}
