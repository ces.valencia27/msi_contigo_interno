//
//  AddModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct AddRequestModel {
    
    let token: String
    
    init(token: String) {
        
        self.token = token
    }
    
    
}

struct AddModel {
    
    static let instance = AddModel()
    
    let emisor: String
    let titulo: String
    let mensaje: String
    let fecha: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.emisor = dictionary?["emisor"] as? String ?? ""
        self.titulo = dictionary?["titulo"] as? String ?? ""
        self.mensaje = dictionary?["mensaje"] as? String ?? ""
        self.fecha = dictionary?["fecha"] as? String ?? ""
    }
    
    init(entity: AddEntity) {
        
        self.emisor = entity.emisor ?? ""
        self.titulo = entity.titulo ?? ""
        self.mensaje = entity.mensaje ?? ""
        self.fecha = entity.fecha ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, [AddModel]?, String?) -> Void) {
        
        var modelArray = [AddModel]()
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let avisos = data["avisos"] as? [NSDictionary] else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            
            if avisos.count > 0 {
                modelArray.removeAll()
                avisos.forEach { (object) in
                    let model = AddModel(dictionary: object)
                    modelArray.append(model)
                }
                completionHandler(true, modelArray, nil)
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: AddModel { return instance }
}

