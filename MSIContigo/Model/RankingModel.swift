//
//  RankingModel.swift
//  MSIContigo
//
//  Created by David Valencia on 8/27/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation

struct RankingRequestModel {
    
    let token: String
    let mural_id: String
    
    init(token: String, mural_id: String) {
        
        self.token = token
        self.mural_id = mural_id
    }
}

struct RankingModel {
    
    static let instance = RankingModel()
    
    let filial: String
    let valor: String
    let signo: String
    
    init(dictionary: NSDictionary? = nil) {
        
        self.filial = dictionary?["filial"] as? String ?? ""
        self.valor = dictionary?["valor"] as? String ?? ""
        self.signo = dictionary?["signo"] as? String ?? ""
    }
    
    public func getData(dictionary: NSDictionary, completionHandler: @escaping (Bool, [RankingModel]?, String?) -> Void) {
        
        var modelArray = [RankingModel]()
        
        guard let status = dictionary["status"] as? Bool else {
            completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
            return
        }
        
        if status {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let rankings = data["rankings"] as? [NSDictionary] else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            
            if rankings.count > 0 {
                modelArray.removeAll()
                rankings.forEach { (object) in
                    let model = RankingModel(dictionary: object)
                    modelArray.append(model)
                }
                completionHandler(true, modelArray, nil)
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            guard let data = dictionary["data"] as? NSDictionary else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            guard let msg = data["msg"] as? String else {
                completionHandler(false, nil, "Ocurrió un error, espere unos segundos y vuelva a intentar")
                return
            }
            completionHandler(false, nil, msg)
        }
    }
    
    public static var shared: RankingModel { return instance }
}

