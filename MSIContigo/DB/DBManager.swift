//
//  DBManager.swift
//  MSIContigo
//
//  Created by David Valencia on 9/23/19.
//  Copyright © 2019 Multisistemas. All rights reserved.
//

import Foundation
import CoreData

struct Bridge {
    
    static var context = DBManager.shared.persistanceContainer.viewContext
}

struct DBManager {
    
    static let shared = DBManager()
    
    let persistanceContainer: NSPersistentContainer = {
        let persistanceContainer = NSPersistentContainer(name: "MSIContigo")
        persistanceContainer.loadPersistentStores(completionHandler: { (storeDesc, error) in
            if let err = error {
                fatalError("No fue posible cargar la información: \(err)")
            }
        })
        
        return persistanceContainer
    }()
    
    // Método para eliminar todos los registros de cualquier entidad de Core Data
    func cleanEntity<T: NSManagedObject>(currentEntity: T.Type) {
        
        let context = DBManager.shared.persistanceContainer.viewContext
        
        let entName = String(describing: currentEntity)
        if let request = NSFetchRequest<T>(entityName: entName) as? NSFetchRequest<NSFetchRequestResult> {
            
            let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: request)
            
            do {
                try context.execute(batchDeleteRequest)
                debugPrint("Se eliminaron los registros correctamente")
            } catch let error {
                debugPrint("No se pudieron eliminar los registros de la entidad: \(error)")
            }
        } else {
            debugPrint("Error en el casting")
        }
        
    }
}


